#20190107
    电子商务系统商品模块：
        概念：
            1、商品分类：
                1.1、分为前台商品分类和后台商品分类，先有后台商品分类，前台商品分类从中进行选择；
                
                1.2、商品分类扩展信息：属性名+属性值
                    属性名区分 允许别名、关键属性、销售属性、非关键属性
                    是否必填、是否可枚举、是否多选、是否可自定义输入 - 
                        （5类具体的输入方式——单选（选项不可自定义）、单选（选项允许自定义）、多选（选项不可自定义）、多选（选项允许自定义）、文本输入框） 
                
            2、商品品牌:
            
            3、商品分类与商品品牌的关联关系：
            
            4、SPU、SKU
                4.1、SPU: 商品(名称、货号等)、商品基本属性： 商品+关键属性，确定唯一一种商品；
                4.2、SKU: 库存单位，携带销售属性确定唯一一个可销售SKU；
            
    表结构设计：  
         商品分类、商品分类扩展属性名、商品分类扩展属性值、商品品牌、商品货品(SPU)、商品货品信息基本扩展、商品信息(SKU)
         
#20190225
    淘宝的商品结构中，给每个属性项设定了4个特性——是否必填、是否可枚举、是否多选、是否可自定义输入。
    具体体现的场景是在创建商品时，某一个属性项的属性值应该采用什么方式输入——是不是必填项？是枚举选择项还是文本输入框？是单选还是多选？单选或多选的候选项是不是可以自定义？
    
    借鉴这4个特性的逻辑，我在设计时将属性项的输入抽象成了两项设置：一是是否必填，二是5类具体的输入方式——单选（选项不可自定义）、单选（选项允许自定义）、多选（选项不可自定义）、多选（选项允许自定义）、文本输入框。
    
    如果输入方式是可枚举的（单选或多选），那么就必须由类目属性的管理者（平台运营）预先录入枚举候选值，这样才能在输入这个属性项时有值可以选择。
    并且可以通过允许选项自定义，允许商家在录入商品时，在给出的候选值之外自定义想要输入的属性值，这样可以起到增加灵活性和丰富标准属性库的作用。毕竟商家是最了解自己商品的人。
    而如果输入方式是不可枚举的文本输入框，那么所有的属性值录入都由商品创建时自定义生成。平台运营者可以不需要维护枚举值，最大限度保证商品录入的灵活性。

    综上，当采用类目属性管理体系（叶子类目、标准属性库、类目属性及其输入方式）的方式，将上千个叶子类目的全部类目属性维护妥当之后，无论入驻的商家还是公司负责管理商品的运营，都可以准确的维护各个类目的商品信息。
    需要额外说明的是，类目属性设置的维护是一个长期的工作，只有不断地调整更新类目属性设置，才能确保商品信息的真实准确，以及能够更加精确的描述商品。
    这样在类目属性被应用于电商另一个核心领域——商品搜索——时，才能帮助用户更加精准的找到他们想要的商品。         



SET FOREIGN_KEY_CHECKS=0;
 
-- ----------------------------
-- Table structure for brand 品牌
-- ----------------------------
DROP TABLE IF EXISTS `brand`;
CREATE TABLE `brand` (
  `b_id` int(12) NOT NULL AUTO_INCREMENT,
  `b_name` varchar(50) DEFAULT NULL,
  `i_id` int(12) DEFAULT NULL,
  PRIMARY KEY (`b_id`),
  KEY `i_rel_bid` (`i_id`),
  KEY `b_name` (`b_name`),
  CONSTRAINT `i_rel_bid` FOREIGN KEY (`i_id`) REFERENCES `item` (`i_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
 
-- ----------------------------
-- Records of brand
-- ----------------------------
INSERT INTO `brand` VALUES ('1', '李宁', '2');
INSERT INTO `brand` VALUES ('2', '耐克', '2');
INSERT INTO `brand` VALUES ('3', '李宁', '4');
INSERT INTO `brand` VALUES ('4', '耐克', '4');
INSERT INTO `brand` VALUES ('5', '红蜻蜓', '1');
INSERT INTO `brand` VALUES ('6', '东方骆驼', '4');
INSERT INTO `brand` VALUES ('7', '婷美', '4');
INSERT INTO `brand` VALUES ('8', 'Chanel', '5');
INSERT INTO `brand` VALUES ('9', 'CoCo', '7');
INSERT INTO `brand` VALUES ('10', 'Amani', '8');
 
-- ----------------------------
-- Table structure for item 类目
-- ----------------------------
DROP TABLE IF EXISTS `item`;
CREATE TABLE `item` (
  `i_id` int(12) NOT NULL AUTO_INCREMENT,
  `i_name` varchar(50) DEFAULT NULL,
  `i_parent_id` int(12) DEFAULT NULL,
  PRIMARY KEY (`i_id`),
  KEY `i_fk_i` (`i_parent_id`),
  CONSTRAINT `i_fk_i` FOREIGN KEY (`i_parent_id`) REFERENCES `item` (`i_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
 
-- ----------------------------
-- Records of item
-- ----------------------------
INSERT INTO `item` VALUES ('1', '服装/鞋包', null);
INSERT INTO `item` VALUES ('2', '男装', '1');
INSERT INTO `item` VALUES ('3', '流行男鞋', '1');
INSERT INTO `item` VALUES ('4', '女装', '1');
INSERT INTO `item` VALUES ('5', '箱包', null);
INSERT INTO `item` VALUES ('6', '双肩包', '5');
INSERT INTO `item` VALUES ('7', '单肩包', '5');
INSERT INTO `item` VALUES ('8', '行李箱', '5');
INSERT INTO `item` VALUES ('9', '其他', null);
 
-- ----------------------------
-- Table structure for product 产品  ------可以再考虑增加多媒体资源的图片视频集合 
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `p_id` int(12) NOT NULL AUTO_INCREMENT,
  `p_name` varchar(50) DEFAULT NULL,
  `b_name` varchar(50) DEFAULT NULL,
  `p_fk_p` int(12) DEFAULT NULL,
  `p_fk_i` int(12) DEFAULT NULL,
  PRIMARY KEY (`p_id`),
  KEY `bname_rel_p` (`b_name`),
  KEY `p_rel_p` (`p_fk_p`),
  KEY `i_rel_p` (`p_fk_i`),
  CONSTRAINT `i_rel_p` FOREIGN KEY (`p_fk_i`) REFERENCES `item` (`i_id`),
  CONSTRAINT `bname_rel_p` FOREIGN KEY (`b_name`) REFERENCES `brand` (`b_name`),
  CONSTRAINT `p_rel_p` FOREIGN KEY (`p_fk_p`) REFERENCES `product` (`p_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
 
-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('1', '球鞋', '李宁', null, '1');
INSERT INTO `product` VALUES ('2', '网球', '李宁', null, '9');
INSERT INTO `product` VALUES ('3', '衬衫', '李宁', null, '1');
INSERT INTO `product` VALUES ('4', '袜子', '李宁', null, '1');
INSERT INTO `product` VALUES ('5', '球鞋', '耐克', null, '1');
INSERT INTO `product` VALUES ('6', 'Air', '耐克', null, '1');
INSERT INTO `product` VALUES ('7', '袜子', '耐克', null, '1');
INSERT INTO `product` VALUES ('8', '乒乓球', '耐克', null, '9');
INSERT INTO `product` VALUES ('9', '高跟鞋', '红蜻蜓', null, '1');
INSERT INTO `product` VALUES ('10', '水晶鞋', '红蜻蜓', null, '1');
INSERT INTO `product` VALUES ('11', '凉鞋', '婷美', null, '1');
INSERT INTO `product` VALUES ('12', '皮鞋', '婷美', null, '1');
INSERT INTO `product` VALUES ('13', '高跟鞋', '婷美', null, '1');
INSERT INTO `product` VALUES ('14', 'Air1', '耐克', '6', '1');
INSERT INTO `product` VALUES ('15', 'Air2', '耐克', '6', '1');
INSERT INTO `product` VALUES ('16', 'Air3', '耐克', '6', '1');
 
-- ----------------------------
-- Table structure for product_pro  产品基本属性值表		产品 属性名/属性值
-- ----------------------------
DROP TABLE IF EXISTS `product_pro`;
CREATE TABLE `product_pro` (
  `pp_id` int(12) NOT NULL AUTO_INCREMENT,
  `pp_fk_p` int(12) DEFAULT NULL,
  `pp_fk_pv` int(12) DEFAULT NULL,
  `pp_fk_pn` int(12) DEFAULT NULL,
  PRIMARY KEY (`pp_id`),
  KEY `pv_rel_pp` (`pp_fk_pv`),
  KEY `p_rel_pp` (`pp_fk_p`),
  KEY `pn_rel_pp` (`pp_fk_pn`),
  CONSTRAINT `pn_rel_pp` FOREIGN KEY (`pp_fk_pn`) REFERENCES `pro_name` (`pro_id`),
  CONSTRAINT `pv_rel_pp` FOREIGN KEY (`pp_fk_pv`) REFERENCES `pro_value` (`pv_id`),
  CONSTRAINT `p_rel_pp` FOREIGN KEY (`pp_fk_p`) REFERENCES `product` (`p_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
 
-- ----------------------------
-- Records of product_pro
-- ----------------------------
INSERT INTO `product_pro` VALUES ('9', '1', '1', '1');
INSERT INTO `product_pro` VALUES ('10', '1', '2', '1');
INSERT INTO `product_pro` VALUES ('11', '1', '3', '2');
INSERT INTO `product_pro` VALUES ('12', '1', '4', '2');
INSERT INTO `product_pro` VALUES ('13', '1', '5', '2');
INSERT INTO `product_pro` VALUES ('14', '1', '6', '3');
INSERT INTO `product_pro` VALUES ('15', '1', '7', '3');
INSERT INTO `product_pro` VALUES ('16', '1', '8', '3');
 
-- ----------------------------
-- Table structure for product_sku	产品SKU
-- ----------------------------
DROP TABLE IF EXISTS `product_sku`;
CREATE TABLE `product_sku` (
  `ps_id` int(12) NOT NULL AUTO_INCREMENT,
  `pd_fk_id` int(12) DEFAULT NULL,
  `pd_num` int(11) DEFAULT NULL,
  `pd_price` decimal(10,4) DEFAULT NULL,
  `pd_name` varchar(50) DEFAULT NULL,
  `pd_properties` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`ps_id`),
  KEY `p_rel_ps` (`pd_fk_id`),
  CONSTRAINT `p_rel_ps` FOREIGN KEY (`pd_fk_id`) REFERENCES `product` (`p_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
 
-- ----------------------------
-- Records of product_sku
-- ----------------------------
INSERT INTO `product_sku` VALUES ('1', '1', '53', '528.5000', '李宁-A79-12球鞋（红，亚麻，北美）', '1;3;6;');
INSERT INTO `product_sku` VALUES ('2', '1', '23', '238.9000', '李宁-A79-12球鞋（绿，塑料，中国）', '2;4;8;');
INSERT INTO `product_sku` VALUES ('3', '1', '10', '370.0000', '李宁-A21-11球鞋（绿，针织，南非）', '2;5;7;');
INSERT INTO `product_sku` VALUES ('4', '2', '12', '123.3000', '李宁-B1-12网球（红，针织，南非）', '1;5;7;');
INSERT INTO `product_sku` VALUES ('5', '2', '19', '250.0000', '李宁-B3-18网球（绿，针织，南非）', '2;5;7;');
INSERT INTO `product_sku` VALUES ('6', '3', '12', '200.0000', '李宁-A-12衬衫（绿，针织，南非）', '2;5;7;');
INSERT INTO `product_sku` VALUES ('7', '4', '10', '10.0000', '李宁-C-12袜子（绿，针织，南非）', '2;5;7;');
INSERT INTO `product_sku` VALUES ('9', '5', '5', '498.5000', '耐克-A79-12球鞋（绿，亚麻，北美）', '2;3;6;');
INSERT INTO `product_sku` VALUES ('10', '5', '25', '498.5000', '耐克-A79-12球鞋（绿，塑料，北美）', '2;4;6;');
INSERT INTO `product_sku` VALUES ('11', '5', '20', '498.5000', '耐克-A79-12球鞋（绿，塑料，南非）', '2;4;7;');
INSERT INTO `product_sku` VALUES ('13', '14', '1', '238.9000', '耐克Air1-A102-23球鞋（绿，亚麻，中国）', '2;3;8;');
INSERT INTO `product_sku` VALUES ('14', '14', '2', '250.9000', '耐克Air1-A102-23球鞋（绿，针织，北美）', '2;5;6;');
INSERT INTO `product_sku` VALUES ('15', '15', '22', '200.9000', '耐克Air2-A102-23球鞋（绿，针织，南非）', '2;5;7;');
INSERT INTO `product_sku` VALUES ('16', '16', '12', '200.9000', '耐克Air3-A21-11球鞋（红，亚麻，北美）', '1;3;6;');
INSERT INTO `product_sku` VALUES ('17', '12', '12', '200.9000', '婷美-A21-11凉鞋（红，亚麻，南非）', '1;3;7;');
INSERT INTO `product_sku` VALUES ('18', '12', '12', '238.9000', '婷美-A21-11凉鞋（红，亚麻，中国）', '1;3;8;');
INSERT INTO `product_sku` VALUES ('19', '12', '12', '320.9000', '婷美-A21-11凉鞋（红，塑料，南非）', '2;4;8;');
 
-- ----------------------------
-- Table structure for pro_name	允许别名  枚举属性(从属性值中选择一个) 输入属性(属性值允许输入)  关键属性(能够确定唯一商品) 销售属性(能够确定SKU) 非关键属性(非重要属性) 搜索属性(允许搜索)
-- 是否必填、是否可枚举、是否多选、是否可自定义输入 - （5类具体的输入方式——单选（选项不可自定义）、单选（选项允许自定义）、多选（选项不可自定义）、多选（选项允许自定义）、文本输入框）
-- ----------------------------	
DROP TABLE IF EXISTS `pro_name`;
CREATE TABLE `pro_name` (
  `pro_id` int(12) NOT NULL AUTO_INCREMENT,
  `pro_name` varchar(50) DEFAULT NULL,
  `pro_fk_iid` int(12) DEFAULT NULL,
  `pro_has_otherName` char(2) NOT NULL DEFAULT '0',
  `pro_has_enum` char(2) NOT NULL DEFAULT '0',
  `pro_has_input` char(2) NOT NULL DEFAULT '0',
  `pro_is_key` char(2) NOT NULL DEFAULT '0',
  `pro_is_sale` char(2) NOT NULL DEFAULT '0',
  `pro_is_must` char(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pro_id`),
  KEY `i_ref_pn` (`pro_fk_iid`),
  CONSTRAINT `i_ref_pn` FOREIGN KEY (`pro_fk_iid`) REFERENCES `item` (`i_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
 
-- ----------------------------
-- Records of pro_name
-- ----------------------------
INSERT INTO `pro_name` VALUES ('1', '颜色', '1', '0', '1', '0', '0', '0', '0', '0');
INSERT INTO `pro_name` VALUES ('2', '材质', '1', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `pro_name` VALUES ('3', '厂商', '1', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `pro_name` VALUES ('4', '尺码', '1', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `pro_name` VALUES ('5', '面值', '5', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `pro_name` VALUES ('6', '渠道', '5', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `pro_name` VALUES ('15', '鞋跟', '3', '0', '0', '0', '0', '0', '0', '0');
 
-- ----------------------------
-- Table structure for pro_value
-- ----------------------------
DROP TABLE IF EXISTS `pro_value`;
CREATE TABLE `pro_value` (
  `pv_id` int(12) NOT NULL AUTO_INCREMENT,
  `pv_name` varchar(50) DEFAULT NULL,
  `pv_fk_pid` int(12) DEFAULT NULL,
  PRIMARY KEY (`pv_id`),
  KEY `pn_rel_pv` (`pv_fk_pid`),
  CONSTRAINT `pn_rel_pv` FOREIGN KEY (`pv_fk_pid`) REFERENCES `pro_name` (`pro_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
 
-- ----------------------------
-- Records of pro_value
-- ----------------------------
INSERT INTO `pro_value` VALUES ('1', '红', '1');
INSERT INTO `pro_value` VALUES ('2', '绿', '1');
INSERT INTO `pro_value` VALUES ('3', '亚麻', '2');
INSERT INTO `pro_value` VALUES ('4', '塑料', '2');
INSERT INTO `pro_value` VALUES ('5', '针织', '2');
INSERT INTO `pro_value` VALUES ('6', '北美工厂店', '3');
INSERT INTO `pro_value` VALUES ('7', '南非工厂店', '3');
INSERT INTO `pro_value` VALUES ('8', '中国制造', '3');
INSERT INTO `pro_value` VALUES ('9', '高脚鞋跟', '15');
INSERT INTO `pro_value` VALUES ('10', '平底鞋跟', '15');
INSERT INTO `pro_value` VALUES ('11', '尖顶鞋跟', '15');









CREATE TABLE brand_info(
  brand_id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL COMMENT '品牌ID',
  brand_name VARCHAR(50) NOT NULL COMMENT '品牌名称',
  telephone VARCHAR(50) NOT NULL COMMENT '联系电话',
  brand_web VARCHAR(100) COMMENT '品牌网络',
  brand_logo VARCHAR(100) COMMENT '品牌logo URL',
  brand_desc VARCHAR(150) COMMENT '品牌描述',
  brand_status TINYINT NOT NULL DEFAULT 0 COMMENT '品牌状态,0禁用,1启用',
  brand_order TINYINT NOT NULL DEFAULT 0 COMMENT '排序',
  modified_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最后修改时间',
  PRIMARY KEY pk_brandid (brand_id)
)ENGINE=innodb COMMENT '品牌信息表';