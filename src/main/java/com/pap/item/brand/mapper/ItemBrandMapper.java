package com.pap.item.brand.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.item.brand.entity.ItemBrand;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;;

@Mapper
public interface ItemBrandMapper extends PapBaseMapper<ItemBrand> {
    int deleteByPrimaryKey(String itemBrandId);

    int selectCountByMap(Map<Object, Object> map);

    List<ItemBrand> selectListByMap(Map<Object, Object> map);

    ItemBrand selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(ItemBrand record);

    int insertSelective(ItemBrand record);

    ItemBrand selectByPrimaryKey(String itemBrandId);

    int updateByPrimaryKeySelective(ItemBrand record);

    int updateByPrimaryKey(ItemBrand record);
}