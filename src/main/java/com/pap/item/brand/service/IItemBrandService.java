package com.pap.item.brand.service;

import com.pap.item.brand.entity.ItemBrand;

import java.util.List;
import java.util.Map;

public interface IItemBrandService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<ItemBrand> selectListByMap(Map<Object, Object> map);

    int insert(ItemBrand record);

    int insertSelective(ItemBrand record);

    ItemBrand selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ItemBrand record);

    int updateByPrimaryKey(ItemBrand record);
}
