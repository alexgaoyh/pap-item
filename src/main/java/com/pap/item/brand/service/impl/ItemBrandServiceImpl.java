package com.pap.item.brand.service.impl;

import com.pap.item.brand.mapper.ItemBrandMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pap.item.brand.entity.ItemBrand;
import com.pap.item.brand.service.IItemBrandService;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Transactional
@Service("itemBrandService")
public class ItemBrandServiceImpl implements IItemBrandService {

    @Resource(name = "itemBrandMapper")
    private ItemBrandMapper mapper;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<ItemBrand> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(ItemBrand record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(ItemBrand record) {
       return mapper.insertSelective(record);
    }

    @Override
    public ItemBrand selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(ItemBrand record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(ItemBrand record) {
      return mapper.updateByPrimaryKey(record);
    }
}