package com.pap.item.brand.entity;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.*;
import java.io.Serializable;

@MyBatisTableAnnotation(name = "t_item_brand", namespace = "com.pap.item.brand.mapper.ItemBrandMapper", remarks = " 修改点 ", aliasName = "t_item_brand t_item_brand" )
public class ItemBrand extends PapBaseEntity implements Serializable {
    /**
     *  商品品牌编号,所属表字段为t_item_brand.ITEM_BRAND_ID
     */
    @MyBatisColumnAnnotation(name = "ITEM_BRAND_ID", value = "t_item_brand_ITEM_BRAND_ID", chineseNote = "商品品牌编号", tableAlias = "t_item_brand")
    @MyApiModelPropertyAnnotation(value = "商品品牌编号")
    private String itemBrandId;

    /**
     *  商品品牌编码,所属表字段为t_item_brand.ITEM_BRAND_CODE
     */
    @MyBatisColumnAnnotation(name = "ITEM_BRAND_CODE", value = "t_item_brand_ITEM_BRAND_CODE", chineseNote = "商品品牌编码", tableAlias = "t_item_brand")
    @MyApiModelPropertyAnnotation(value = "商品品牌编码")
    private String itemBrandCode;

    /**
     *  商品品牌名称,所属表字段为t_item_brand.ITEM_BRAND_NAME
     */
    @MyBatisColumnAnnotation(name = "ITEM_BRAND_NAME", value = "t_item_brand_ITEM_BRAND_NAME", chineseNote = "商品品牌名称", tableAlias = "t_item_brand")
    @MyApiModelPropertyAnnotation(value = "商品品牌名称")
    private String itemBrandName;

    /**
     *  联系电话,所属表字段为t_item_brand.TELEPHONE
     */
    @MyBatisColumnAnnotation(name = "TELEPHONE", value = "t_item_brand_TELEPHONE", chineseNote = "联系电话", tableAlias = "t_item_brand")
    @MyApiModelPropertyAnnotation(value = "联系电话")
    private String telephone;

    /**
     *  品牌官网,所属表字段为t_item_brand.BRAND_URL
     */
    @MyBatisColumnAnnotation(name = "BRAND_URL", value = "t_item_brand_BRAND_URL", chineseNote = "品牌官网", tableAlias = "t_item_brand")
    @MyApiModelPropertyAnnotation(value = "品牌官网")
    private String brandUrl;

    /**
     *  品牌LOGO,所属表字段为t_item_brand.BRAND_LOGO
     */
    @MyBatisColumnAnnotation(name = "BRAND_LOGO", value = "t_item_brand_BRAND_LOGO", chineseNote = "品牌LOGO", tableAlias = "t_item_brand")
    @MyApiModelPropertyAnnotation(value = "品牌LOGO")
    private String brandLogo;

    /**
     *  品牌描述,所属表字段为t_item_brand.BRAND_DESC
     */
    @MyBatisColumnAnnotation(name = "BRAND_DESC", value = "t_item_brand_BRAND_DESC", chineseNote = "品牌描述", tableAlias = "t_item_brand")
    @MyApiModelPropertyAnnotation(value = "品牌描述")
    private String brandDesc;

    /**
     *  可用状态标示(Y可用/N不可),所属表字段为t_item_brand.DISABLE_FLAG
     */
    @MyBatisColumnAnnotation(name = "DISABLE_FLAG", value = "t_item_brand_DISABLE_FLAG", chineseNote = "可用状态标示(Y可用/N不可)", tableAlias = "t_item_brand")
    @MyApiModelPropertyAnnotation(value = "可用状态标示(Y可用/N不可)")
    private String disableFlag;

    /**
     *  排序号,所属表字段为t_item_brand.SEQ_NO
     */
    @MyBatisColumnAnnotation(name = "SEQ_NO", value = "t_item_brand_SEQ_NO", chineseNote = "排序号", tableAlias = "t_item_brand")
    @MyApiModelPropertyAnnotation(value = "排序号")
    private Integer seqNo;

    @Override
    public String getDynamicTableName() {
        return "t_item_brand";
    }

    private static final long serialVersionUID = 1L;

    public String getItemBrandId() {
        return itemBrandId;
    }

    public void setItemBrandId(String itemBrandId) {
        this.itemBrandId = itemBrandId;
    }

    public String getItemBrandCode() {
        return itemBrandCode;
    }

    public void setItemBrandCode(String itemBrandCode) {
        this.itemBrandCode = itemBrandCode;
    }

    public String getItemBrandName() {
        return itemBrandName;
    }

    public void setItemBrandName(String itemBrandName) {
        this.itemBrandName = itemBrandName;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getBrandUrl() {
        return brandUrl;
    }

    public void setBrandUrl(String brandUrl) {
        this.brandUrl = brandUrl;
    }

    public String getBrandLogo() {
        return brandLogo;
    }

    public void setBrandLogo(String brandLogo) {
        this.brandLogo = brandLogo;
    }

    public String getBrandDesc() {
        return brandDesc;
    }

    public void setBrandDesc(String brandDesc) {
        this.brandDesc = brandDesc;
    }

    public String getDisableFlag() {
        return disableFlag;
    }

    public void setDisableFlag(String disableFlag) {
        this.disableFlag = disableFlag;
    }

    public Integer getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(Integer seqNo) {
        this.seqNo = seqNo;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", itemBrandId=").append(itemBrandId);
        sb.append(", itemBrandCode=").append(itemBrandCode);
        sb.append(", itemBrandName=").append(itemBrandName);
        sb.append(", telephone=").append(telephone);
        sb.append(", brandUrl=").append(brandUrl);
        sb.append(", brandLogo=").append(brandLogo);
        sb.append(", brandDesc=").append(brandDesc);
        sb.append(", disableFlag=").append(disableFlag);
        sb.append(", seqNo=").append(seqNo);
        sb.append("]");
        return sb.toString();
    }
}