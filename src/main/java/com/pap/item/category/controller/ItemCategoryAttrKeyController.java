package com.pap.item.category.controller;

import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.item.category.entity.ItemCategoryAttrKey;
import com.pap.item.category.service.IItemCategoryAttrKeyService;
import com.pap.item.dto.ItemCategoryAttrKeyDTO;
import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.page.Page;
import com.pap.obj.vo.response.ResponseVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/itemcategoryattrkey")
@Api(value = "商品类别扩展属性管理", tags = "商品类别扩展属性管理", description="商品类别扩展属性管理")
public class ItemCategoryAttrKeyController {

	private static Logger logger  =  LoggerFactory.getLogger(ItemCategoryAttrKeyController.class);
	
	@Resource(name = "itemCategoryAttrKeyService")
	private IItemCategoryAttrKeyService itemCategoryAttrKeyService;

	@Resource(name = "idWorker")
	private IdWorker idWorker;

	@ApiOperation("创建")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "itemCategoryAttrKeyDTOList", value = "", required = true, dataType = "ItemCategoryAttrKeyDTO", paramType="body")
	})
	@PostMapping(value = "")
	public ResponseVO<ItemCategoryAttrKeyDTO> create(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													 @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													 @RequestBody List<ItemCategoryAttrKeyDTO> itemCategoryAttrKeyDTOList,
													 @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		itemCategoryAttrKeyService.saveItemCategoryAttrKeyWithDetailInfo(itemCategoryAttrKeyDTOList, loginedUserVO);

		ItemCategoryAttrKeyDTO itemCategoryAttrKeyDTOTemp = itemCategoryAttrKeyService.selectItemCategoryAttrKeyWithDetailInfoById(itemCategoryAttrKeyDTOList.get(0).getItemCategoryId());

		return ResponseVO.successdata(itemCategoryAttrKeyDTOTemp);
	}

	@ApiOperation("更新")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "itemCategoryAttrKeyDTOList", value = "", required = true, dataType = "ItemCategoryAttrKeyDTO", paramType="body")
	})
	@PutMapping(value = "")
	public ResponseVO<ItemCategoryAttrKeyDTO> update(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody List<ItemCategoryAttrKeyDTO> itemCategoryAttrKeyDTOList,
													 @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		itemCategoryAttrKeyService.updateItemCategoryAttrKeyWithDetailInfo(itemCategoryAttrKeyDTOList, loginedUserVO);

		ItemCategoryAttrKeyDTO itemCategoryAttrKeyDTOTemp = itemCategoryAttrKeyService.selectItemCategoryAttrKeyWithDetailInfoById(itemCategoryAttrKeyDTOList.get(0).getItemCategoryId());

		return ResponseVO.successdata(itemCategoryAttrKeyDTOTemp);
	}


	@ApiOperation("删除")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "itemCategoryAttrKeyDTO", value = "", required = true, dataType = "ItemCategoryAttrKeyDTO", paramType="body")
	})
	@DeleteMapping(value = "")
	public ResponseVO<String> delete(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody ItemCategoryAttrKeyDTO itemCategoryAttrKeyDTO){

		int i = itemCategoryAttrKeyService.deleteItemCategoryAttrKeyWithDetailInfo(itemCategoryAttrKeyDTO.getItemCategoryAttrKeyId());

		return ResponseVO.successdata("操作成功");
	}

	@ApiOperation("根据类目编号获取属性集合信息")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "itemCategoryId", value = "类目编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "getByItemCategoryId/{itemCategoryId}")
	public ResponseVO<List<ItemCategoryAttrKeyDTO>> getByItemCategoryId(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
														@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
														@PathVariable("itemCategoryId") String itemCategoryId,
														@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		List<ItemCategoryAttrKeyDTO> itemCategoryAttrKeyDTOList = itemCategoryAttrKeyService.selectItemCategoryAttrKeyWithDetailListByItemCategoryId(itemCategoryId);

		return ResponseVO.successdatas(itemCategoryAttrKeyDTOList, null);
	}

	@ApiOperation("详情")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "id", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/{id}")
	public ResponseVO<ItemCategoryAttrKeyDTO> selectOne(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@PathVariable("id") String id,
													@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		ItemCategoryAttrKeyDTO itemCategoryAttrKeyDTO = itemCategoryAttrKeyService.selectItemCategoryAttrKeyWithDetailInfoById(id);

		return ResponseVO.successdata(itemCategoryAttrKeyDTO);
	}

	@ApiOperation("查询")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "itemCategoryAttrKeyDTO", value = "应用查询参数", required = false, dataType = "ItemCategoryAttrKeyDTO", paramType="body"),
		@ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
		@ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
	})
	@PostMapping(value = "/query")
	public ResponseVO<ItemCategoryAttrKeyDTO>query(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												@RequestBody ItemCategoryAttrKeyDTO itemCategoryAttrKeyDTO,
												@RequestParam Integer pageNo, @RequestParam Integer pageSize,
												@RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
											    @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		Map<Object, Object> map = BeanCopyUtilss.beanToMap(itemCategoryAttrKeyDTO, loginedUserVO);
		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			map.put("page", newPage);
		}
		List<ItemCategoryAttrKey> list = itemCategoryAttrKeyService.selectListByMap(map);

		//
		List<ItemCategoryAttrKeyDTO> dtoList = toDTO(list);

		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			int countNum = itemCategoryAttrKeyService.selectCountByMap(map);
			newPage.setCount(countNum);
			return ResponseVO.successdatas(dtoList, newPage);
		} else {
			return ResponseVO.successdatas(dtoList, null);
		}

	}

	private List<ItemCategoryAttrKeyDTO> toDTO(List<ItemCategoryAttrKey> databaseList) {
		List<ItemCategoryAttrKeyDTO> returnList = new ArrayList<ItemCategoryAttrKeyDTO>();
		if(databaseList != null) {
			for(ItemCategoryAttrKey dbEntity : databaseList) {
				ItemCategoryAttrKeyDTO dtoTemp = new ItemCategoryAttrKeyDTO();
				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
				returnList.add(dtoTemp);
			}
		}
		return returnList;
	}

	private List<ItemCategoryAttrKey> toDB(List<ItemCategoryAttrKeyDTO> dtoList) {
		List<ItemCategoryAttrKey> returnList = new ArrayList<ItemCategoryAttrKey>();
		if(dtoList != null) {
			for(ItemCategoryAttrKeyDTO dtoEntity : dtoList) {
				ItemCategoryAttrKey dbTemp = new ItemCategoryAttrKey();
				BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
				returnList.add(dbTemp);
			}
		}
		return returnList;
	}
}
