package com.pap.item.category.controller;

import com.github.stuxuhai.jpinyin.PinyinFormat;
import com.github.stuxuhai.jpinyin.PinyinHelper;
import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.base.util.string.StringUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.item.category.entity.ItemCategory;
import com.pap.item.category.service.IItemCategoryService;
import com.pap.item.dto.ItemCategoryDTO;
import com.pap.item.dto.ItemCategoryTreeNodeVO;
import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.page.Page;
import com.pap.obj.vo.response.ResponseVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/itemcategory")
@Api(value = "商品类别管理", tags = "商品类别管理", description="商品类别管理")
public class ItemCategoryController {

	private static Logger logger  =  LoggerFactory.getLogger(ItemCategoryController.class);
	
	@Resource(name = "itemCategoryService")
	private IItemCategoryService itemCategoryService;

	@Resource(name = "idWorker")
	private IdWorker idWorker;

	@ApiOperation("创建")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "itemCategoryDTO", value = "", required = true, dataType = "ItemCategoryDTO", paramType="body")
	})
	@PostMapping(value = "")
	public ResponseVO<ItemCategoryDTO> create(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody ItemCategoryDTO itemCategoryDTO,
											  @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO) throws Exception {
		String idStr = idWorker.nextIdStr();
		itemCategoryDTO.setItemCategoryId(idStr);

		if(itemCategoryDTO != null &&
				StringUtilss.isNotEmpty(itemCategoryDTO.getItemCategoryName())) {
			// 如果前端没有传递过来code值，则默认使用拼音代替
			if(StringUtilss.isEmpty(itemCategoryDTO.getItemCategoryCode())) {
				itemCategoryDTO.setItemCategoryCode(PinyinHelper.convertToPinyinString(itemCategoryDTO.getItemCategoryName(), "", PinyinFormat.WITHOUT_TONE));
			}
			// 处理父级数据
			ItemCategory parentitemCategory = itemCategoryService.selectByPrimaryKey(itemCategoryDTO.getItemCategoryParentId());
			if(parentitemCategory != null) {
				// pathId
				if(StringUtilss.isNotEmpty(parentitemCategory.getPathIds())) {
					itemCategoryDTO.setPathIds(parentitemCategory.getPathIds() + "," + parentitemCategory.getItemCategoryId());
				} else {
					itemCategoryDTO.setPathIds(parentitemCategory.getItemCategoryId());
				}
				// pathCode
				if(StringUtilss.isNotEmpty(parentitemCategory.getPathCodes())) {
					itemCategoryDTO.setPathCodes(parentitemCategory.getPathCodes() + "," + parentitemCategory.getItemCategoryCode());
				} else {
					itemCategoryDTO.setPathCodes(parentitemCategory.getItemCategoryCode());
				}
				// pathName
				if(StringUtilss.isNotEmpty(parentitemCategory.getPathNames())) {
					itemCategoryDTO.setPathNames(parentitemCategory.getPathNames() + "," + parentitemCategory.getItemCategoryName());
				} else {
					itemCategoryDTO.setPathNames(parentitemCategory.getItemCategoryName());
				}
			} else {
				itemCategoryDTO.setItemCategoryParentId(null);
				itemCategoryDTO.setPathIds(null);
				itemCategoryDTO.setPathCodes(null);
				itemCategoryDTO.setPathNames(null);
			}
			itemCategoryDTO.setItemCategoryParentId(StringUtilss.isNotEmpty(itemCategoryDTO.getItemCategoryParentId()) == true ? itemCategoryDTO.getItemCategoryParentId() : "-1");

			ItemCategory dbitemCategory = new ItemCategory();
			BeanCopyUtilss.myCopyProperties(itemCategoryDTO, dbitemCategory, loginedUserVO, true);
			int operationInt = itemCategoryService.insertSelective(dbitemCategory);

			return ResponseVO.successdata(itemCategoryDTO);

		} else {
			return ResponseVO.validfail("入参异常，请判断是否含有组织类别名称");
		}
	}

	@ApiOperation("更新")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "itemCategoryDTO", value = "", required = true, dataType = "ItemCategoryDTO", paramType="body")
	})
	@PutMapping(value = "")
	public ResponseVO<ItemCategoryDTO> update(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody ItemCategoryDTO itemCategoryDTO,
											  @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){


		ItemCategory databaseObj = new ItemCategory();
		BeanCopyUtilss.myCopyProperties(itemCategoryDTO, databaseObj, loginedUserVO, false);

		ItemCategory dbItemCategory = new ItemCategory();
		BeanCopyUtilss.myCopyProperties(itemCategoryDTO, dbItemCategory);
		ResponseVO responseVO = itemCategoryService.updateAndCheckItemCategory(dbItemCategory);

		if(responseVO.getCode().equals("200")) {
			// 查询最新的数据进行返回
			ItemCategory itemCategoryReturn = itemCategoryService.selectByPrimaryKey(itemCategoryDTO.getItemCategoryId());

			ItemCategory itemCategoryDTOReturn = new ItemCategory();
			BeanCopyUtilss.myCopyProperties(itemCategoryReturn, itemCategoryDTOReturn);

			return ResponseVO.successdata(itemCategoryDTOReturn);
		} else {
			return responseVO;
		}

	}


	@ApiOperation("删除")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "itemCategoryDTO", value = "", required = true, dataType = "ItemCategoryDTO", paramType="body")
	})
	@DeleteMapping(value = "")
	public ResponseVO<String> delete(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody ItemCategoryDTO itemCategoryDTO){

		if(itemCategoryDTO != null && StringUtilss.isNotEmpty(itemCategoryDTO.getItemCategoryId())) {
			Map<Object, Object> pathIdsMap = new HashMap<Object, Object>();
			pathIdsMap.put("pathIds", itemCategoryDTO.getItemCategoryId());
			List<ItemCategory> ItemCategoryList = itemCategoryService.selectListByMap(pathIdsMap);
			if(ItemCategoryList != null && ItemCategoryList.size() > 0) {
				return ResponseVO.validfail("请检查入参，此类别被其他类别所依赖!");
			} else {
				int operationInt = itemCategoryService.deleteByPrimaryKey(itemCategoryDTO.getItemCategoryId());
				return ResponseVO.successdata("操作成功");
			}
		} else {
			return ResponseVO.validfail("入参异常，请判断是否含有类别编码");
		}
	}

	@ApiOperation("详情")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "id", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/{id}")
	public ResponseVO<ItemCategoryDTO> selectOne(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@PathVariable("id") String id){

		ItemCategory databaseObj = itemCategoryService.selectByPrimaryKey(id);

		return ResponseVO.successdata(databaseObj);
	}

	@ApiOperation("查询")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "itemCategoryDTO", value = "应用查询参数", required = false, dataType = "ItemCategoryDTO", paramType="body"),
		@ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
		@ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
	})
	@PostMapping(value = "/query")
	public ResponseVO<ItemCategoryDTO>query(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												@RequestBody ItemCategoryDTO itemCategoryDTO,
												@RequestParam Integer pageNo, @RequestParam Integer pageSize,
												@RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
												@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		Map<Object, Object> map = BeanCopyUtilss.beanToMap(itemCategoryDTO, loginedUserVO);
		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			map.put("page", newPage);
		}
		List<ItemCategory> list = itemCategoryService.selectListByMap(map);

		//
		List<ItemCategoryDTO> dtoList = toDTO(list);

		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			int countNum = itemCategoryService.selectCountByMap(map);
			newPage.setCount(countNum);
			return ResponseVO.successdatas(dtoList, newPage);
		} else {
			return ResponseVO.successdatas(dtoList, null);
		}

	}

	@ApiOperation("产品类别树形结构")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String")
	})
	@RequestMapping(value = "/treejson", method = {RequestMethod.GET})
	public ResponseVO<List<ItemCategoryTreeNodeVO>> treeJSON(@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO) {
		//  全局默认组织类别的顶层编码为 -1 
		String globalParentId = "-1";
		return ResponseVO.successdatas(itemCategoryService.itemCategoryTreeJson(loginedUserVO != null ? loginedUserVO.getClientLicenseId() : "", globalParentId), null);
	}

	private List<ItemCategoryDTO> toDTO(List<ItemCategory> databaseList) {
		List<ItemCategoryDTO> returnList = new ArrayList<ItemCategoryDTO>();
		if(databaseList != null) {
			for(ItemCategory dbEntity : databaseList) {
				ItemCategoryDTO dtoTemp = new ItemCategoryDTO();
				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
				returnList.add(dtoTemp);
			}
		}
		return returnList;
	}

	private List<ItemCategory> toDB(List<ItemCategoryDTO> dtoList) {
		List<ItemCategory> returnList = new ArrayList<ItemCategory>();
		if(dtoList != null) {
			for(ItemCategoryDTO dtoEntity : dtoList) {
				ItemCategory dbTemp = new ItemCategory();
				BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
				returnList.add(dbTemp);
			}
		}
		return returnList;
	}
}
