package com.pap.item.category.controller;

import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.item.category.entity.ItemCategoryAttrValue;
import com.pap.item.category.service.IItemCategoryAttrValueService;
import com.pap.item.dto.ItemCategoryAttrValueDTO;
import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.page.Page;
import com.pap.obj.vo.response.ResponseVO;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


//@RestController
//@RequestMapping("/itemcategoryattrvalue")
//@Api(value = "", tags = "", description="")
@Deprecated
public class ItemCategoryAttrValueController {

	private static Logger logger  =  LoggerFactory.getLogger(ItemCategoryAttrValueController.class);
	
	@Resource(name = "itemCategoryAttrValueService")
	private IItemCategoryAttrValueService itemCategoryAttrValueService;

	@Resource(name = "idWorker")
	private IdWorker idWorker;

	@ApiOperation("创建")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "itemCategoryAttrValueDTO", value = "", required = true, dataType = "ItemCategoryAttrValueDTO", paramType="body")
	})
	@PostMapping(value = "")
	public ResponseVO<ItemCategoryAttrValueDTO> create(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													   @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													   @RequestBody ItemCategoryAttrValueDTO itemCategoryAttrValueDTO){
		String idStr = idWorker.nextIdStr();
		itemCategoryAttrValueDTO.setItemCategoryAttrValueId(idStr);

		ItemCategoryAttrValue databaseObj = new ItemCategoryAttrValue();
		BeanCopyUtilss.myCopyProperties(itemCategoryAttrValueDTO, databaseObj);

		int i = itemCategoryAttrValueService.insertSelective(databaseObj);

		return ResponseVO.successdata(itemCategoryAttrValueDTO);
	}

	@ApiOperation("更新")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "itemCategoryAttrValueDTO", value = "", required = true, dataType = "ItemCategoryAttrValueDTO", paramType="body")
	})
	@PutMapping(value = "")
	public ResponseVO<ItemCategoryAttrValueDTO> update(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody ItemCategoryAttrValueDTO itemCategoryAttrValueDTO){

		ItemCategoryAttrValue databaseObj = new ItemCategoryAttrValue();
		BeanCopyUtilss.myCopyProperties(itemCategoryAttrValueDTO, databaseObj);
		int i = itemCategoryAttrValueService.updateByPrimaryKeySelective(databaseObj);

		databaseObj = itemCategoryAttrValueService.selectByPrimaryKey(databaseObj.getItemCategoryAttrValueId());

		return ResponseVO.successdata(databaseObj);
	}


	@ApiOperation("删除")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "itemCategoryAttrValueDTO", value = "", required = true, dataType = "ItemCategoryAttrValueDTO", paramType="body")
	})
	@DeleteMapping(value = "")
	public ResponseVO<String> delete(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody ItemCategoryAttrValueDTO itemCategoryAttrValueDTO){

		ItemCategoryAttrValue databaseObj = new ItemCategoryAttrValue();
		BeanCopyUtilss.myCopyProperties(itemCategoryAttrValueDTO, databaseObj);

		int i = itemCategoryAttrValueService.deleteByPrimaryKey(databaseObj.getItemCategoryAttrValueId());

		return ResponseVO.successdata("操作成功");
	}

	@ApiOperation("详情")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "id", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/{id}")
	public ResponseVO<ItemCategoryAttrValueDTO> selectOne(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@PathVariable("id") String id){

		ItemCategoryAttrValue databaseObj = itemCategoryAttrValueService.selectByPrimaryKey(id);

		return ResponseVO.successdata(databaseObj);
	}

	@ApiOperation("查询")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "itemCategoryAttrValueDTO", value = "应用查询参数", required = false, dataType = "ItemCategoryAttrValueDTO", paramType="body"),
		@ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
		@ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
	})
	@PostMapping(value = "/query")
	public ResponseVO<ItemCategoryAttrValueDTO>query(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												@RequestBody ItemCategoryAttrValueDTO itemCategoryAttrValueDTO,
												@RequestParam Integer pageNo, @RequestParam Integer pageSize,
												@RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
												 @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		Map<Object, Object> map = BeanCopyUtilss.beanToMap(itemCategoryAttrValueDTO, loginedUserVO);
		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			map.put("page", newPage);
		}
		List<ItemCategoryAttrValue> list = itemCategoryAttrValueService.selectListByMap(map);

		//
		List<ItemCategoryAttrValueDTO> dtoList = toDTO(list);

		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			int countNum = itemCategoryAttrValueService.selectCountByMap(map);
			newPage.setCount(countNum);
			return ResponseVO.successdatas(dtoList, newPage);
		} else {
			return ResponseVO.successdatas(dtoList, null);
		}

	}

	private List<ItemCategoryAttrValueDTO> toDTO(List<ItemCategoryAttrValue> databaseList) {
		List<ItemCategoryAttrValueDTO> returnList = new ArrayList<ItemCategoryAttrValueDTO>();
		if(databaseList != null) {
			for(ItemCategoryAttrValue dbEntity : databaseList) {
				ItemCategoryAttrValueDTO dtoTemp = new ItemCategoryAttrValueDTO();
				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
				returnList.add(dtoTemp);
			}
		}
		return returnList;
	}

	private List<ItemCategoryAttrValue> toDB(List<ItemCategoryAttrValueDTO> dtoList) {
		List<ItemCategoryAttrValue> returnList = new ArrayList<ItemCategoryAttrValue>();
		if(dtoList != null) {
			for(ItemCategoryAttrValueDTO dtoEntity : dtoList) {
				ItemCategoryAttrValue dbTemp = new ItemCategoryAttrValue();
				BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
				returnList.add(dbTemp);
			}
		}
		return returnList;
	}
}
