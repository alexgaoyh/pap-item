package com.pap.item.category.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.item.category.entity.ItemCategoryAttrValue;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;;

@Mapper
public interface ItemCategoryAttrValueMapper extends PapBaseMapper<ItemCategoryAttrValue> {
    int deleteByPrimaryKey(String itemCategoryAttrValueId);

    int selectCountByMap(Map<Object, Object> map);

    List<ItemCategoryAttrValue> selectListByMap(Map<Object, Object> map);

    ItemCategoryAttrValue selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(ItemCategoryAttrValue record);

    int insertSelective(ItemCategoryAttrValue record);

    ItemCategoryAttrValue selectByPrimaryKey(String itemCategoryAttrValueId);

    int updateByPrimaryKeySelective(ItemCategoryAttrValue record);

    int updateByPrimaryKey(ItemCategoryAttrValue record);

    // alexgaoyh added

    /**
     * 根据 扩展属性 删除明细信息
     * @param itemCategoryAttrKeyId
     * @return
     */
    int deleteByItemCategoryAttrKeyId(@Param("itemCategoryAttrKeyId") String itemCategoryAttrKeyId);
}