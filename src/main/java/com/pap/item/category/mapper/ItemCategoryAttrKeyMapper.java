package com.pap.item.category.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.item.category.entity.ItemCategoryAttrKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ItemCategoryAttrKeyMapper extends PapBaseMapper<ItemCategoryAttrKey> {
    int deleteByPrimaryKey(String itemCategoryAttrKeyId);

    int selectCountByMap(Map<Object, Object> map);

    List<ItemCategoryAttrKey> selectListByMap(Map<Object, Object> map);

    ItemCategoryAttrKey selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(ItemCategoryAttrKey record);

    int insertSelective(ItemCategoryAttrKey record);

    ItemCategoryAttrKey selectByPrimaryKey(String itemCategoryAttrKeyId);

    int updateByPrimaryKeySelective(ItemCategoryAttrKey record);

    int updateByPrimaryKey(ItemCategoryAttrKey record);
}