package com.pap.item.category.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.item.category.entity.ItemCategory;
import java.util.List;
import java.util.Map;

import com.pap.item.dto.ItemCategoryTreeNodeVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;;

@Mapper
public interface ItemCategoryMapper extends PapBaseMapper<ItemCategory> {
    int deleteByPrimaryKey(String itemCategoryId);

    int selectCountByMap(Map<Object, Object> map);

    List<ItemCategory> selectListByMap(Map<Object, Object> map);

    ItemCategory selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(ItemCategory record);

    int insertSelective(ItemCategory record);

    ItemCategory selectByPrimaryKey(String itemCategoryId);

    int updateByPrimaryKeySelective(ItemCategory record);

    int updateByPrimaryKey(ItemCategory record);


    // add alexgaoyh
    List<ItemCategoryTreeNodeVO> itemCategoryTreeJson(@Param("clientLicenseId") String clientLicenseId,
                                                                 @Param("globalParentId") String globalParentId);
}