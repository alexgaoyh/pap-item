package com.pap.item.category.entity;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.*;
import java.io.Serializable;

@MyBatisTableAnnotation(name = "t_item_category_attr_value", namespace = "com.pap.item.category.mapper.ItemCategoryAttrValueMapper", remarks = " 修改点 ", aliasName = "t_item_category_attr_value t_item_category_attr_value" )
public class ItemCategoryAttrValue extends PapBaseEntity implements Serializable {
    /**
     *  商品类目编码,所属表字段为t_item_category_attr_value.ITEM_CATEGORY_ATTR_VALUE_ID
     */
    @MyBatisColumnAnnotation(name = "ITEM_CATEGORY_ATTR_VALUE_ID", value = "t_item_category_attr_value_ITEM_CATEGORY_ATTR_VALUE_ID", chineseNote = "商品类目编码", tableAlias = "t_item_category_attr_value")
    @MyApiModelPropertyAnnotation(value = "商品类目编码")
    private String itemCategoryAttrValueId;

    /**
     *  商品类目名称,所属表字段为t_item_category_attr_value.ITEM_CATEGORY_ATTR_VALUE_NAME
     */
    @MyBatisColumnAnnotation(name = "ITEM_CATEGORY_ATTR_VALUE_NAME", value = "t_item_category_attr_value_ITEM_CATEGORY_ATTR_VALUE_NAME", chineseNote = "商品类目名称", tableAlias = "t_item_category_attr_value")
    @MyApiModelPropertyAnnotation(value = "商品类目名称")
    private String itemCategoryAttrValueName;

    /**
     *  所属分类扩展属性值,所属表字段为t_item_category_attr_value.ITEM_CATEGORY_ATTR_KEY_ID
     */
    @MyBatisColumnAnnotation(name = "ITEM_CATEGORY_ATTR_KEY_ID", value = "t_item_category_attr_value_ITEM_CATEGORY_ATTR_KEY_ID", chineseNote = "所属分类扩展属性值", tableAlias = "t_item_category_attr_value")
    @MyApiModelPropertyAnnotation(value = "所属分类扩展属性值")
    private String itemCategoryAttrKeyId;

    /**
     *  所属分类,所属表字段为t_item_category_attr_value.ITEM_CATEGORY_ID
     */
    @MyBatisColumnAnnotation(name = "ITEM_CATEGORY_ID", value = "t_item_category_attr_value_ITEM_CATEGORY_ID", chineseNote = "所属分类", tableAlias = "t_item_category_attr_value")
    @MyApiModelPropertyAnnotation(value = "所属分类")
    private String itemCategoryId;

    /**
     *  备注,所属表字段为t_item_category_attr_value.REMARK
     */
    @MyBatisColumnAnnotation(name = "REMARK", value = "t_item_category_attr_value_REMARK", chineseNote = "备注", tableAlias = "t_item_category_attr_value")
    @MyApiModelPropertyAnnotation(value = "备注")
    private String remark;

    /**
     *  排序号,所属表字段为t_item_category_attr_value.SEQ_NO
     */
    @MyBatisColumnAnnotation(name = "SEQ_NO", value = "t_item_category_attr_value_SEQ_NO", chineseNote = "排序号", tableAlias = "t_item_category_attr_value")
    @MyApiModelPropertyAnnotation(value = "排序号")
    private Integer seqNo;

    @Override
    public String getDynamicTableName() {
        return "t_item_category_attr_value";
    }

    private static final long serialVersionUID = 1L;

    public String getItemCategoryAttrValueId() {
        return itemCategoryAttrValueId;
    }

    public void setItemCategoryAttrValueId(String itemCategoryAttrValueId) {
        this.itemCategoryAttrValueId = itemCategoryAttrValueId;
    }

    public String getItemCategoryAttrValueName() {
        return itemCategoryAttrValueName;
    }

    public void setItemCategoryAttrValueName(String itemCategoryAttrValueName) {
        this.itemCategoryAttrValueName = itemCategoryAttrValueName;
    }

    public String getItemCategoryAttrKeyId() {
        return itemCategoryAttrKeyId;
    }

    public void setItemCategoryAttrKeyId(String itemCategoryAttrKeyId) {
        this.itemCategoryAttrKeyId = itemCategoryAttrKeyId;
    }

    public String getItemCategoryId() {
        return itemCategoryId;
    }

    public void setItemCategoryId(String itemCategoryId) {
        this.itemCategoryId = itemCategoryId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(Integer seqNo) {
        this.seqNo = seqNo;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", itemCategoryAttrValueId=").append(itemCategoryAttrValueId);
        sb.append(", itemCategoryAttrValueName=").append(itemCategoryAttrValueName);
        sb.append(", itemCategoryAttrKeyId=").append(itemCategoryAttrKeyId);
        sb.append(", itemCategoryId=").append(itemCategoryId);
        sb.append(", remark=").append(remark);
        sb.append(", seqNo=").append(seqNo);
        sb.append("]");
        return sb.toString();
    }
}