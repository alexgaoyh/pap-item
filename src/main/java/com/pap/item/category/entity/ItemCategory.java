package com.pap.item.category.entity;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.*;
import java.io.Serializable;

@MyBatisTableAnnotation(name = "t_item_category", namespace = "com.pap.item.category.mapper.ItemCategoryMapper", remarks = " 修改点 ", aliasName = "t_item_category t_item_category" )
public class ItemCategory extends PapBaseEntity implements Serializable {
    /**
     *  商品类目编号,所属表字段为t_item_category.ITEM_CATEGORY_ID
     */
    @MyBatisColumnAnnotation(name = "ITEM_CATEGORY_ID", value = "t_item_category_ITEM_CATEGORY_ID", chineseNote = "商品类目编号", tableAlias = "t_item_category")
    @MyApiModelPropertyAnnotation(value = "商品类目编号")
    private String itemCategoryId;

    /**
     *  商品类目编码,所属表字段为t_item_category.ITEM_CATEGORY_CODE
     */
    @MyBatisColumnAnnotation(name = "ITEM_CATEGORY_CODE", value = "t_item_category_ITEM_CATEGORY_CODE", chineseNote = "商品类目编码", tableAlias = "t_item_category")
    @MyApiModelPropertyAnnotation(value = "商品类目编码")
    private String itemCategoryCode;

    /**
     *  商品类目名称,所属表字段为t_item_category.ITEM_CATEGORY_NAME
     */
    @MyBatisColumnAnnotation(name = "ITEM_CATEGORY_NAME", value = "t_item_category_ITEM_CATEGORY_NAME", chineseNote = "商品类目名称", tableAlias = "t_item_category")
    @MyApiModelPropertyAnnotation(value = "商品类目名称")
    private String itemCategoryName;

    /**
     *  父级商品类目编号,所属表字段为t_item_category.ITEM_CATEGORY_PARENT_ID
     */
    @MyBatisColumnAnnotation(name = "ITEM_CATEGORY_PARENT_ID", value = "t_item_category_ITEM_CATEGORY_PARENT_ID", chineseNote = "父级商品类目编号", tableAlias = "t_item_category")
    @MyApiModelPropertyAnnotation(value = "父级商品类目编号")
    private String itemCategoryParentId;

    /**
     *  编号路径集合,所属表字段为t_item_category.PATH_IDS
     */
    @MyBatisColumnAnnotation(name = "PATH_IDS", value = "t_item_category_PATH_IDS", chineseNote = "编号路径集合", tableAlias = "t_item_category")
    @MyApiModelPropertyAnnotation(value = "编号路径集合")
    private String pathIds;

    /**
     *  业务编码路径集合,所属表字段为t_item_category.PATH_CODES
     */
    @MyBatisColumnAnnotation(name = "PATH_CODES", value = "t_item_category_PATH_CODES", chineseNote = "业务编码路径集合", tableAlias = "t_item_category")
    @MyApiModelPropertyAnnotation(value = "业务编码路径集合")
    private String pathCodes;

    /**
     *  名称路径集合,所属表字段为t_item_category.PATH_NAMES
     */
    @MyBatisColumnAnnotation(name = "PATH_NAMES", value = "t_item_category_PATH_NAMES", chineseNote = "名称路径集合", tableAlias = "t_item_category")
    @MyApiModelPropertyAnnotation(value = "名称路径集合")
    private String pathNames;

    /**
     *  附件顺序(由上传方维护),所属表字段为t_item_category.SEQ_NO
     */
    @MyBatisColumnAnnotation(name = "SEQ_NO", value = "t_item_category_SEQ_NO", chineseNote = "附件顺序(由上传方维护)", tableAlias = "t_item_category")
    @MyApiModelPropertyAnnotation(value = "附件顺序(由上传方维护)")
    private Integer seqNo;

    @Override
    public String getDynamicTableName() {
        return "t_item_category";
    }

    private static final long serialVersionUID = 1L;

    public String getItemCategoryId() {
        return itemCategoryId;
    }

    public void setItemCategoryId(String itemCategoryId) {
        this.itemCategoryId = itemCategoryId;
    }

    public String getItemCategoryCode() {
        return itemCategoryCode;
    }

    public void setItemCategoryCode(String itemCategoryCode) {
        this.itemCategoryCode = itemCategoryCode;
    }

    public String getItemCategoryName() {
        return itemCategoryName;
    }

    public void setItemCategoryName(String itemCategoryName) {
        this.itemCategoryName = itemCategoryName;
    }

    public String getItemCategoryParentId() {
        return itemCategoryParentId;
    }

    public void setItemCategoryParentId(String itemCategoryParentId) {
        this.itemCategoryParentId = itemCategoryParentId;
    }

    public String getPathIds() {
        return pathIds;
    }

    public void setPathIds(String pathIds) {
        this.pathIds = pathIds;
    }

    public String getPathCodes() {
        return pathCodes;
    }

    public void setPathCodes(String pathCodes) {
        this.pathCodes = pathCodes;
    }

    public String getPathNames() {
        return pathNames;
    }

    public void setPathNames(String pathNames) {
        this.pathNames = pathNames;
    }

    public Integer getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(Integer seqNo) {
        this.seqNo = seqNo;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", itemCategoryId=").append(itemCategoryId);
        sb.append(", itemCategoryCode=").append(itemCategoryCode);
        sb.append(", itemCategoryName=").append(itemCategoryName);
        sb.append(", itemCategoryParentId=").append(itemCategoryParentId);
        sb.append(", pathIds=").append(pathIds);
        sb.append(", pathCodes=").append(pathCodes);
        sb.append(", pathNames=").append(pathNames);
        sb.append(", seqNo=").append(seqNo);
        sb.append("]");
        return sb.toString();
    }
}