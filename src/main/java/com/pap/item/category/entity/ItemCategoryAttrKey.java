package com.pap.item.category.entity;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.*;
import java.io.Serializable;

@MyBatisTableAnnotation(name = "t_item_category_attr_key", namespace = "com.pap.item.category.mapper.ItemCategoryAttrKeyMapper", remarks = " 修改点 ", aliasName = "t_item_category_attr_key t_item_category_attr_key" )
public class ItemCategoryAttrKey extends PapBaseEntity implements Serializable {
    /**
     *  商品类目编码,所属表字段为t_item_category_attr_key.ITEM_CATEGORY_ATTR_KEY_ID
     */
    @MyBatisColumnAnnotation(name = "ITEM_CATEGORY_ATTR_KEY_ID", value = "t_item_category_attr_key_ITEM_CATEGORY_ATTR_KEY_ID", chineseNote = "商品类目编码", tableAlias = "t_item_category_attr_key")
    @MyApiModelPropertyAnnotation(value = "商品类目编码")
    private String itemCategoryAttrKeyId;

    /**
     *  商品类目名称,所属表字段为t_item_category_attr_key.ITEM_CATEGORY_ATTR_KEY_NAME
     */
    @MyBatisColumnAnnotation(name = "ITEM_CATEGORY_ATTR_KEY_NAME", value = "t_item_category_attr_key_ITEM_CATEGORY_ATTR_KEY_NAME", chineseNote = "商品类目名称", tableAlias = "t_item_category_attr_key")
    @MyApiModelPropertyAnnotation(value = "商品类目名称")
    private String itemCategoryAttrKeyName;

    /**
     *  商品类目编号,所属表字段为t_item_category_attr_key.ITEM_CATEGORY_ID
     */
    @MyBatisColumnAnnotation(name = "ITEM_CATEGORY_ID", value = "t_item_category_attr_key_ITEM_CATEGORY_ID", chineseNote = "商品类目编号", tableAlias = "t_item_category_attr_key")
    @MyApiModelPropertyAnnotation(value = "商品类目编号")
    private String itemCategoryId;

    /**
     *  是否允许别名,所属表字段为t_item_category_attr_key.ALAIS_FLAG
     */
    @MyBatisColumnAnnotation(name = "ALAIS_FLAG", value = "t_item_category_attr_key_ALAIS_FLAG", chineseNote = "是否允许别名", tableAlias = "t_item_category_attr_key")
    @MyApiModelPropertyAnnotation(value = "是否允许别名")
    private String alaisFlag;

    /**
     *  枚举属性(N选1),所属表字段为t_item_category_attr_key.ENUM_FLAG
     */
    @MyBatisColumnAnnotation(name = "ENUM_FLAG", value = "t_item_category_attr_key_ENUM_FLAG", chineseNote = "枚举属性(N选1)", tableAlias = "t_item_category_attr_key")
    @MyApiModelPropertyAnnotation(value = "枚举属性(N选1)")
    private String enumFlag;

    /**
     *  多选属性(N选K),所属表字段为t_item_category_attr_key.MULTIPLE_FLAG
     */
    @MyBatisColumnAnnotation(name = "MULTIPLE_FLAG", value = "t_item_category_attr_key_MULTIPLE_FLAG", chineseNote = "多选属性(N选K)", tableAlias = "t_item_category_attr_key")
    @MyApiModelPropertyAnnotation(value = "多选属性(N选K)")
    private String multipleFlag;

    /**
     *  输入属性(允许输入),所属表字段为t_item_category_attr_key.INPUT_FLAG
     */
    @MyBatisColumnAnnotation(name = "INPUT_FLAG", value = "t_item_category_attr_key_INPUT_FLAG", chineseNote = "输入属性(允许输入)", tableAlias = "t_item_category_attr_key")
    @MyApiModelPropertyAnnotation(value = "输入属性(允许输入)")
    private String inputFlag;

    /**
     *  关键属性(确定货品的关键属性),所属表字段为t_item_category_attr_key.KEY_FLAG
     */
    @MyBatisColumnAnnotation(name = "KEY_FLAG", value = "t_item_category_attr_key_KEY_FLAG", chineseNote = "关键属性(确定货品的关键属性)", tableAlias = "t_item_category_attr_key")
    @MyApiModelPropertyAnnotation(value = "关键属性(确定货品的关键属性)")
    private String keyFlag;

    /**
     *  销售属性(扩展确定SKU),所属表字段为t_item_category_attr_key.SALE_FLAG
     */
    @MyBatisColumnAnnotation(name = "SALE_FLAG", value = "t_item_category_attr_key_SALE_FLAG", chineseNote = "销售属性(扩展确定SKU)", tableAlias = "t_item_category_attr_key")
    @MyApiModelPropertyAnnotation(value = "销售属性(扩展确定SKU)")
    private String saleFlag;

    /**
     *  非关键属性(属性扩展便于明确信息),所属表字段为t_item_category_attr_key.NO_KEY_FLAG
     */
    @MyBatisColumnAnnotation(name = "NO_KEY_FLAG", value = "t_item_category_attr_key_NO_KEY_FLAG", chineseNote = "非关键属性(属性扩展便于明确信息)", tableAlias = "t_item_category_attr_key")
    @MyApiModelPropertyAnnotation(value = "非关键属性(属性扩展便于明确信息)")
    private String noKeyFlag;

    /**
     *  是否必选(必须选择确定),所属表字段为t_item_category_attr_key.REQUIRED_FLAG
     */
    @MyBatisColumnAnnotation(name = "REQUIRED_FLAG", value = "t_item_category_attr_key_REQUIRED_FLAG", chineseNote = "是否必选(必须选择确定)", tableAlias = "t_item_category_attr_key")
    @MyApiModelPropertyAnnotation(value = "是否必选(必须选择确定)")
    private String requiredFlag;

    /**
     *  是否搜索属性,所属表字段为t_item_category_attr_key.SEARCH_FLAG
     */
    @MyBatisColumnAnnotation(name = "SEARCH_FLAG", value = "t_item_category_attr_key_SEARCH_FLAG", chineseNote = "是否搜索属性", tableAlias = "t_item_category_attr_key")
    @MyApiModelPropertyAnnotation(value = "是否搜索属性")
    private String searchFlag;

    /**
     *  备注,所属表字段为t_item_category_attr_key.REMARK
     */
    @MyBatisColumnAnnotation(name = "REMARK", value = "t_item_category_attr_key_REMARK", chineseNote = "备注", tableAlias = "t_item_category_attr_key")
    @MyApiModelPropertyAnnotation(value = "备注")
    private String remark;

    /**
     *  排序号,所属表字段为t_item_category_attr_key.SEQ_NO
     */
    @MyBatisColumnAnnotation(name = "SEQ_NO", value = "t_item_category_attr_key_SEQ_NO", chineseNote = "排序号", tableAlias = "t_item_category_attr_key")
    @MyApiModelPropertyAnnotation(value = "排序号")
    private Integer seqNo;

    @Override
    public String getDynamicTableName() {
        return "t_item_category_attr_key";
    }

    private static final long serialVersionUID = 1L;

    public String getItemCategoryAttrKeyId() {
        return itemCategoryAttrKeyId;
    }

    public void setItemCategoryAttrKeyId(String itemCategoryAttrKeyId) {
        this.itemCategoryAttrKeyId = itemCategoryAttrKeyId;
    }

    public String getItemCategoryAttrKeyName() {
        return itemCategoryAttrKeyName;
    }

    public void setItemCategoryAttrKeyName(String itemCategoryAttrKeyName) {
        this.itemCategoryAttrKeyName = itemCategoryAttrKeyName;
    }

    public String getItemCategoryId() {
        return itemCategoryId;
    }

    public void setItemCategoryId(String itemCategoryId) {
        this.itemCategoryId = itemCategoryId;
    }

    public String getAlaisFlag() {
        return alaisFlag;
    }

    public void setAlaisFlag(String alaisFlag) {
        this.alaisFlag = alaisFlag;
    }

    public String getEnumFlag() {
        return enumFlag;
    }

    public void setEnumFlag(String enumFlag) {
        this.enumFlag = enumFlag;
    }

    public String getMultipleFlag() {
        return multipleFlag;
    }

    public void setMultipleFlag(String multipleFlag) {
        this.multipleFlag = multipleFlag;
    }

    public String getInputFlag() {
        return inputFlag;
    }

    public void setInputFlag(String inputFlag) {
        this.inputFlag = inputFlag;
    }

    public String getKeyFlag() {
        return keyFlag;
    }

    public void setKeyFlag(String keyFlag) {
        this.keyFlag = keyFlag;
    }

    public String getSaleFlag() {
        return saleFlag;
    }

    public void setSaleFlag(String saleFlag) {
        this.saleFlag = saleFlag;
    }

    public String getNoKeyFlag() {
        return noKeyFlag;
    }

    public void setNoKeyFlag(String noKeyFlag) {
        this.noKeyFlag = noKeyFlag;
    }

    public String getRequiredFlag() {
        return requiredFlag;
    }

    public void setRequiredFlag(String requiredFlag) {
        this.requiredFlag = requiredFlag;
    }

    public String getSearchFlag() {
        return searchFlag;
    }

    public void setSearchFlag(String searchFlag) {
        this.searchFlag = searchFlag;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(Integer seqNo) {
        this.seqNo = seqNo;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", itemCategoryAttrKeyId=").append(itemCategoryAttrKeyId);
        sb.append(", itemCategoryAttrKeyName=").append(itemCategoryAttrKeyName);
        sb.append(", itemCategoryId=").append(itemCategoryId);
        sb.append(", alaisFlag=").append(alaisFlag);
        sb.append(", enumFlag=").append(enumFlag);
        sb.append(", multipleFlag=").append(multipleFlag);
        sb.append(", inputFlag=").append(inputFlag);
        sb.append(", keyFlag=").append(keyFlag);
        sb.append(", saleFlag=").append(saleFlag);
        sb.append(", noKeyFlag=").append(noKeyFlag);
        sb.append(", requiredFlag=").append(requiredFlag);
        sb.append(", searchFlag=").append(searchFlag);
        sb.append(", remark=").append(remark);
        sb.append(", seqNo=").append(seqNo);
        sb.append("]");
        return sb.toString();
    }
}