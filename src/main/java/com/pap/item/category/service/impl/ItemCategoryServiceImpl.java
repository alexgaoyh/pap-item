package com.pap.item.category.service.impl;

import com.pap.base.util.string.StringUtilss;
import com.pap.item.category.mapper.ItemCategoryMapper;
import com.pap.item.dto.ItemCategoryTreeNodeVO;
import com.pap.obj.vo.response.ResponseVO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pap.item.category.entity.ItemCategory;
import com.pap.item.category.service.IItemCategoryService;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional
@Service("itemCategoryService")
public class ItemCategoryServiceImpl implements IItemCategoryService {

    @Resource(name = "itemCategoryMapper")
    private ItemCategoryMapper mapper;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<ItemCategory> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(ItemCategory record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(ItemCategory record) {
       return mapper.insertSelective(record);
    }

    @Override
    public ItemCategory selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(ItemCategory record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(ItemCategory record) {
      return mapper.updateByPrimaryKey(record);
    }

    @Override
    public List<ItemCategoryTreeNodeVO> itemCategoryTreeJson(String clientLicenseId, String globalParentId) {
        return mapper.itemCategoryTreeJson(clientLicenseId, globalParentId);
    }

    @Override
    public Boolean checkTreeCircularDependency(String operaItemCategoryId, String newParentItemCategoryId) {
        String parentItemCategoryTemp = "";
        ItemCategory newParentItemCategoryInfo = mapper.selectByPrimaryKey(newParentItemCategoryId);
        if(newParentItemCategoryInfo != null) {
            if(StringUtilss.isNotEmpty(newParentItemCategoryInfo.getPathIds())) {
                parentItemCategoryTemp = newParentItemCategoryInfo.getPathIds() + ",";
            }
            parentItemCategoryTemp += newParentItemCategoryInfo.getItemCategoryId();
        }
        if (StringUtilss.checkArrayValue(parentItemCategoryTemp.split(","), operaItemCategoryId)) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public ResponseVO<ItemCategory> updateAndCheckItemCategory(ItemCategory inputItemCategory) {
        // 检验是否循环依赖
        if(checkTreeCircularDependency(inputItemCategory.getItemCategoryId(), inputItemCategory.getItemCategoryParentId())) {

            // 忽略前台传递过来的数据
            inputItemCategory.setPathIds(null);
            inputItemCategory.setPathCodes(null);
            inputItemCategory.setPathNames(null);
            mapper.updateByPrimaryKeySelective(inputItemCategory);

            ItemCategory tempForPath = mapper.selectByPrimaryKey(inputItemCategory.getItemCategoryId());
            if("-1".equals(inputItemCategory.getItemCategoryParentId())) {
                tempForPath.setPathCodes(null);
                tempForPath.setPathIds(null);
                tempForPath.setPathNames(null);

                tempForPath.setItemCategoryParentId("-1");
                mapper.updateByPrimaryKey(tempForPath);
            } else {
                // 查询父节点信息，处理当前节点的冗余字段
                ItemCategory tempParentForPath = mapper.selectByPrimaryKey(inputItemCategory.getItemCategoryParentId());
                if(tempParentForPath != null) {
                    if(StringUtilss.isNotEmpty(tempParentForPath.getPathIds())) {
                        tempForPath.setPathIds(tempParentForPath.getPathIds() + "," + tempParentForPath.getItemCategoryId());
                    } else {
                        tempForPath.setPathIds(tempParentForPath.getItemCategoryId());
                    }
                    if(StringUtilss.isNotEmpty(tempParentForPath.getPathCodes())) {
                        tempForPath.setPathCodes(tempParentForPath.getPathCodes() + "," + tempParentForPath.getItemCategoryCode());
                    } else {
                        tempForPath.setPathCodes(tempParentForPath.getItemCategoryCode());
                    }
                    if(StringUtilss.isNotEmpty(tempParentForPath.getPathNames())) {
                        tempForPath.setPathNames(tempParentForPath.getPathNames() + "," + tempParentForPath.getItemCategoryName());
                    } else {
                        tempForPath.setPathNames(tempParentForPath.getItemCategoryName());
                    }
                } else {
                    tempForPath.setItemCategoryParentId("-1");
                    tempForPath.setPathCodes(null);
                    tempForPath.setPathIds(null);
                    tempForPath.setPathNames(null);
                }

                mapper.updateByPrimaryKey(tempForPath);
            }

            // 维护子集的商品类别
            ResponseVO<ItemCategory> responseVO =  updateRecursionPathColumnItemCategory(inputItemCategory.getItemCategoryId());
            return responseVO;
        } else {
            return ResponseVO.validfail("商品类别被循环依赖，请检查数据有效性!");
        }
    }

    @Override
    public ResponseVO<ItemCategory> updateRecursionPathColumnItemCategory(String inputItemCategoryId) {
        try {
            Map<Object, Object> parentIdMap = new HashMap<Object, Object>();
            parentIdMap.put("itemCategoryParentId", inputItemCategoryId);
            List<ItemCategory> list = mapper.selectListByMap(parentIdMap);
            if (null != list && list.size()>0) {
                for (int i = 0; i < list.size(); i++) {
                    ItemCategory tempForPath = list.get(i);
                    // 更新数据
                    ItemCategory tempParentForPath = mapper.selectByPrimaryKey(tempForPath.getItemCategoryParentId());
                    if(tempParentForPath != null) {
                        if(StringUtilss.isNotEmpty(tempParentForPath.getPathIds())) {
                            tempForPath.setPathIds(tempParentForPath.getPathIds() + "," + tempParentForPath.getItemCategoryId());
                        } else {
                            tempForPath.setPathIds(tempParentForPath.getItemCategoryId());
                        }
                        if(StringUtilss.isNotEmpty(tempParentForPath.getPathCodes())) {
                            tempForPath.setPathCodes(tempParentForPath.getPathCodes() + "," + tempParentForPath.getItemCategoryCode());
                        } else {
                            tempForPath.setPathCodes(tempParentForPath.getItemCategoryCode());
                        }
                        if(StringUtilss.isNotEmpty(tempParentForPath.getPathNames())) {
                            tempForPath.setPathNames(tempParentForPath.getPathNames() + "," + tempParentForPath.getItemCategoryName());
                        } else {
                            tempForPath.setPathNames(tempParentForPath.getItemCategoryName());
                        }

                    } else {
                        tempForPath.setPathCodes(null);
                        tempForPath.setPathIds(null);
                        tempForPath.setPathNames(null);
                    }
                    mapper.updateByPrimaryKeySelective(tempForPath);
                    updateRecursionPathColumnItemCategory(tempForPath.getItemCategoryId());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseVO.validfail(e.getMessage());
        }
        return ResponseVO.successdata("更新成功");
    }
}