package com.pap.item.category.service;

import com.pap.item.category.entity.ItemCategoryAttrValue;

import java.util.List;
import java.util.Map;

public interface IItemCategoryAttrValueService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<ItemCategoryAttrValue> selectListByMap(Map<Object, Object> map);

    int insert(ItemCategoryAttrValue record);

    int insertSelective(ItemCategoryAttrValue record);

    ItemCategoryAttrValue selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ItemCategoryAttrValue record);

    int updateByPrimaryKey(ItemCategoryAttrValue record);
}
