package com.pap.item.category.service;

import com.pap.item.category.entity.ItemCategoryAttrKey;
import com.pap.item.dto.ItemCategoryAttrKeyDTO;
import com.pap.obj.vo.logineduser.LoginedUserVO;

import java.util.List;
import java.util.Map;

public interface IItemCategoryAttrKeyService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<ItemCategoryAttrKey> selectListByMap(Map<Object, Object> map);

    int insert(ItemCategoryAttrKey record);

    int insertSelective(ItemCategoryAttrKey record);

    ItemCategoryAttrKey selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ItemCategoryAttrKey record);

    int updateByPrimaryKey(ItemCategoryAttrKey record);

    /**
     * 保存商品类别扩展属性明细相关数据
     * @param operObj
     * @return
     */
    @Deprecated
    String saveItemCategoryAttrKeyWithDetailInfo(ItemCategoryAttrKeyDTO operObj);

    /**
     * 保存商品类别扩展属性明细相关数据
     * @param operObjList 类别属性集合信息
     * @param loginedUserVO 用户信息
     * @return
     */
    void saveItemCategoryAttrKeyWithDetailInfo(List<ItemCategoryAttrKeyDTO> operObjList, LoginedUserVO loginedUserVO);

    /**
     * 更新商品类别扩展属性明细相关数据
     * @param operObj
     * @return
     */
    @Deprecated
    String updateItemCategoryAttrKeyWithDetailInfo(ItemCategoryAttrKeyDTO operObj);

    /**
     * 更新商品类别扩展属性明细相关数据
     * @param operObjList 类别属性集合信息
     * @param loginedUserVO 用户信息
     * @return
     */
    void updateItemCategoryAttrKeyWithDetailInfo(List<ItemCategoryAttrKeyDTO> operObjList, LoginedUserVO loginedUserVO);

    /**
     * 删除商品类别扩展属性明细相关数据
     * @param inputItemCategoryAttrKeyId
     * @return
     */
    int deleteItemCategoryAttrKeyWithDetailInfo(String inputItemCategoryAttrKeyId);

    /**
     * 查询商品类别扩展属性明细相关数据
     * @param inputSelectItemCategoryAttrKeyId
     * @return
     */
    ItemCategoryAttrKeyDTO selectItemCategoryAttrKeyWithDetailInfoById(String inputSelectItemCategoryAttrKeyId);

    /**
     * 根据类目编号，查询当前类目下的属性key - value 集合
     * @param itemCategoryId
     * @return
     */
    List<ItemCategoryAttrKeyDTO> selectItemCategoryAttrKeyWithDetailListByItemCategoryId(String itemCategoryId);
}
