package com.pap.item.category.service.impl;

import com.pap.item.category.mapper.ItemCategoryAttrValueMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pap.item.category.entity.ItemCategoryAttrValue;
import com.pap.item.category.service.IItemCategoryAttrValueService;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Transactional
@Service("itemCategoryAttrValueService")
public class ItemCategoryAttrValueServiceImpl implements IItemCategoryAttrValueService {

    @Resource(name = "itemCategoryAttrValueMapper")
    private ItemCategoryAttrValueMapper mapper;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<ItemCategoryAttrValue> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(ItemCategoryAttrValue record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(ItemCategoryAttrValue record) {
       return mapper.insertSelective(record);
    }

    @Override
    public ItemCategoryAttrValue selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(ItemCategoryAttrValue record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(ItemCategoryAttrValue record) {
      return mapper.updateByPrimaryKey(record);
    }
}