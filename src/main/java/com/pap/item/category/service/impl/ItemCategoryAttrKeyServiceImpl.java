package com.pap.item.category.service.impl;

import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.base.util.date.DateUtils;
import com.pap.beans.idworker.IdWorker;
import com.pap.item.category.entity.ItemCategoryAttrKey;
import com.pap.item.category.entity.ItemCategoryAttrValue;
import com.pap.item.category.mapper.ItemCategoryAttrKeyMapper;
import com.pap.item.category.mapper.ItemCategoryAttrValueMapper;
import com.pap.item.category.service.IItemCategoryAttrKeyService;
import com.pap.item.dto.ItemCategoryAttrKeyDTO;
import com.pap.item.dto.ItemCategoryAttrValueDTO;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional
@Service("itemCategoryAttrKeyService")
public class ItemCategoryAttrKeyServiceImpl implements IItemCategoryAttrKeyService {

    @Resource(name = "itemCategoryAttrKeyMapper")
    private ItemCategoryAttrKeyMapper mapper;

    @Resource(name = "itemCategoryAttrValueMapper")
    private ItemCategoryAttrValueMapper itemCategoryAttrValueMapper;
    
    @Resource(name = "idWorker")
    private IdWorker idWorker;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<ItemCategoryAttrKey> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(ItemCategoryAttrKey record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(ItemCategoryAttrKey record) {
       return mapper.insertSelective(record);
    }

    @Override
    public ItemCategoryAttrKey selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(ItemCategoryAttrKey record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(ItemCategoryAttrKey record) {
      return mapper.updateByPrimaryKey(record);
    }

    @Override
    public String saveItemCategoryAttrKeyWithDetailInfo(ItemCategoryAttrKeyDTO operObj) {
        if(operObj != null) {

            // BeanCopy
            com.pap.item.category.entity.ItemCategoryAttrKey databaseItemCategoryAttrKey = new com.pap.item.category.entity.ItemCategoryAttrKey();
            BeanCopyUtilss.myCopyProperties(operObj, databaseItemCategoryAttrKey);

            List<ItemCategoryAttrValue> databaseItemCategoryAttrValueList = new ArrayList<ItemCategoryAttrValue>();
            List<ItemCategoryAttrValueDTO> inputDetailList = operObj.getDetails();
            if(inputDetailList != null) {
                databaseItemCategoryAttrValueList = toDetailDB(inputDetailList);
            }

            mapper.insertSelective(databaseItemCategoryAttrKey);
            if(databaseItemCategoryAttrValueList != null) {
                for (ItemCategoryAttrValue detailDTO : databaseItemCategoryAttrValueList) {
                    detailDTO.setItemCategoryAttrValueId(idWorker.nextIdStr());
                    detailDTO.setItemCategoryAttrKeyId(operObj.getItemCategoryAttrKeyId());
                    detailDTO.setItemCategoryId(operObj.getItemCategoryId());
                    detailDTO.setCreateIp(operObj.getCreateIp());
                    detailDTO.setCreateUser(operObj.getCreateUser());
                    detailDTO.setCreateTime(DateUtils.getCurrDateTimeStr());
                    detailDTO.setClientLicenseId(operObj.getClientLicenseId());
                    itemCategoryAttrValueMapper.insertSelective(detailDTO);
                }
            }
        }
        return operObj.getItemCategoryAttrKeyId();
    }

    @Override
    public void saveItemCategoryAttrKeyWithDetailInfo(List<ItemCategoryAttrKeyDTO> operObjList, LoginedUserVO loginedUserVO) {
        if(operObjList != null && operObjList.size() > 0) {
            for(ItemCategoryAttrKeyDTO itemCategoryAttrKeyDTO : operObjList) {
                // BeanCopy
                com.pap.item.category.entity.ItemCategoryAttrKey databaseItemCategoryAttrKey = new com.pap.item.category.entity.ItemCategoryAttrKey();
                BeanCopyUtilss.myCopyProperties(itemCategoryAttrKeyDTO, databaseItemCategoryAttrKey, loginedUserVO);

                List<ItemCategoryAttrValue> databaseItemCategoryAttrValueList = new ArrayList<ItemCategoryAttrValue>();
                List<ItemCategoryAttrValueDTO> inputDetailList = itemCategoryAttrKeyDTO.getDetails();
                if(inputDetailList != null) {
                    databaseItemCategoryAttrValueList = toDetailDB(inputDetailList);
                }

                mapper.insertSelective(databaseItemCategoryAttrKey);
                if(databaseItemCategoryAttrValueList != null) {
                    for (ItemCategoryAttrValue detailDTO : databaseItemCategoryAttrValueList) {
                        detailDTO.setItemCategoryAttrValueId(idWorker.nextIdStr());
                        detailDTO.setItemCategoryAttrKeyId(databaseItemCategoryAttrKey.getItemCategoryAttrKeyId());
                        detailDTO.setItemCategoryId(databaseItemCategoryAttrKey.getItemCategoryId());
                        detailDTO.setCreateIp(databaseItemCategoryAttrKey.getCreateIp());
                        detailDTO.setCreateUser(databaseItemCategoryAttrKey.getCreateUser());
                        detailDTO.setCreateTime(DateUtils.getCurrDateTimeStr());
                        detailDTO.setClientLicenseId(databaseItemCategoryAttrKey.getClientLicenseId());
                        itemCategoryAttrValueMapper.insertSelective(detailDTO);
                    }
                }
            }
        }
    }

    @Override
    public String updateItemCategoryAttrKeyWithDetailInfo(ItemCategoryAttrKeyDTO operObj) {
        if(operObj != null) {
            // BeanCopy
            com.pap.item.category.entity.ItemCategoryAttrKey databaseItemCategoryAttrKey = new com.pap.item.category.entity.ItemCategoryAttrKey();
            BeanCopyUtilss.myCopyProperties(operObj, databaseItemCategoryAttrKey);

            List<ItemCategoryAttrValue> databaseItemCategoryAttrValueList = new ArrayList<ItemCategoryAttrValue>();
            List<ItemCategoryAttrValueDTO> inputDetailList = operObj.getDetails();
            if(inputDetailList != null) {
                databaseItemCategoryAttrValueList = toDetailDB(inputDetailList);
            }

            mapper.updateByPrimaryKeySelective(databaseItemCategoryAttrKey);

            if(databaseItemCategoryAttrValueList != null) {
                // 删除已被删除的明细数据: dto中无明细数据，数据库中有明细数据，删除
                Map<Object, Object> operSelectItemCategoryAttrKeyMap = new HashMap<Object, Object>();
                operSelectItemCategoryAttrKeyMap.put("itemCategoryAttrKeyId", databaseItemCategoryAttrKey.getItemCategoryAttrKeyId());
                List<ItemCategoryAttrValue> dbItemCategoryAttrValueList = itemCategoryAttrValueMapper.selectListByMap(operSelectItemCategoryAttrKeyMap);
                if(dbItemCategoryAttrValueList != null && dbItemCategoryAttrValueList.size() > 0) {
                    for(ItemCategoryAttrValue dbItemCategoryAttrValue : dbItemCategoryAttrValueList) {
                        boolean existBool = false;
                        for(ItemCategoryAttrValueDTO itemCategoryAttrValueDTO: inputDetailList) {
                            if(dbItemCategoryAttrValue.getItemCategoryAttrValueId().equals(itemCategoryAttrValueDTO.getItemCategoryAttrValueId())) {
                                existBool = true;
                            }
                        }
                        // existBool = true 说明 dbItemCategoryAttrValue 数据未被删除
                        if(existBool == false) {
                            itemCategoryAttrValueMapper.deleteByPrimaryKey(dbItemCategoryAttrValue.getItemCategoryAttrValueId());
                        }

                    }
                }

                for (ItemCategoryAttrValue detailDTO : databaseItemCategoryAttrValueList) {
                    ItemCategoryAttrValue temp = itemCategoryAttrValueMapper.selectByPrimaryKey(detailDTO.getItemCategoryAttrValueId());
                    if(temp != null) {
                        detailDTO.setItemCategoryId(operObj.getItemCategoryId());
                        itemCategoryAttrValueMapper.updateByPrimaryKeySelective(detailDTO);
                    } else {
                        detailDTO.setItemCategoryAttrValueId(idWorker.nextIdStr());
                        detailDTO.setItemCategoryAttrKeyId(operObj.getItemCategoryAttrKeyId());
                        detailDTO.setItemCategoryId(operObj.getItemCategoryId());
                        detailDTO.setCreateIp(operObj.getCreateIp());
                        detailDTO.setCreateUser(operObj.getCreateUser());
                        detailDTO.setCreateTime(DateUtils.getCurrDateTimeStr());
                        detailDTO.setClientLicenseId(operObj.getClientLicenseId());
                        itemCategoryAttrValueMapper.insertSelective(detailDTO);
                    }
                }
            }
        }
        return operObj.getItemCategoryAttrKeyId();
    }

    @Override
    public void updateItemCategoryAttrKeyWithDetailInfo(List<ItemCategoryAttrKeyDTO> operObjList, LoginedUserVO loginedUserVO) {
        if(operObjList != null && operObjList.size() > 0) {
            for (ItemCategoryAttrKeyDTO itemCategoryAttrKeyDTO : operObjList) {
                // BeanCopy
                com.pap.item.category.entity.ItemCategoryAttrKey databaseItemCategoryAttrKey = new com.pap.item.category.entity.ItemCategoryAttrKey();
                BeanCopyUtilss.myCopyProperties(itemCategoryAttrKeyDTO, databaseItemCategoryAttrKey, loginedUserVO, false);

                List<ItemCategoryAttrValue> databaseItemCategoryAttrValueList = new ArrayList<ItemCategoryAttrValue>();
                List<ItemCategoryAttrValueDTO> inputDetailList = itemCategoryAttrKeyDTO.getDetails();
                if(inputDetailList != null) {
                    databaseItemCategoryAttrValueList = toDetailDB(inputDetailList);
                }

                // 判断当前头数据是否已经被保存，保存的话进行更新，未保存的进行插入操作
                com.pap.item.category.entity.ItemCategoryAttrKey tempAttrKey = mapper.selectByPrimaryKey(databaseItemCategoryAttrKey.getItemCategoryAttrKeyId());
                if (tempAttrKey != null && tempAttrKey.getItemCategoryAttrKeyId() != null) {
                    mapper.updateByPrimaryKeySelective(databaseItemCategoryAttrKey);
                } else {
                    mapper.insertSelective(databaseItemCategoryAttrKey);
                }

                if(databaseItemCategoryAttrValueList != null) {
                    // 删除已被删除的明细数据: dto中无明细数据，数据库中有明细数据，删除
                    Map<Object, Object> operSelectItemCategoryAttrKeyMap = new HashMap<Object, Object>();
                    operSelectItemCategoryAttrKeyMap.put("itemCategoryAttrKeyId", databaseItemCategoryAttrKey.getItemCategoryAttrKeyId());
                    List<ItemCategoryAttrValue> dbItemCategoryAttrValueList = itemCategoryAttrValueMapper.selectListByMap(operSelectItemCategoryAttrKeyMap);
                    if(dbItemCategoryAttrValueList != null && dbItemCategoryAttrValueList.size() > 0) {
                        for(ItemCategoryAttrValue dbItemCategoryAttrValue : dbItemCategoryAttrValueList) {
                            boolean existBool = false;
                            for(ItemCategoryAttrValueDTO itemCategoryAttrValueDTO: inputDetailList) {
                                if(dbItemCategoryAttrValue.getItemCategoryAttrValueId().equals(itemCategoryAttrValueDTO.getItemCategoryAttrValueId())) {
                                    existBool = true;
                                }
                            }
                            // existBool = true 说明 dbItemCategoryAttrValue 数据未被删除
                            if(existBool == false) {
                                itemCategoryAttrValueMapper.deleteByPrimaryKey(dbItemCategoryAttrValue.getItemCategoryAttrValueId());
                            }

                        }
                    }

                    for (ItemCategoryAttrValue detailDTO : databaseItemCategoryAttrValueList) {
                        ItemCategoryAttrValue temp = itemCategoryAttrValueMapper.selectByPrimaryKey(detailDTO.getItemCategoryAttrValueId());
                        if(temp != null) {
                            detailDTO.setItemCategoryId(databaseItemCategoryAttrKey.getItemCategoryId());
                            detailDTO.setModifyIp(databaseItemCategoryAttrKey.getModifyIp());
                            detailDTO.setModifyUser(databaseItemCategoryAttrKey.getModifyUser());
                            detailDTO.setModifyTime(DateUtils.getCurrDateTimeStr());
                            itemCategoryAttrValueMapper.updateByPrimaryKeySelective(detailDTO);
                        } else {
                            detailDTO.setItemCategoryAttrValueId(idWorker.nextIdStr());
                            detailDTO.setItemCategoryAttrKeyId(databaseItemCategoryAttrKey.getItemCategoryAttrKeyId());
                            detailDTO.setItemCategoryId(databaseItemCategoryAttrKey.getItemCategoryId());
                            detailDTO.setCreateIp(databaseItemCategoryAttrKey.getModifyIp());
                            detailDTO.setCreateUser(databaseItemCategoryAttrKey.getModifyUser());
                            detailDTO.setCreateTime(DateUtils.getCurrDateTimeStr());
                            detailDTO.setClientLicenseId(databaseItemCategoryAttrKey.getClientLicenseId());
                            itemCategoryAttrValueMapper.insertSelective(detailDTO);
                        }
                    }
                }
            }
        }
    }

    @Override
    public int deleteItemCategoryAttrKeyWithDetailInfo(String inputItemCategoryAttrKeyId) {
        mapper.deleteByPrimaryKey(inputItemCategoryAttrKeyId);
        itemCategoryAttrValueMapper.deleteByItemCategoryAttrKeyId(inputItemCategoryAttrKeyId);
        return 0;
    }

    @Override
    public ItemCategoryAttrKeyDTO selectItemCategoryAttrKeyWithDetailInfoById(String inputSelectItemCategoryAttrKeyId) {
        ItemCategoryAttrKeyDTO ItemCategoryAttrKeyDTO = new ItemCategoryAttrKeyDTO();
        List<ItemCategoryAttrValueDTO> ItemCategoryAttrKeyDetailDTOList = new ArrayList<ItemCategoryAttrValueDTO>();

        com.pap.item.category.entity.ItemCategoryAttrKey databaseItemCategoryAttrKey = mapper.selectByPrimaryKey(inputSelectItemCategoryAttrKeyId);
        if(databaseItemCategoryAttrKey != null) {
            BeanCopyUtilss.myCopyProperties(databaseItemCategoryAttrKey, ItemCategoryAttrKeyDTO);
        }


        Map<Object, Object> operSelectItemCategoryAttrKeyMap = new HashMap<Object, Object>();
        operSelectItemCategoryAttrKeyMap.put("itemCategoryAttrKeyId", inputSelectItemCategoryAttrKeyId);
        List<ItemCategoryAttrValue> databaseItemCategoryAttrKeyDetailList = itemCategoryAttrValueMapper.selectListByMap(operSelectItemCategoryAttrKeyMap);
        if(databaseItemCategoryAttrKeyDetailList != null) {
            ItemCategoryAttrKeyDetailDTOList = toDetailDTO(databaseItemCategoryAttrKeyDetailList);
        }

        ItemCategoryAttrKeyDTO.setDetails(ItemCategoryAttrKeyDetailDTOList);

        return ItemCategoryAttrKeyDTO;
    }

    @Override
    public List<ItemCategoryAttrKeyDTO> selectItemCategoryAttrKeyWithDetailListByItemCategoryId(String itemCategoryId) {
        List<ItemCategoryAttrKeyDTO> returnList = new ArrayList<ItemCategoryAttrKeyDTO>();

        Map<Object, Object> categoryIdMap = new HashMap<Object, Object>();
        categoryIdMap.put("itemCategoryId", itemCategoryId);
        // orderBySqlJudgeStr 排序 必选属性/关键属性/销售属性/非关键属性/枚举属性/多选属性/输入属性
        categoryIdMap.put("orderBySqlJudgeStr", " REQUIRED_FLAG DESC, KEY_FLAG DESC, SALE_FLAG DESC, NO_KEY_FLAG DESC, ENUM_FLAG DESC, MULTIPLE_FLAG DESC, INPUT_FLAG DESC  ");
        List<ItemCategoryAttrKey> itemCategoryAttrKeyList = mapper.selectListByMap(categoryIdMap);
        if(itemCategoryAttrKeyList != null && itemCategoryAttrKeyList.size() > 0) {
            for(ItemCategoryAttrKey itemCategoryAttrKey: itemCategoryAttrKeyList) {
                ItemCategoryAttrKeyDTO itemCategoryAttrKeyDTO = selectItemCategoryAttrKeyWithDetailInfoById(itemCategoryAttrKey.getItemCategoryAttrKeyId());
                returnList.add(itemCategoryAttrKeyDTO);
            }
        }
        return returnList;
    }

    private List<ItemCategoryAttrValue> toDetailDB(List<ItemCategoryAttrValueDTO> dtoList) {
        List<ItemCategoryAttrValue> returnList = new ArrayList<ItemCategoryAttrValue>();
        if(dtoList != null) {
            for(ItemCategoryAttrValueDTO dtoEntity : dtoList) {
                ItemCategoryAttrValue dbTemp = new ItemCategoryAttrValue();
                BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
                returnList.add(dbTemp);
            }
        }
        return returnList;
    }

    private List<ItemCategoryAttrValueDTO> toDetailDTO(List<ItemCategoryAttrValue> databaseList) {
        List<ItemCategoryAttrValueDTO> returnList = new ArrayList<ItemCategoryAttrValueDTO>();
        if(databaseList != null) {
            for(ItemCategoryAttrValue dbEntity : databaseList) {
                ItemCategoryAttrValueDTO dtoTemp = new ItemCategoryAttrValueDTO();
                BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
                returnList.add(dtoTemp);
            }
        }
        return returnList;
    }
}