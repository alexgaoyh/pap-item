package com.pap.item.category.service;

import com.pap.base.annotation.vo.ExtraParamsVO;
import com.pap.item.category.entity.ItemCategory;
import com.pap.item.dto.ItemCategoryDTO;
import com.pap.item.dto.ItemCategoryTreeNodeVO;
import com.pap.obj.vo.response.ResponseVO;

import java.util.List;
import java.util.Map;

public interface IItemCategoryService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<ItemCategory> selectListByMap(Map<Object, Object> map);

    int insert(ItemCategory record);

    int insertSelective(ItemCategory record);

    ItemCategory selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ItemCategory record);

    int updateByPrimaryKey(ItemCategory record);

    // alexgaoyh add

    /**
     * 商品类别树形结构，根据 license 获取
     * @param clientLicenseId
     * @param globalParentId 全局统一的默认顶层组织架构的编码为 -1
     */
    List<ItemCategoryTreeNodeVO> itemCategoryTreeJson(String clientLicenseId, String globalParentId);

    /**
     * 检查当前操作的商品类别编码，对照新维护的上级商品类别，是否被循环依赖
     * @param operaItemCategoryId
     * @param newParentItemCategoryId
     * @return
     */
    Boolean checkTreeCircularDependency(String operaItemCategoryId, String newParentItemCategoryId);

    /**
     * 商品类别更新操作,参数校验并且进行数据维护	pathIds pathCodes pathNames 三个字段的数据维护
     * @return
     */
    ResponseVO<ItemCategory> updateAndCheckItemCategory(ItemCategory inputItemCategory);

    /**
     * 维护当前商品类别节点下，包含的子集的商品类别的数据值,冗余字段的维护，
     * pathIds pathCodes pathNames
     * 请确保当前操作的 inputItemCategoryId，对应的实体数据已经是最新的。
     *
     * 递归调用，维护完毕所有子类的path冗余字段
     * @param inputItemCategoryId
     * @return
     */
    ResponseVO<ItemCategory> updateRecursionPathColumnItemCategory(String inputItemCategoryId);

}
