package com.pap.item.item.service;

import com.pap.item.item.entity.ItemProductDetail;

import java.util.List;
import java.util.Map;

public interface IItemProductDetailService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<ItemProductDetail> selectListByMap(Map<Object, Object> map);

    int insert(ItemProductDetail record);

    int insertSelective(ItemProductDetail record);

    ItemProductDetail selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ItemProductDetail record);

    int updateByPrimaryKey(ItemProductDetail record);
}
