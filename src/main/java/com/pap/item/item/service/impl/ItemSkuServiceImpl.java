package com.pap.item.item.service.impl;

import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.base.util.date.DateUtils;
import com.pap.beans.idworker.IdWorker;
import com.pap.item.dto.*;
import com.pap.item.item.entity.ItemSku;
import com.pap.item.item.entity.ItemSkuExtra;
import com.pap.item.item.mapper.ItemSkuExtraMapper;
import com.pap.item.item.mapper.ItemSkuMapper;
import com.pap.item.item.service.IItemSkuService;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional
@Service("itemSkuService")
public class ItemSkuServiceImpl implements IItemSkuService {

    @Resource(name = "itemSkuMapper")
    private ItemSkuMapper mapper;    
    
    @Resource(name = "itemSkuExtraMapper")
    private ItemSkuExtraMapper itemSkuExtraMapper;

    @Resource(name = "idWorker")
    private IdWorker idWorker;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<ItemSku> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(ItemSku record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(ItemSku record) {
       return mapper.insertSelective(record);
    }

    @Override
    public ItemSku selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(ItemSku record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(ItemSku record) {
      return mapper.updateByPrimaryKey(record);
    }

    @Override
    public String createWithDetails(ItemSkuWithDetailDTO itemSkuWithDetailDTO) {
        if(itemSkuWithDetailDTO != null) {
            // BeanCopy
            com.pap.item.item.entity.ItemSku databaseItemSku = new com.pap.item.item.entity.ItemSku();
            BeanCopyUtilss.myCopyProperties(itemSkuWithDetailDTO, databaseItemSku);
            mapper.insertSelective(databaseItemSku);

            List<ItemSkuExtra> databaseItemSkuExtraDTOList = new ArrayList<ItemSkuExtra>();
            List<ItemSkuExtraDTO> inputDetailList = itemSkuWithDetailDTO.getDetails();
            if(inputDetailList != null) {
                databaseItemSkuExtraDTOList = toDetailDB(inputDetailList);
            }

            if(databaseItemSkuExtraDTOList != null) {
                for (ItemSkuExtra itemSkuExtra : databaseItemSkuExtraDTOList) {
                    itemSkuExtra.setItemSkuExtraId(idWorker.nextIdStr());
                    itemSkuExtra.setItemSkuId(itemSkuWithDetailDTO.getItemSkuId());
                    itemSkuExtra.setCreateIp(itemSkuWithDetailDTO.getCreateIp());
                    itemSkuExtra.setCreateUser(itemSkuWithDetailDTO.getCreateUser());
                    itemSkuExtra.setCreateTime(DateUtils.getCurrDateTimeStr());
                    itemSkuExtra.setClientLicenseId(itemSkuWithDetailDTO.getClientLicenseId());
                    itemSkuExtraMapper.insertSelective(itemSkuExtra);
                }
            }
        }
        return itemSkuWithDetailDTO.getItemSkuId();
    }

    @Override
    public void createWithDetails(List<ItemSkuWithDetailDTO> itemSkuWithDetailDTOList, LoginedUserVO loginedUserVO) {
        if(itemSkuWithDetailDTOList != null && itemSkuWithDetailDTOList.size() > 0) {
            for (ItemSkuWithDetailDTO itemSkuWithDetailDTO : itemSkuWithDetailDTOList) {
                BeanCopyUtilss.myCopyPropertiesForLoginedUser(itemSkuWithDetailDTO, loginedUserVO, true);
                itemSkuWithDetailDTO.setItemSkuId(idWorker.nextIdStr());

                createWithDetails(itemSkuWithDetailDTO);
            }
        }
    }

    @Override
    public String modifyWithDetails(ItemSkuWithDetailDTO itemSkuWithDetailDTO) {
        if(itemSkuWithDetailDTO != null) {
            // BeanCopy
            com.pap.item.item.entity.ItemSku databaseItemSku = new com.pap.item.item.entity.ItemSku();
            BeanCopyUtilss.myCopyProperties(itemSkuWithDetailDTO, databaseItemSku);
            mapper.updateByPrimaryKeySelective(databaseItemSku);

            List<ItemSkuExtra> databaseItemSkuExtraList = new ArrayList<ItemSkuExtra>();
            List<ItemSkuExtraDTO> inputDetailList = itemSkuWithDetailDTO.getDetails();
            if(inputDetailList != null) {
                databaseItemSkuExtraList = toDetailDB(inputDetailList);
            }

            if(databaseItemSkuExtraList != null) {
                // 删除已被删除的明细数据: dto中无明细数据，数据库中有明细数据，删除
                Map<Object, Object> operSelectItemSkuMap = new HashMap<Object, Object>();
                operSelectItemSkuMap.put("itemSkuId", itemSkuWithDetailDTO.getItemSkuId());
                List<ItemSkuExtra> dbItemSkuExtraList = itemSkuExtraMapper.selectListByMap(operSelectItemSkuMap);
                if(dbItemSkuExtraList != null && dbItemSkuExtraList.size() > 0) {
                    for(ItemSkuExtra dbItemSkuExtra : dbItemSkuExtraList) {
                        boolean existBool = false;
                        for(ItemSkuExtraDTO itemSkuExtraDTO: inputDetailList) {
                            if(dbItemSkuExtra.getItemSkuExtraId().equals(itemSkuExtraDTO.getItemSkuExtraId())) {
                                existBool = true;
                            }
                        }
                        // existBool = true 说明 dbItemSkuExtra 数据未被删除
                        if(existBool == false) {
                            itemSkuExtraMapper.deleteByPrimaryKey(dbItemSkuExtra.getItemSkuExtraId());
                        }

                    }
                }

                for (ItemSkuExtra detailDTO : databaseItemSkuExtraList) {
                    ItemSkuExtra temp = itemSkuExtraMapper.selectByPrimaryKey(detailDTO.getItemSkuExtraId());
                    if(temp != null) {
                        detailDTO.setItemSkuId(itemSkuWithDetailDTO.getItemSkuId());
                        detailDTO.setModifyIp(itemSkuWithDetailDTO.getModifyIp());
                        detailDTO.setModifyUser(itemSkuWithDetailDTO.getModifyUser());
                        detailDTO.setModifyTime(DateUtils.getCurrDateTimeStr());
                        detailDTO.setClientLicenseId(itemSkuWithDetailDTO.getClientLicenseId());
                        itemSkuExtraMapper.updateByPrimaryKeySelective(detailDTO);
                    } else {
                        detailDTO.setItemSkuExtraId(idWorker.nextIdStr());
                        detailDTO.setItemSkuId(itemSkuWithDetailDTO.getItemSkuId());
                        detailDTO.setCreateIp(itemSkuWithDetailDTO.getCreateIp());
                        detailDTO.setCreateUser(itemSkuWithDetailDTO.getCreateUser());
                        detailDTO.setCreateTime(DateUtils.getCurrDateTimeStr());
                        detailDTO.setClientLicenseId(itemSkuWithDetailDTO.getClientLicenseId());
                        itemSkuExtraMapper.insertSelective(detailDTO);
                    }
                }
            }
        }
        return itemSkuWithDetailDTO.getItemSkuId();
    }

    @Override
    public void modifyWithDetails(List<ItemSkuWithDetailDTO> itemSkuWithDetailDTOList, LoginedUserVO loginedUserVO) {
        if(itemSkuWithDetailDTOList != null && itemSkuWithDetailDTOList.size() > 0) {
            for (ItemSkuWithDetailDTO itemSkuWithDetailDTO : itemSkuWithDetailDTOList) {
                BeanCopyUtilss.myCopyPropertiesForLoginedUser(itemSkuWithDetailDTO, loginedUserVO, false);
                modifyWithDetails(itemSkuWithDetailDTO);
            }
        }
    }

    @Override
    public String deleteWithDetails(String itemSkuId) {
        ItemSku itemSku = mapper.selectByPrimaryKey(itemSkuId);
        if(itemSku != null) {
            itemSku.setDisableFlag("N");
            mapper.updateByPrimaryKeySelective(itemSku);
        }
        return itemSkuId;
    }

    @Override
    public ItemSkuWithDetailDTO selectWithDetails(String itemSkuId) {
        ItemSkuWithDetailDTO itemSkuWithDetailDTO = new ItemSkuWithDetailDTO();
        List<ItemSkuExtraDTO> itemSkuExtraDTOList = new ArrayList<ItemSkuExtraDTO>();

        com.pap.item.item.entity.ItemSku databaseItemSku = mapper.selectByPrimaryKey(itemSkuId);
        if(databaseItemSku != null) {
            BeanCopyUtilss.myCopyProperties(databaseItemSku, itemSkuWithDetailDTO);
        }

        Map<Object, Object> operSelectItemSkuMap = new HashMap<Object, Object>();
        operSelectItemSkuMap.put("itemSkuId", itemSkuId);
        List<ItemSkuExtra> databaseItemSkuExtraList = itemSkuExtraMapper.selectListByMap(operSelectItemSkuMap);
        if(databaseItemSkuExtraList != null) {
            itemSkuExtraDTOList = toDetailDTO(databaseItemSkuExtraList);
        }

        itemSkuWithDetailDTO.setDetails(itemSkuExtraDTOList);

        return itemSkuWithDetailDTO;
    }

    @Override
    public List<ItemSkuWithDetailDTO> selectWithDetailsByProductId(String itemProductId) {
        List<ItemSkuWithDetailDTO> returnItemSkuWithDetailDTO = new ArrayList<ItemSkuWithDetailDTO>();

        Map<Object, Object> operItemSkuMap = new HashMap<Object, Object>();
        operItemSkuMap.put("itemProductId", itemProductId);
        List<ItemSku> databaseItemSkuList = mapper.selectListByMap(operItemSkuMap);
        if(databaseItemSkuList != null && databaseItemSkuList.size() > 0) {
            for (ItemSku itemSku : databaseItemSkuList) {
                ItemSkuWithDetailDTO itemSkuWithDetailDTO = selectWithDetails(itemSku.getItemSkuId());
                returnItemSkuWithDetailDTO.add(itemSkuWithDetailDTO);
            }
        }

        return returnItemSkuWithDetailDTO;
    }

    @Override
    public BigDecimal selectItemPriceByItemSkuId(String itemSkuId) {
        ItemSku itemSku = mapper.selectByPrimaryKey(itemSkuId);
        return itemSku.getItemPrice();
    }

    @Override
    public List<SkuPropsDTO> selectSkuPropsByItemProductId(String itemProductId) {
        return mapper.selectSkuPropsByItemProductId(itemProductId);
    }

    @Override
    public List<SkuPriceAndStockDTO> selectSkuPriceAndStockByItemProductId(String itemProductId) {
        return mapper.selectSkuPriceAndStockByItemProductId(itemProductId);
    }

    @Override
    public List<VantSkuPropsDTO> selectVantSkuPropsByItemProductId(String itemProductId) {
        return mapper.selectVantSkuPropsByItemProductId(itemProductId);
    }

    /**
     *                             {
     *                                 id: 2259,
     *                                 price: 120, //价格
     *                                 discount: 122,
     *                                 s1: '1215',
     *                                 s2: '1193',
     *                                 s3: '0',
     *                                 s4: '0',
     *                                 s5: '0',
     *                                 stock_num: 20, //库存
     *                                 goods_id: 946755
     *                             }
     * @param itemProductId
     * @return
     */
    @Override
    public List<Map<String, Object>> selectVantSkuListByItemProductId(String itemProductId) {
        List<Map<String, Object>> returnList = new ArrayList<Map<String, Object>>();

        Map<Object, Object> itemProductIdMap = new HashMap<Object, Object>(1);
        itemProductIdMap.put("itemProductId", itemProductId);
        List<ItemSku> itemSkuList = mapper.selectListByMap(itemProductIdMap);
        if(itemSkuList != null && itemSkuList.size() > 0) {
            for(ItemSku itemSku : itemSkuList) {
                Map<String, Object> itemSkuMap = new HashMap<String, Object>();
                itemSkuMap.put("id", itemSku.getItemSkuId());
                // 价格需要 * 100， 以 分 为单位
                itemSkuMap.put("price", itemSku.getItemPrice().multiply(new BigDecimal(100)));
                itemSkuMap.put("discount", itemSku.getItemPrice().multiply(new BigDecimal(100)));
                itemSkuMap.put("stock_num", itemSku.getItemStockNum());
                itemSkuMap.put("goods_id", itemProductId);

                List<ItemSkuAttrNameAndValueDTO> itemSkuAttrNameAndValueDTOList = itemSkuExtraMapper.selectItemSkuAttrNameAndValueByItemSkuId(itemSku.getItemSkuId());
                if(itemSkuAttrNameAndValueDTOList != null && itemSkuAttrNameAndValueDTOList.size() > 0) {
                    for(ItemSkuAttrNameAndValueDTO itemSkuAttrNameAndValueDTO : itemSkuAttrNameAndValueDTOList) {
                        // 移除 字符串 空格
                        itemSkuMap.put(itemSkuAttrNameAndValueDTO.getAttrName().replace(" ",""), itemSkuAttrNameAndValueDTO.getAttrValue().replace(" ",""));
                    }
                }

                returnList.add(itemSkuMap);
            }
        }

        return returnList;
    }

    private List<ItemSkuExtra> toDetailDB(List<ItemSkuExtraDTO> dtoList) {
        List<ItemSkuExtra> returnList = new ArrayList<ItemSkuExtra>();
        if(dtoList != null) {
            for(ItemSkuExtraDTO dtoEntity : dtoList) {
                ItemSkuExtra dbTemp = new ItemSkuExtra();
                BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
                returnList.add(dbTemp);
            }
        }
        return returnList;
    }

    private List<ItemSkuExtraDTO> toDetailDTO(List<ItemSkuExtra> databaseList) {
        List<ItemSkuExtraDTO> returnList = new ArrayList<ItemSkuExtraDTO>();
        if(databaseList != null) {
            for(ItemSkuExtra dbEntity : databaseList) {
                ItemSkuExtraDTO dtoTemp = new ItemSkuExtraDTO();
                BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
                returnList.add(dtoTemp);
            }
        }
        return returnList;
    }
}