package com.pap.item.item.service.impl;

import com.pap.item.item.mapper.ItemProductDetailMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pap.item.item.entity.ItemProductDetail;
import com.pap.item.item.service.IItemProductDetailService;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Transactional
@Service("itemProductDetailService")
public class ItemProductDetailServiceImpl implements IItemProductDetailService {

    @Resource(name = "itemProductDetailMapper")
    private ItemProductDetailMapper mapper;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<ItemProductDetail> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(ItemProductDetail record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(ItemProductDetail record) {
       return mapper.insertSelective(record);
    }

    @Override
    public ItemProductDetail selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(ItemProductDetail record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(ItemProductDetail record) {
      return mapper.updateByPrimaryKey(record);
    }
}