package com.pap.item.item.service.impl;

import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.base.util.date.DateUtils;
import com.pap.base.util.string.StringUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.item.category.entity.ItemCategoryAttrKey;
import com.pap.item.category.mapper.ItemCategoryAttrKeyMapper;
import com.pap.item.dto.ItemProductDetailDTO;
import com.pap.item.dto.ItemProductTreeNodeVO;
import com.pap.item.dto.ItemProductWithDetailDTO;
import com.pap.item.item.entity.ItemProduct;
import com.pap.item.item.entity.ItemProductDetail;
import com.pap.item.item.mapper.ItemProductDetailMapper;
import com.pap.item.item.mapper.ItemProductMapper;
import com.pap.item.item.service.IItemProductService;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.response.ResponseVO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional
@Service("itemProductService")
public class ItemProductServiceImpl implements IItemProductService {

    @Resource(name = "itemProductMapper")
    private ItemProductMapper mapper;

    @Resource(name = "itemProductDetailMapper")
    private ItemProductDetailMapper itemProductDetailMapper;

    @Resource(name = "itemCategoryAttrKeyMapper")
    private ItemCategoryAttrKeyMapper itemCategoryAttrKeyMapper;

    @Resource(name = "idWorker")
    private IdWorker idWorker;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<ItemProduct> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(ItemProduct record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(ItemProduct record) {
       return mapper.insertSelective(record);
    }

    @Override
    public ItemProduct selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(ItemProduct record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(ItemProduct record) {
      return mapper.updateByPrimaryKey(record);
    }

    @Override
    public List<ItemProductTreeNodeVO> itemProductTreeJson(String clientLicenseId, String globalParentId) {
        return mapper.itemProductTreeJson(clientLicenseId, globalParentId);
    }

    @Override
    public Boolean checkTreeCircularDependency(String operaItemProductId, String newParentItemProductId) {
        String parentItemProductTemp = "";
        ItemProduct newParentItemProductInfo = mapper.selectByPrimaryKey(newParentItemProductId);
        if(newParentItemProductInfo != null) {
            if(StringUtilss.isNotEmpty(newParentItemProductInfo.getPathIds())) {
                parentItemProductTemp = newParentItemProductInfo.getPathIds() + ",";
            }
            parentItemProductTemp += newParentItemProductInfo.getItemProductId();
        }
        if (StringUtilss.checkArrayValue(parentItemProductTemp.split(","), operaItemProductId)) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public ResponseVO<ItemProduct> updateAndCheckItemProduct(ItemProduct inputItemProduct) {
        // 检验是否循环依赖
        if(checkTreeCircularDependency(inputItemProduct.getItemProductId(), inputItemProduct.getItemProductParentId())) {

            // 忽略前台传递过来的数据
            inputItemProduct.setPathIds(null);
            inputItemProduct.setPathCodes(null);
            inputItemProduct.setPathNames(null);
            mapper.updateByPrimaryKeySelective(inputItemProduct);

            ItemProduct tempForPath = mapper.selectByPrimaryKey(inputItemProduct.getItemProductId());
            if("-1".equals(inputItemProduct.getItemProductParentId())) {
                tempForPath.setPathCodes(null);
                tempForPath.setPathIds(null);
                tempForPath.setPathNames(null);

                tempForPath.setItemProductParentId("-1");
                mapper.updateByPrimaryKey(tempForPath);
            } else {
                // 查询父节点信息，处理当前节点的冗余字段
                ItemProduct tempParentForPath = mapper.selectByPrimaryKey(inputItemProduct.getItemProductParentId());
                if(tempParentForPath != null) {
                    if(StringUtilss.isNotEmpty(tempParentForPath.getPathIds())) {
                        tempForPath.setPathIds(tempParentForPath.getPathIds() + "," + tempParentForPath.getItemProductId());
                    } else {
                        tempForPath.setPathIds(tempParentForPath.getItemProductId());
                    }
                    if(StringUtilss.isNotEmpty(tempParentForPath.getPathCodes())) {
                        tempForPath.setPathCodes(tempParentForPath.getPathCodes() + "," + tempParentForPath.getItemProductCode());
                    } else {
                        tempForPath.setPathCodes(tempParentForPath.getItemProductCode());
                    }
                    if(StringUtilss.isNotEmpty(tempParentForPath.getPathNames())) {
                        tempForPath.setPathNames(tempParentForPath.getPathNames() + "," + tempParentForPath.getItemProductName());
                    } else {
                        tempForPath.setPathNames(tempParentForPath.getItemProductName());
                    }
                } else {
                    tempForPath.setItemProductParentId("-1");
                    tempForPath.setPathCodes(null);
                    tempForPath.setPathIds(null);
                    tempForPath.setPathNames(null);
                }

                mapper.updateByPrimaryKey(tempForPath);
            }

            // 维护子集的商品货品
            ResponseVO<ItemProduct> responseVO =  updateRecursionPathColumnItemProduct(inputItemProduct.getItemProductId());
            return responseVO;
        } else {
            return ResponseVO.validfail("商品货品被循环依赖，请检查数据有效性!");
        }

    }

    @Override
    public ResponseVO<ItemProduct> updateRecursionPathColumnItemProduct(String inputItemProductId) {
        try {
            Map<Object, Object> parentIdMap = new HashMap<Object, Object>();
            parentIdMap.put("itemProductParentId", inputItemProductId);
            List<ItemProduct> list = mapper.selectListByMap(parentIdMap);
            if (null != list && list.size()>0) {
                for (int i = 0; i < list.size(); i++) {
                    ItemProduct tempForPath = list.get(i);
                    // 更新数据
                    ItemProduct tempParentForPath = mapper.selectByPrimaryKey(tempForPath.getItemProductParentId());
                    if(tempParentForPath != null) {
                        if(StringUtilss.isNotEmpty(tempParentForPath.getPathIds())) {
                            tempForPath.setPathIds(tempParentForPath.getPathIds() + "," + tempParentForPath.getItemProductId());
                        } else {
                            tempForPath.setPathIds(tempParentForPath.getItemProductId());
                        }
                        if(StringUtilss.isNotEmpty(tempParentForPath.getPathCodes())) {
                            tempForPath.setPathCodes(tempParentForPath.getPathCodes() + "," + tempParentForPath.getItemProductCode());
                        } else {
                            tempForPath.setPathCodes(tempParentForPath.getItemProductCode());
                        }
                        if(StringUtilss.isNotEmpty(tempParentForPath.getPathNames())) {
                            tempForPath.setPathNames(tempParentForPath.getPathNames() + "," + tempParentForPath.getItemProductName());
                        } else {
                            tempForPath.setPathNames(tempParentForPath.getItemProductName());
                        }

                    } else {
                        tempForPath.setPathCodes(null);
                        tempForPath.setPathIds(null);
                        tempForPath.setPathNames(null);
                    }
                    mapper.updateByPrimaryKeySelective(tempForPath);
                    updateRecursionPathColumnItemProduct(tempForPath.getItemProductId());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseVO.validfail(e.getMessage());
        }
        return ResponseVO.successdata("更新成功");
    }

    /**
     * 保存数据
     * @param operObj
     * @return
     */
    @Override
    public String saveItemProductForDetailDTO(ItemProductWithDetailDTO operObj) {
        if(operObj != null) {

            ItemProduct itemProduct = new ItemProduct();
            BeanCopyUtilss.myCopyProperties(operObj, itemProduct);
            mapper.insertSelective(itemProduct);

            List<ItemProductDetail> databaseItemProductDetailDTOList = new ArrayList<ItemProductDetail>();
            List<ItemProductDetailDTO> inputDetailList = operObj.getDetails();
            if(inputDetailList != null) {
                databaseItemProductDetailDTOList = toDetailDB(inputDetailList);
            }

            if(databaseItemProductDetailDTOList != null) {
                for (ItemProductDetail detailDTO : databaseItemProductDetailDTOList) {
                    detailDTO.setItemProductDetailId(idWorker.nextIdStr());
                    detailDTO.setItemProductId(operObj.getItemProductId());
                    detailDTO.setCreateIp(operObj.getCreateIp());
                    detailDTO.setCreateUser(operObj.getCreateUser());
                    detailDTO.setCreateTime(DateUtils.getCurrDateTimeStr());
                    detailDTO.setClientLicenseId(operObj.getClientLicenseId());
                    itemProductDetailMapper.insertSelective(detailDTO);
                }
            }
        }
        return operObj.getItemProductId();
    }

    /**
     * 更新数据
     * @param operObj
     * @return
     */
    @Override
    public String updateItemProductForDetailDTO(ItemProductWithDetailDTO operObj) {
        if(operObj != null) {

            ItemProduct itemProduct = new ItemProduct();
            BeanCopyUtilss.myCopyProperties(operObj, itemProduct);
            mapper.updateByPrimaryKeySelective(itemProduct);

            List<ItemProductDetail> databaseItemProductDetailList = new ArrayList<ItemProductDetail>();
            List<ItemProductDetailDTO> inputDetailList = operObj.getDetails();
            if(inputDetailList != null) {
                databaseItemProductDetailList = toDetailDB(inputDetailList);
            }

            if(databaseItemProductDetailList != null) {
                // 删除已被删除的明细数据: dto中无明细数据，数据库中有明细数据，删除
                Map<Object, Object> operSelectItemProductMap = new HashMap<Object, Object>();
                operSelectItemProductMap.put("itemProductId", operObj.getItemProductId());
                List<ItemProductDetail> dbItemProductDetailList = itemProductDetailMapper.selectListByMap(operSelectItemProductMap);
                if(dbItemProductDetailList != null && dbItemProductDetailList.size() > 0) {
                    for(ItemProductDetail dbItemProductDetail : dbItemProductDetailList) {
                        boolean existBool = false;
                        for(ItemProductDetailDTO itemProductDetailDTO: inputDetailList) {
                            if(dbItemProductDetail.getItemProductDetailId().equals(itemProductDetailDTO.getItemProductDetailId())) {
                                existBool = true;
                            }
                        }
                        // existBool = true 说明 dbItemProductDetail 数据未被删除
                        if(existBool == false) {
                            itemProductDetailMapper.deleteByPrimaryKey(dbItemProductDetail.getItemProductDetailId());
                        }

                    }
                }

                for (ItemProductDetail detailDTO : databaseItemProductDetailList) {
                    ItemProductDetail temp = itemProductDetailMapper.selectByPrimaryKey(detailDTO.getItemProductDetailId());
                    if(temp != null) {
                        detailDTO.setItemProductId(operObj.getItemProductId());
                        detailDTO.setModifyIp(operObj.getModifyIp());
                        detailDTO.setModifyUser(operObj.getModifyUser());
                        detailDTO.setModifyTime(DateUtils.getCurrDateTimeStr());
                        detailDTO.setClientLicenseId(operObj.getClientLicenseId());
                        itemProductDetailMapper.updateByPrimaryKeySelective(detailDTO);
                    } else {
                        detailDTO.setItemProductDetailId(idWorker.nextIdStr());
                        detailDTO.setItemProductId(operObj.getItemProductId());
                        detailDTO.setCreateIp(operObj.getCreateIp());
                        detailDTO.setCreateUser(operObj.getCreateUser());
                        detailDTO.setCreateTime(DateUtils.getCurrDateTimeStr());
                        detailDTO.setClientLicenseId(operObj.getClientLicenseId());
                        itemProductDetailMapper.insertSelective(detailDTO);
                    }
                }
            }
        }
        return operObj.getItemProductId();
    }

    /**
     *
     * @param itemProductId
     * @return
     */
    @Override
    public int deleteItemProductWithDetailDTO(String itemProductId) {
        mapper.deleteByPrimaryKey(itemProductId);
        itemProductDetailMapper.deleteByItemProductId(itemProductId);
        return 0;
    }

    @Override
    public ItemProductWithDetailDTO selectItemProductWithDetailDTOById(String itemProductId, LoginedUserVO loginedUserVO) {
        ItemProductWithDetailDTO itemProductDTO = new ItemProductWithDetailDTO();
        List<ItemProductDetailDTO> itemProductDetailDTOList = new ArrayList<ItemProductDetailDTO>();

        com.pap.item.item.entity.ItemProduct databaseItemProduct = mapper.selectByPrimaryKey(itemProductId);
        if(databaseItemProduct != null) {
            BeanCopyUtilss.myCopyProperties(databaseItemProduct, itemProductDTO);
        }

        Map<Object, Object> operSelectItemProductMap = new HashMap<Object, Object>();
        operSelectItemProductMap.put("itemProductId", itemProductId);
        operSelectItemProductMap.put("clientLicenseId", loginedUserVO != null ? loginedUserVO.getClientLicenseId() : "");
        List<ItemProductDetail> databaseItemProductDetailList = itemProductDetailMapper.selectListByMap(operSelectItemProductMap);
        if(databaseItemProductDetailList != null) {
            itemProductDetailDTOList = toDetailDTO(databaseItemProductDetailList);
        }

        itemProductDTO.setDetails(itemProductDetailDTOList);

        return itemProductDTO;
    }

    private List<ItemProductDetail> toDetailDB(List<ItemProductDetailDTO> dtoList) {
        List<ItemProductDetail> returnList = new ArrayList<ItemProductDetail>();
        if(dtoList != null) {
            for(ItemProductDetailDTO dtoEntity : dtoList) {
                ItemProductDetail dbTemp = new ItemProductDetail();
                BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
                returnList.add(dbTemp);
            }
        }
        return returnList;
    }

    private List<ItemProductDetailDTO> toDetailDTO(List<ItemProductDetail> databaseList) {
        List<ItemProductDetailDTO> returnList = new ArrayList<ItemProductDetailDTO>();
        if(databaseList != null) {
            for(ItemProductDetail dbEntity : databaseList) {
                ItemProductDetailDTO dtoTemp = new ItemProductDetailDTO();
                BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);

                // 查询出来名称进行赋值
                ItemCategoryAttrKey itemCategoryAttrKey = itemCategoryAttrKeyMapper.selectByPrimaryKey(dtoTemp.getItemCategoryAttrKeyId());
                if(itemCategoryAttrKey != null && itemCategoryAttrKey.getItemCategoryAttrKeyId() != null) {
                    dtoTemp.setItemCategoryAttrKeyName(itemCategoryAttrKey.getItemCategoryAttrKeyName());
                }

                returnList.add(dtoTemp);
            }
        }
        return returnList;
    }
}