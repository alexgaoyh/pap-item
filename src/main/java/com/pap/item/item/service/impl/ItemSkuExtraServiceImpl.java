package com.pap.item.item.service.impl;

import com.pap.item.item.mapper.ItemSkuExtraMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pap.item.item.entity.ItemSkuExtra;
import com.pap.item.item.service.IItemSkuExtraService;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Transactional
@Service("itemSkuExtraService")
public class ItemSkuExtraServiceImpl implements IItemSkuExtraService {

    @Resource(name = "itemSkuExtraMapper")
    private ItemSkuExtraMapper mapper;

    @Override
    public int deleteByPrimaryKey(String id) {
       return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int selectCountByMap(Map<Object, Object> map) {
      return mapper.selectCountByMap(map);
    }

    @Override
    public List<ItemSkuExtra> selectListByMap(Map<Object, Object> map) {
        return mapper.selectListByMap(map);
    }

    @Override
    public int insert(ItemSkuExtra record) {
       return mapper.insert(record);
    }

    @Override
    public int insertSelective(ItemSkuExtra record) {
       return mapper.insertSelective(record);
    }

    @Override
    public ItemSkuExtra selectByPrimaryKey(String id) {
       return mapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(ItemSkuExtra record) {
       return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(ItemSkuExtra record) {
      return mapper.updateByPrimaryKey(record);
    }
}