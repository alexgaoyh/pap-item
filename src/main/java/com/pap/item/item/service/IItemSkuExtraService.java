package com.pap.item.item.service;

import com.pap.item.item.entity.ItemSkuExtra;

import java.util.List;
import java.util.Map;

public interface IItemSkuExtraService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<ItemSkuExtra> selectListByMap(Map<Object, Object> map);

    int insert(ItemSkuExtra record);

    int insertSelective(ItemSkuExtra record);

    ItemSkuExtra selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ItemSkuExtra record);

    int updateByPrimaryKey(ItemSkuExtra record);
}
