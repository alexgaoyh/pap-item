package com.pap.item.item.service;

import com.pap.item.dto.ItemSkuWithDetailDTO;
import com.pap.item.dto.SkuPriceAndStockDTO;
import com.pap.item.dto.SkuPropsDTO;
import com.pap.item.dto.VantSkuPropsDTO;
import com.pap.item.item.entity.ItemSku;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface IItemSkuService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<ItemSku> selectListByMap(Map<Object, Object> map);

    int insert(ItemSku record);

    int insertSelective(ItemSku record);

    ItemSku selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ItemSku record);

    int updateByPrimaryKey(ItemSku record);


    // alexgaoyh added

    /**
     * 保存
     * @param itemSkuWithDetailDTO
     * @return
     */
    String createWithDetails(ItemSkuWithDetailDTO itemSkuWithDetailDTO);

    /**
     * 保存
     * @param itemSkuWithDetailDTOList
     * @param loginedUserVO
     * @return
     */
    void createWithDetails(List<ItemSkuWithDetailDTO> itemSkuWithDetailDTOList, LoginedUserVO loginedUserVO);

    /**
     * 更新用户信息(用户基本信息/首要联系人信息/紧急联系人信息)
     * @param itemSkuWithDetailDTO
     * @return
     */
    String modifyWithDetails(ItemSkuWithDetailDTO itemSkuWithDetailDTO);

    /**
     * 更新SKU商品信息
     * @param itemSkuWithDetailDTOList
     * @param loginedUserVO 用户信息
     * @return
     */
    void modifyWithDetails(List<ItemSkuWithDetailDTO> itemSkuWithDetailDTOList, LoginedUserVO loginedUserVO);

    /**
     * 删除
     * @param itemSkuId
     * @return
     */
    String deleteWithDetails(String itemSkuId);

    /**
     * 查询
     * @param itemSkuId
     * @return
     */
    ItemSkuWithDetailDTO selectWithDetails(String itemSkuId);

    /**
     * 根据商品货品编号，查询出来所有的商品明细信息
     * @param itemProductId
     * @return
     */
    List<ItemSkuWithDetailDTO> selectWithDetailsByProductId(String itemProductId);

    /**
     * 根据 商品SKU编号，查询出来对应的商品价格信息
     * @param itemSkuId
     * @return
     */
    BigDecimal selectItemPriceByItemSkuId(String itemSkuId);

    /**
     * 查询 SKU 属性 集合
     * @param itemProductId
     * @return
     */
    List<SkuPropsDTO> selectSkuPropsByItemProductId(String itemProductId);

    /**
     * 查询 SKU 属性 价格 库存
     * @param itemProductId
     * @return
     */
    List<SkuPriceAndStockDTO> selectSkuPriceAndStockByItemProductId(String itemProductId);

    /**
     * 查询 有赞 Vant SKU 属性 集合
     * @param itemProductId
     * @return
     */
    List<VantSkuPropsDTO> selectVantSkuPropsByItemProductId(String itemProductId);

    /**
     * 查询 有赞 Vant SKU 信息 集合
     * @param itemProductId
     * @return
     */
    List<Map<String, Object>> selectVantSkuListByItemProductId(String itemProductId);
}
