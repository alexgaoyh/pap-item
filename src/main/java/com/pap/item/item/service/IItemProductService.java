package com.pap.item.item.service;

import com.pap.item.dto.ItemProductWithDetailDTO;
import com.pap.item.dto.ItemProductTreeNodeVO;
import com.pap.item.item.entity.ItemProduct;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.response.ResponseVO;

import java.util.List;
import java.util.Map;

public interface IItemProductService {

    int deleteByPrimaryKey(String id);

    int selectCountByMap(Map<Object, Object> map);

    List<ItemProduct> selectListByMap(Map<Object, Object> map);

    int insert(ItemProduct record);

    int insertSelective(ItemProduct record);

    ItemProduct selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ItemProduct record);

    int updateByPrimaryKey(ItemProduct record);


    // alexgaoyh add

    /**
     * 商品货品树形结构，根据 license 获取
     * @param clientLicenseId
     * @param globalParentId 全局统一的默认顶层组织架构的编码为 -1
     */
    List<ItemProductTreeNodeVO> itemProductTreeJson(String clientLicenseId, String globalParentId);

    /**
     * 检查当前操作的商品货品编码，对照新维护的上级商品货品，是否被循环依赖
     * @param operaItemProductId
     * @param newParentItemProductId
     * @return
     */
    Boolean checkTreeCircularDependency(String operaItemProductId, String newParentItemProductId);

    /**
     * 商品货品更新操作,参数校验并且进行数据维护	pathIds pathCodes pathNames 三个字段的数据维护
     * @return
     */
    ResponseVO<ItemProduct> updateAndCheckItemProduct(ItemProduct inputItemProduct);

    /**
     * 维护当前商品货品节点下，包含的子集的商品货品的数据值,冗余字段的维护，
     * pathIds pathCodes pathNames
     * 请确保当前操作的 inputItemProductId，对应的实体数据已经是最新的。
     *
     * 递归调用，维护完毕所有子类的path冗余字段
     * @param inputItemProductId
     * @return
     */
    ResponseVO<ItemProduct> updateRecursionPathColumnItemProduct(String inputItemProductId);


    /**
     * 保存商品货品扩展属性明细相关数据
     * @param operObj
     * @return
     */
    String saveItemProductForDetailDTO(ItemProductWithDetailDTO operObj);

    /**
     * 更新商品货品扩展属性明细相关数据
     * @param operObj
     * @return
     */
    String updateItemProductForDetailDTO(ItemProductWithDetailDTO operObj);

    /**
     * 删除商品货品扩展属性明细相关数据
     * @param itemProductId
     * @return
     */
    int deleteItemProductWithDetailDTO(String itemProductId);

    /**
     * 查询商品货品扩展属性明细相关数据
     * @param itemProductId
     * @param loginedUserVO
     * @return
     */
    ItemProductWithDetailDTO selectItemProductWithDetailDTOById(String itemProductId, LoginedUserVO loginedUserVO);
}
