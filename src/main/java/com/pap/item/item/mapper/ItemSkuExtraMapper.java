package com.pap.item.item.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.item.dto.ItemSkuAttrNameAndValueDTO;
import com.pap.item.item.entity.ItemSkuExtra;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;;

@Mapper
public interface ItemSkuExtraMapper extends PapBaseMapper<ItemSkuExtra> {
    int deleteByPrimaryKey(String itemSkuExtraId);

    int selectCountByMap(Map<Object, Object> map);

    List<ItemSkuExtra> selectListByMap(Map<Object, Object> map);

    ItemSkuExtra selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(ItemSkuExtra record);

    int insertSelective(ItemSkuExtra record);

    ItemSkuExtra selectByPrimaryKey(String itemSkuExtraId);

    int updateByPrimaryKeySelective(ItemSkuExtra record);

    int updateByPrimaryKey(ItemSkuExtra record);

    // alexgaoyh

    /**
     * 根据 itemSkuId SKU编号，获取此可销售商品下，商品的属性、属性值
     * @param itemSkuId
     * @return
     */
    List<ItemSkuAttrNameAndValueDTO> selectItemSkuAttrNameAndValueByItemSkuId(@Param("itemSkuId") String itemSkuId);

}