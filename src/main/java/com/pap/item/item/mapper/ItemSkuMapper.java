package com.pap.item.item.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.item.dto.SkuPriceAndStockDTO;
import com.pap.item.dto.SkuPropsDTO;
import com.pap.item.dto.VantSkuPropsDTO;
import com.pap.item.item.entity.ItemSku;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface ItemSkuMapper extends PapBaseMapper<ItemSku> {
    int deleteByPrimaryKey(String itemSkuId);

    int selectCountByMap(Map<Object, Object> map);

    List<ItemSku> selectListByMap(Map<Object, Object> map);

    ItemSku selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(ItemSku record);

    int insertSelective(ItemSku record);

    ItemSku selectByPrimaryKey(String itemSkuId);

    int updateByPrimaryKeySelective(ItemSku record);

    int updateByPrimaryKey(ItemSku record);

    // alexgaoyh

    /**
     * 查询 SKU 属性 集合
     * @param itemProductId
     * @return
     */
    List<SkuPropsDTO> selectSkuPropsByItemProductId(@Param("itemProductId") String itemProductId);

    /**
     * 查询 SKU 属性 价格 库存
     * @param itemProductId
     * @return
     */
    List<SkuPriceAndStockDTO> selectSkuPriceAndStockByItemProductId(@Param("itemProductId") String itemProductId);

    /**
     * 查询 有赞 Vant SKU 属性 集合
     * @param itemProductId
     * @return
     */
    List<VantSkuPropsDTO> selectVantSkuPropsByItemProductId(@Param("itemProductId") String itemProductId);
}