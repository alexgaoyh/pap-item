package com.pap.item.item.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.item.dto.ItemProductTreeNodeVO;
import com.pap.item.item.entity.ItemProduct;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;;

@Mapper
public interface ItemProductMapper extends PapBaseMapper<ItemProduct> {
    int deleteByPrimaryKey(String itemProductId);

    int selectCountByMap(Map<Object, Object> map);

    List<ItemProduct> selectListByMap(Map<Object, Object> map);

    ItemProduct selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(ItemProduct record);

    int insertSelective(ItemProduct record);

    ItemProduct selectByPrimaryKey(String itemProductId);

    int updateByPrimaryKeySelective(ItemProduct record);

    int updateByPrimaryKey(ItemProduct record);

    // alexgaoyh added

    /**
     * 树形结构查询 商品货品信息
     * @param clientLicenseId
     * @param globalParentId
     * @return
     */
    List<ItemProductTreeNodeVO> itemProductTreeJson(@Param("clientLicenseId") String clientLicenseId,
                                                    @Param("globalParentId") String globalParentId);


}