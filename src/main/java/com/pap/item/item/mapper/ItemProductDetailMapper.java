package com.pap.item.item.mapper;

import com.pap.base.mapper.PapBaseMapper;
import com.pap.item.item.entity.ItemProductDetail;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;;

@Mapper
public interface ItemProductDetailMapper extends PapBaseMapper<ItemProductDetail> {
    int deleteByPrimaryKey(String itemProductDetailId);

    int selectCountByMap(Map<Object, Object> map);

    List<ItemProductDetail> selectListByMap(Map<Object, Object> map);

    ItemProductDetail selectByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int deleteByPrimaryKeyAndTableName(@Param("id") String id, @Param("dynamicTableName") String dynamicTableName);

    int insert(ItemProductDetail record);

    int insertSelective(ItemProductDetail record);

    ItemProductDetail selectByPrimaryKey(String itemProductDetailId);

    int updateByPrimaryKeySelective(ItemProductDetail record);

    int updateByPrimaryKey(ItemProductDetail record);

    // alexgaoyh added

    /**
     * 根据 货品编号 删除明细信息
     * @param itemProductId
     * @return
     */
    int deleteByItemProductId(@Param("itemProductId") String itemProductId);
}