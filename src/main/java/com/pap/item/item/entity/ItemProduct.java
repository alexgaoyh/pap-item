package com.pap.item.item.entity;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.*;
import java.io.Serializable;

@MyBatisTableAnnotation(name = "t_item_product", namespace = "com.pap.item.item.mapper.ItemProductMapper", remarks = " 修改点 ", aliasName = "t_item_product t_item_product" )
public class ItemProduct extends PapBaseEntity implements Serializable {
    /**
     *  货品编号,所属表字段为t_item_product.ITEM_PRODUCT_ID
     */
    @MyBatisColumnAnnotation(name = "ITEM_PRODUCT_ID", value = "t_item_product_ITEM_PRODUCT_ID", chineseNote = "货品编号", tableAlias = "t_item_product")
    @MyApiModelPropertyAnnotation(value = "货品编号")
    private String itemProductId;

    /**
     *  货品编码,所属表字段为t_item_product.ITEM_PRODUCT_CODE
     */
    @MyBatisColumnAnnotation(name = "ITEM_PRODUCT_CODE", value = "t_item_product_ITEM_PRODUCT_CODE", chineseNote = "货品编码", tableAlias = "t_item_product")
    @MyApiModelPropertyAnnotation(value = "货品编码")
    private String itemProductCode;

    /**
     *  货品名称,所属表字段为t_item_product.ITEM_PRODUCT_NAME
     */
    @MyBatisColumnAnnotation(name = "ITEM_PRODUCT_NAME", value = "t_item_product_ITEM_PRODUCT_NAME", chineseNote = "货品名称", tableAlias = "t_item_product")
    @MyApiModelPropertyAnnotation(value = "货品名称")
    private String itemProductName;

    /**
     *  父级货品编号,所属表字段为t_item_product.ITEM_PRODUCT_PARENT_ID
     */
    @MyBatisColumnAnnotation(name = "ITEM_PRODUCT_PARENT_ID", value = "t_item_product_ITEM_PRODUCT_PARENT_ID", chineseNote = "父级货品编号", tableAlias = "t_item_product")
    @MyApiModelPropertyAnnotation(value = "父级货品编号")
    private String itemProductParentId;

    /**
     *  所属商品品牌,所属表字段为t_item_product.ITEM_BRAND_ID
     */
    @MyBatisColumnAnnotation(name = "ITEM_BRAND_ID", value = "t_item_product_ITEM_BRAND_ID", chineseNote = "所属商品品牌", tableAlias = "t_item_product")
    @MyApiModelPropertyAnnotation(value = "所属商品品牌")
    private String itemBrandId;

    /**
     *  所属商品类目,所属表字段为t_item_product.ITEM_CATEGORY_ID
     */
    @MyBatisColumnAnnotation(name = "ITEM_CATEGORY_ID", value = "t_item_product_ITEM_CATEGORY_ID", chineseNote = "所属商品类目", tableAlias = "t_item_product")
    @MyApiModelPropertyAnnotation(value = "所属商品类目")
    private String itemCategoryId;

    /**
     *  编号路径集合,所属表字段为t_item_product.PATH_IDS
     */
    @MyBatisColumnAnnotation(name = "PATH_IDS", value = "t_item_product_PATH_IDS", chineseNote = "编号路径集合", tableAlias = "t_item_product")
    @MyApiModelPropertyAnnotation(value = "编号路径集合")
    private String pathIds;

    /**
     *  编码路径集合,所属表字段为t_item_product.PATH_CODES
     */
    @MyBatisColumnAnnotation(name = "PATH_CODES", value = "t_item_product_PATH_CODES", chineseNote = "编码路径集合", tableAlias = "t_item_product")
    @MyApiModelPropertyAnnotation(value = "编码路径集合")
    private String pathCodes;

    /**
     *  名称路径集合,所属表字段为t_item_product.PATH_NAMES
     */
    @MyBatisColumnAnnotation(name = "PATH_NAMES", value = "t_item_product_PATH_NAMES", chineseNote = "名称路径集合", tableAlias = "t_item_product")
    @MyApiModelPropertyAnnotation(value = "名称路径集合")
    private String pathNames;

    /**
     *  备注,所属表字段为t_item_product.REMARK
     */
    @MyBatisColumnAnnotation(name = "REMARK", value = "t_item_product_REMARK", chineseNote = "备注", tableAlias = "t_item_product")
    @MyApiModelPropertyAnnotation(value = "备注")
    private String remark;

    /**
     *  顺序号,所属表字段为t_item_product.SEQ_NO
     */
    @MyBatisColumnAnnotation(name = "SEQ_NO", value = "t_item_product_SEQ_NO", chineseNote = "顺序号", tableAlias = "t_item_product")
    @MyApiModelPropertyAnnotation(value = "顺序号")
    private Integer seqNo;

    @Override
    public String getDynamicTableName() {
        return "t_item_product";
    }

    private static final long serialVersionUID = 1L;

    public String getItemProductId() {
        return itemProductId;
    }

    public void setItemProductId(String itemProductId) {
        this.itemProductId = itemProductId;
    }

    public String getItemProductCode() {
        return itemProductCode;
    }

    public void setItemProductCode(String itemProductCode) {
        this.itemProductCode = itemProductCode;
    }

    public String getItemProductName() {
        return itemProductName;
    }

    public void setItemProductName(String itemProductName) {
        this.itemProductName = itemProductName;
    }

    public String getItemProductParentId() {
        return itemProductParentId;
    }

    public void setItemProductParentId(String itemProductParentId) {
        this.itemProductParentId = itemProductParentId;
    }

    public String getItemBrandId() {
        return itemBrandId;
    }

    public void setItemBrandId(String itemBrandId) {
        this.itemBrandId = itemBrandId;
    }

    public String getItemCategoryId() {
        return itemCategoryId;
    }

    public void setItemCategoryId(String itemCategoryId) {
        this.itemCategoryId = itemCategoryId;
    }

    public String getPathIds() {
        return pathIds;
    }

    public void setPathIds(String pathIds) {
        this.pathIds = pathIds;
    }

    public String getPathCodes() {
        return pathCodes;
    }

    public void setPathCodes(String pathCodes) {
        this.pathCodes = pathCodes;
    }

    public String getPathNames() {
        return pathNames;
    }

    public void setPathNames(String pathNames) {
        this.pathNames = pathNames;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(Integer seqNo) {
        this.seqNo = seqNo;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", itemProductId=").append(itemProductId);
        sb.append(", itemProductCode=").append(itemProductCode);
        sb.append(", itemProductName=").append(itemProductName);
        sb.append(", itemProductParentId=").append(itemProductParentId);
        sb.append(", itemBrandId=").append(itemBrandId);
        sb.append(", itemCategoryId=").append(itemCategoryId);
        sb.append(", pathIds=").append(pathIds);
        sb.append(", pathCodes=").append(pathCodes);
        sb.append(", pathNames=").append(pathNames);
        sb.append(", remark=").append(remark);
        sb.append(", seqNo=").append(seqNo);
        sb.append("]");
        return sb.toString();
    }
}