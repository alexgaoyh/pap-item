package com.pap.item.item.entity;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.*;
import java.io.Serializable;
import java.math.BigDecimal;

@MyBatisTableAnnotation(name = "t_item_sku", namespace = "com.pap.item.item.mapper.ItemSkuMapper", remarks = " 修改点 ", aliasName = "t_item_sku t_item_sku" )
public class ItemSku extends PapBaseEntity implements Serializable {
    /**
     *  商品SKU编号,所属表字段为t_item_sku.ITEM_SKU_ID
     */
    @MyBatisColumnAnnotation(name = "ITEM_SKU_ID", value = "t_item_sku_ITEM_SKU_ID", chineseNote = "商品SKU编号", tableAlias = "t_item_sku")
    @MyApiModelPropertyAnnotation(value = "商品SKU编号")
    private String itemSkuId;

    /**
     *  商品SKU编码,所属表字段为t_item_sku.ITEM_SKU_CODE
     */
    @MyBatisColumnAnnotation(name = "ITEM_SKU_CODE", value = "t_item_sku_ITEM_SKU_CODE", chineseNote = "商品SKU编码", tableAlias = "t_item_sku")
    @MyApiModelPropertyAnnotation(value = "商品SKU编码")
    private String itemSkuCode;

    /**
     *  商品SKU名称,所属表字段为t_item_sku.ITEM_SKU_NAME
     */
    @MyBatisColumnAnnotation(name = "ITEM_SKU_NAME", value = "t_item_sku_ITEM_SKU_NAME", chineseNote = "商品SKU名称", tableAlias = "t_item_sku")
    @MyApiModelPropertyAnnotation(value = "商品SKU名称")
    private String itemSkuName;

    /**
     *  所属货品编号,所属表字段为t_item_sku.ITEM_PRODUCT_ID
     */
    @MyBatisColumnAnnotation(name = "ITEM_PRODUCT_ID", value = "t_item_sku_ITEM_PRODUCT_ID", chineseNote = "所属货品编号", tableAlias = "t_item_sku")
    @MyApiModelPropertyAnnotation(value = "所属货品编号")
    private String itemProductId;

    /**
     *  商品SKU库存量,所属表字段为t_item_sku.ITEM_STOCK_NUM
     */
    @MyBatisColumnAnnotation(name = "ITEM_STOCK_NUM", value = "t_item_sku_ITEM_STOCK_NUM", chineseNote = "商品SKU库存量", tableAlias = "t_item_sku")
    @MyApiModelPropertyAnnotation(value = "商品SKU库存量")
    private Integer itemStockNum;

    /**
     *  商品SKU价格,所属表字段为t_item_sku.ITEM_PRICE
     */
    @MyBatisColumnAnnotation(name = "ITEM_PRICE", value = "t_item_sku_ITEM_PRICE", chineseNote = "商品SKU价格", tableAlias = "t_item_sku")
    @MyApiModelPropertyAnnotation(value = "商品SKU价格")
    private BigDecimal itemPrice;

    /**
     *  备注,所属表字段为t_item_sku.REMARK
     */
    @MyBatisColumnAnnotation(name = "REMARK", value = "t_item_sku_REMARK", chineseNote = "备注", tableAlias = "t_item_sku")
    @MyApiModelPropertyAnnotation(value = "备注")
    private String remark;

    /**
     *  顺序号,所属表字段为t_item_sku.SEQ_NO
     */
    @MyBatisColumnAnnotation(name = "SEQ_NO", value = "t_item_sku_SEQ_NO", chineseNote = "顺序号", tableAlias = "t_item_sku")
    @MyApiModelPropertyAnnotation(value = "顺序号")
    private Integer seqNo;

    /**
     *  可用状态标示(Y可用/N不可),所属表字段为t_item_sku.DISABLE_FLAG
     */
    @MyBatisColumnAnnotation(name = "DISABLE_FLAG", value = "t_item_sku_DISABLE_FLAG", chineseNote = "可用状态标示(Y可用/N不可)", tableAlias = "t_item_sku")
    @MyApiModelPropertyAnnotation(value = "可用状态标示(Y可用/N不可)")
    private String disableFlag;

    private static final long serialVersionUID = 1L;

    public String getItemSkuId() {
        return itemSkuId;
    }

    public void setItemSkuId(String itemSkuId) {
        this.itemSkuId = itemSkuId;
    }

    public String getItemSkuCode() {
        return itemSkuCode;
    }

    public void setItemSkuCode(String itemSkuCode) {
        this.itemSkuCode = itemSkuCode;
    }

    public String getItemSkuName() {
        return itemSkuName;
    }

    public void setItemSkuName(String itemSkuName) {
        this.itemSkuName = itemSkuName;
    }

    public String getItemProductId() {
        return itemProductId;
    }

    public void setItemProductId(String itemProductId) {
        this.itemProductId = itemProductId;
    }

    public Integer getItemStockNum() {
        return itemStockNum;
    }

    public void setItemStockNum(Integer itemStockNum) {
        this.itemStockNum = itemStockNum;
    }

    public BigDecimal getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(BigDecimal itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(Integer seqNo) {
        this.seqNo = seqNo;
    }

    public String getDisableFlag() {
        return disableFlag;
    }

    public void setDisableFlag(String disableFlag) {
        this.disableFlag = disableFlag;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", itemSkuId=").append(itemSkuId);
        sb.append(", itemSkuCode=").append(itemSkuCode);
        sb.append(", itemSkuName=").append(itemSkuName);
        sb.append(", itemProductId=").append(itemProductId);
        sb.append(", itemStockNum=").append(itemStockNum);
        sb.append(", itemPrice=").append(itemPrice);
        sb.append(", remark=").append(remark);
        sb.append(", seqNo=").append(seqNo);
        sb.append(", disableFlag=").append(disableFlag);
        sb.append("]");
        return sb.toString();
    }
}