package com.pap.item.item.controller;

import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.item.category.entity.ItemCategory;
import com.pap.item.category.service.IItemCategoryService;
import com.pap.item.dto.ItemProductWithDetailDTO;
import com.pap.item.item.service.IItemProductService;
import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.response.ResponseVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 处理完商品货品信息之后，单独维护商品货品扩展信息(按行存储)
 */
@RestController
@RequestMapping("/itemproductwithdetail")
@Api(value = "商品货品扩展属性管理", tags = "商品货品扩展属性管理", description="商品货品扩展属性管理")
public class ItemProductWithDetailController {

	private static Logger logger  =  LoggerFactory.getLogger(ItemProductWithDetailController.class);
	
	@Resource(name = "itemProductService")
	private IItemProductService itemProductService;

	@Resource(name = "itemCategoryService")
	private IItemCategoryService itemCategoryService;

	@Resource(name = "idWorker")
	private IdWorker idWorker;

	@ApiOperation("创建")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "itemProductWithDetailDTO", value = "", required = true, dataType = "ItemProductWithDetailDTO", paramType="body")
	})
	@PostMapping(value = "")
	public ResponseVO<ItemProductWithDetailDTO> create(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													 @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													 @RequestBody ItemProductWithDetailDTO itemProductWithDetailDTO,
													 @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		String idStr = idWorker.nextIdStr();
		itemProductWithDetailDTO.setItemProductId(idStr);

		BeanCopyUtilss.myCopyPropertiesForLoginedUser(itemProductWithDetailDTO, loginedUserVO, true);
		String strTemp = itemProductService.saveItemProductForDetailDTO(itemProductWithDetailDTO);

		ItemProductWithDetailDTO ItemProductWithDetailDTOTemp = itemProductService.selectItemProductWithDetailDTOById(strTemp, loginedUserVO);

		return ResponseVO.successdata(ItemProductWithDetailDTOTemp);
	}

	@ApiOperation("更新")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "itemProductWithDetailDTO", value = "", required = true, dataType = "ItemProductWithDetailDTO", paramType="body")
	})
	@PutMapping(value = "")
	public ResponseVO<ItemProductWithDetailDTO> update(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody ItemProductWithDetailDTO itemProductWithDetailDTO,
													 @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		BeanCopyUtilss.myCopyPropertiesForLoginedUser(itemProductWithDetailDTO, loginedUserVO, false);
		String strTemp = itemProductService.updateItemProductForDetailDTO(itemProductWithDetailDTO);

		ItemProductWithDetailDTO ItemProductWithDetailDTOTemp = itemProductService.selectItemProductWithDetailDTOById(strTemp, loginedUserVO);

		return ResponseVO.successdata(ItemProductWithDetailDTOTemp);
	}


	@ApiOperation("删除")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "itemProductWithDetailDTO", value = "", required = true, dataType = "ItemProductWithDetailDTO", paramType="body")
	})
	@DeleteMapping(value = "")
	public ResponseVO<String> delete(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody ItemProductWithDetailDTO itemProductWithDetailDTO){

		int i = itemProductService.deleteItemProductWithDetailDTO(itemProductWithDetailDTO.getItemProductId());

		return ResponseVO.successdata("操作成功");
	}

	@ApiOperation("详情")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "id", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/{id}")
	public ResponseVO<ItemProductWithDetailDTO> selectOne(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@PathVariable("id") String id,
													@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		ItemProductWithDetailDTO itemProductWithDetailDTO = itemProductService.selectItemProductWithDetailDTOById(id, loginedUserVO);

		// 维护所属类目的父级节点信息 itemCategoryIds
		ItemCategory itemCategory = itemCategoryService.selectByPrimaryKey(itemProductWithDetailDTO.getItemCategoryId());
		if(itemCategory != null && itemCategory.getItemCategoryId() != null) {
			itemProductWithDetailDTO.setItemCategoryIds(itemCategory.getPathIds());
		}

		return ResponseVO.successdata(itemProductWithDetailDTO);
	}

	private List<ItemProductWithDetailDTO> toDTO(List<ItemProductWithDetailDTO> databaseList) {
		List<ItemProductWithDetailDTO> returnList = new ArrayList<ItemProductWithDetailDTO>();
		if(databaseList != null) {
			for(ItemProductWithDetailDTO dbEntity : databaseList) {
				ItemProductWithDetailDTO dtoTemp = new ItemProductWithDetailDTO();
				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
				returnList.add(dtoTemp);
			}
		}
		return returnList;
	}

	private List<ItemProductWithDetailDTO> toDB(List<ItemProductWithDetailDTO> dtoList) {
		List<ItemProductWithDetailDTO> returnList = new ArrayList<ItemProductWithDetailDTO>();
		if(dtoList != null) {
			for(ItemProductWithDetailDTO dtoEntity : dtoList) {
				ItemProductWithDetailDTO dbTemp = new ItemProductWithDetailDTO();
				BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
				returnList.add(dbTemp);
			}
		}
		return returnList;
	}
}
