package com.pap.item.item.controller;

import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.item.dto.ItemSkuExtraDTO;
import com.pap.item.item.entity.ItemSkuExtra;
import com.pap.item.item.service.IItemSkuExtraService;
import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.page.Page;
import com.pap.obj.vo.response.ResponseVO;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


//@RestController
//@RequestMapping("/itemskuextra")
//@Api(value = "", tags = "", description="")
@Deprecated
public class ItemSkuExtraController {

	private static Logger logger  =  LoggerFactory.getLogger(ItemSkuExtraController.class);
	
	@Resource(name = "itemSkuExtraService")
	private IItemSkuExtraService itemSkuExtraService;

	@Resource(name = "idWorker")
	private IdWorker idWorker;

	@ApiOperation("创建")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "itemSkuExtraDTO", value = "", required = true, dataType = "ItemSkuExtraDTO", paramType="body")
	})
	@PostMapping(value = "")
	public ResponseVO<ItemSkuExtraDTO> create(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
											  @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
											  @RequestBody ItemSkuExtraDTO itemSkuExtraDTO,
											  @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		String idStr = idWorker.nextIdStr();
		// TODO 设置主键
		itemSkuExtraDTO.setItemSkuExtraId(idStr);

		ItemSkuExtra databaseObj = new ItemSkuExtra();
		BeanCopyUtilss.myCopyProperties(itemSkuExtraDTO, databaseObj, loginedUserVO);

		int i = itemSkuExtraService.insertSelective(databaseObj);

		return ResponseVO.successdata(itemSkuExtraDTO);
	}

	@ApiOperation("更新")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "itemSkuExtraDTO", value = "", required = true, dataType = "ItemSkuExtraDTO", paramType="body")
	})
	@PutMapping(value = "")
	public ResponseVO<ItemSkuExtraDTO> update(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody ItemSkuExtraDTO itemSkuExtraDTO,
													@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		ItemSkuExtra databaseObj = new ItemSkuExtra();
		BeanCopyUtilss.myCopyProperties(itemSkuExtraDTO, databaseObj, loginedUserVO, false);
		int i = itemSkuExtraService.updateByPrimaryKeySelective(databaseObj);

		// TODO 主键部分
		databaseObj = itemSkuExtraService.selectByPrimaryKey(databaseObj.getItemSkuExtraId());

		return ResponseVO.successdata(databaseObj);
	}


	@ApiOperation("删除")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "itemSkuExtraDTO", value = "", required = true, dataType = "ItemSkuExtraDTO", paramType="body")
	})
	@DeleteMapping(value = "")
	public ResponseVO<String> delete(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody ItemSkuExtraDTO itemSkuExtraDTO,
													@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		ItemSkuExtra databaseObj = new ItemSkuExtra();
		BeanCopyUtilss.myCopyProperties(itemSkuExtraDTO, databaseObj);

		// TODO 主键部分
		int i = itemSkuExtraService.deleteByPrimaryKey(databaseObj.getItemSkuExtraId());

		return ResponseVO.successdata("操作成功");
	}

	@ApiOperation("详情")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "id", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/{id}")
	public ResponseVO<ItemSkuExtraDTO> selectOne(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@PathVariable("id") String id){

		ItemSkuExtra databaseObj = itemSkuExtraService.selectByPrimaryKey(id);

		return ResponseVO.successdata(databaseObj);
	}

	@ApiOperation("查询")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "itemSkuExtraDTO", value = "应用查询参数", required = false, dataType = "ItemSkuExtraDTO", paramType="body"),
		@ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
		@ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
	})
	@PostMapping(value = "/query")
	public ResponseVO<ItemSkuExtraDTO>query(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												@RequestBody ItemSkuExtraDTO itemSkuExtraDTO,
												@RequestParam Integer pageNo, @RequestParam Integer pageSize,
												@RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
												@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		Map<Object, Object> map = BeanCopyUtilss.beanToMap(itemSkuExtraDTO, loginedUserVO);
		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			map.put("page", newPage);
		}
		List<ItemSkuExtra> list = itemSkuExtraService.selectListByMap(map);

		//
		List<ItemSkuExtraDTO> dtoList = toDTO(list);

		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			int countNum = itemSkuExtraService.selectCountByMap(map);
			newPage.setCount(countNum);
			return ResponseVO.successdatas(dtoList, newPage);
		} else {
			return ResponseVO.successdatas(dtoList, null);
		}

	}

	private List<ItemSkuExtraDTO> toDTO(List<ItemSkuExtra> databaseList) {
		List<ItemSkuExtraDTO> returnList = new ArrayList<ItemSkuExtraDTO>();
		if(databaseList != null) {
			for(ItemSkuExtra dbEntity : databaseList) {
				ItemSkuExtraDTO dtoTemp = new ItemSkuExtraDTO();
				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
				returnList.add(dtoTemp);
			}
		}
		return returnList;
	}

	private List<ItemSkuExtra> toDB(List<ItemSkuExtraDTO> dtoList) {
		List<ItemSkuExtra> returnList = new ArrayList<ItemSkuExtra>();
		if(dtoList != null) {
			for(ItemSkuExtraDTO dtoEntity : dtoList) {
				ItemSkuExtra dbTemp = new ItemSkuExtra();
				BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
				returnList.add(dbTemp);
			}
		}
		return returnList;
	}
}
