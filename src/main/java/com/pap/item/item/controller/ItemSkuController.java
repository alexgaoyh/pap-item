package com.pap.item.item.controller;

import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.item.dto.*;
import com.pap.item.item.entity.ItemSku;
import com.pap.item.item.service.IItemSkuService;
import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.page.Page;
import com.pap.obj.vo.response.ResponseVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/itemsku")
@Api(value = "商品SKU信息操作", tags = "商品SKU信息操作", description="商品SKU信息操作")
public class ItemSkuController {

	private static Logger logger  =  LoggerFactory.getLogger(ItemSkuController.class);
	
	@Resource(name = "itemSkuService")
	private IItemSkuService itemSkuService;

	@Resource(name = "idWorker")
	private IdWorker idWorker;

	@ApiOperation("创建")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "itemSkuWithDetailDTOList", value = "", required = true, dataType = "ItemSkuWithDetailDTO", paramType="body")
	})
	@PostMapping(value = "")
	public ResponseVO<List<ItemSkuWithDetailDTO>> create(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
										 @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
										 @RequestBody List<ItemSkuWithDetailDTO> itemSkuWithDetailDTOList,
										 @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		itemSkuService.createWithDetails(itemSkuWithDetailDTOList, loginedUserVO);

		List<ItemSkuWithDetailDTO> temp = itemSkuService.selectWithDetailsByProductId(itemSkuWithDetailDTOList.get(0).getItemProductId());

		return ResponseVO.successdatas(temp, null);
	}

	@ApiOperation("更新")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "itemSkuWithDetailDTOList", value = "", required = true, dataType = "ItemSkuWithDetailDTO", paramType="body")
	})
	@PutMapping(value = "")
	public ResponseVO<List<ItemSkuWithDetailDTO>> update(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody List<ItemSkuWithDetailDTO> itemSkuWithDetailDTOList,
													@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		itemSkuService.modifyWithDetails(itemSkuWithDetailDTOList, loginedUserVO);

		List<ItemSkuWithDetailDTO> temp = itemSkuService.selectWithDetailsByProductId(itemSkuWithDetailDTOList.get(0).getItemProductId());

		return ResponseVO.successdatas(temp, null);
	}


	@ApiOperation("删除")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "itemSkuDTO", value = "", required = true, dataType = "ItemSkuWithDetailDTO", paramType="body")
	})
	@DeleteMapping(value = "")
	public ResponseVO<String> delete(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody ItemSkuWithDetailDTO itemSkuDTO,
													@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		String skuIdStr = itemSkuService.deleteWithDetails(itemSkuDTO.getItemSkuId());

		return ResponseVO.successdata("操作成功");
	}

	@ApiOperation("详情")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "id", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/{id}")
	public ResponseVO<ItemSkuWithDetailDTO> selectOne(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@PathVariable("id") String id){

		ItemSkuWithDetailDTO temp = itemSkuService.selectWithDetails(id);

		return ResponseVO.successdata(temp);
	}

	@ApiOperation("商品SKU属性对象集合")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "itemProductId", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/skuprops/{itemProductId}")
	public ResponseVO<List<SkuPropsDTO>> selectSkuPropsByItemProductId(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													  @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													  @PathVariable("itemProductId") String itemProductId){

		List<SkuPropsDTO> temp = itemSkuService.selectSkuPropsByItemProductId(itemProductId);

		return ResponseVO.successdatas(temp, null);
	}

	@ApiOperation("商品有赞SKU属性对象集合")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "itemProductId", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/vantskuprops/{itemProductId}")
	public ResponseVO<List<VantSkuPropsDTO>> selectVantSkuPropsByItemProductId(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
																	   @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
																	   @PathVariable("itemProductId") String itemProductId){

		List<VantSkuPropsDTO> temp = itemSkuService.selectVantSkuPropsByItemProductId(itemProductId);

		return ResponseVO.successdatas(temp, null);
	}

	@ApiOperation("商品有赞SKU商品信息集合")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "itemProductId", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/vantskulist/{itemProductId}")
	public ResponseVO<List<Map<String, Object>>> selectVantSkuListByItemProductId(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
																			   @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
																			   @PathVariable("itemProductId") String itemProductId){

		List<Map<String, Object>> temp = itemSkuService.selectVantSkuListByItemProductId(itemProductId);

		return ResponseVO.successdatas(temp, null);
	}

	@ApiOperation("商品SKU属性对象集合")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "itemProductId", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/skupriceandcount/{itemProductId}")
	public ResponseVO<Map<String, SkuPriceAndStockDTO>> selectSkuPriceAndStockByItemProductId(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
																	   @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
																	   @PathVariable("itemProductId") String itemProductId){
		Map<String, SkuPriceAndStockDTO> returnMap = new HashMap<String, SkuPriceAndStockDTO>();

		List<SkuPriceAndStockDTO> temp = itemSkuService.selectSkuPriceAndStockByItemProductId(itemProductId);
		if(temp != null && temp.size() > 0) {
			for(SkuPriceAndStockDTO skuPriceAndStockDTO : temp) {
				returnMap.put(skuPriceAndStockDTO.getName(), skuPriceAndStockDTO);
			}
		}

		return ResponseVO.successdata(returnMap);
	}

	@ApiOperation("根据SKU商品编号获取商品价格")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "itemSkuId", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/itemprice/{itemSkuId}")
	public ResponseVO<BigDecimal> selectItemPriceByItemSkuId(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
											@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
											@PathVariable("itemSkuId") String itemSkuId){

		BigDecimal itemPriceBigDec = itemSkuService.selectItemPriceByItemSkuId(itemSkuId);

		return ResponseVO.successdata(itemPriceBigDec);
	}

	@ApiOperation("根据商品货品编号获取商品SKU信息集合")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "itemProductId", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/selectbyproductid/{itemProductId}")
	public ResponseVO<List<ItemSkuWithDetailDTO>> selectListByItemProductId(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
											@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
											@PathVariable("itemProductId") String itemProductId){

		List<ItemSkuWithDetailDTO> tempList = itemSkuService.selectWithDetailsByProductId(itemProductId);

		return ResponseVO.successdatas(tempList, null);
	}

	@ApiOperation("查询")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "itemSkuDTO", value = "应用查询参数", required = false, dataType = "ItemSkuDTO", paramType="body"),
		@ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
		@ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
	})
	@PostMapping(value = "/query")
	public ResponseVO<ItemSkuDTO>query(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												@RequestBody ItemSkuDTO itemSkuDTO,
												@RequestParam Integer pageNo, @RequestParam Integer pageSize,
												@RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
												@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		Map<Object, Object> map = BeanCopyUtilss.beanToMap(itemSkuDTO, loginedUserVO);
		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			map.put("page", newPage);
		}
		List<ItemSku> list = itemSkuService.selectListByMap(map);

		//
		List<ItemSkuDTO> dtoList = toDTO(list);

		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			int countNum = itemSkuService.selectCountByMap(map);
			newPage.setCount(countNum);
			return ResponseVO.successdatas(dtoList, newPage);
		} else {
			return ResponseVO.successdatas(dtoList, null);
		}

	}

	private List<ItemSkuDTO> toDTO(List<ItemSku> databaseList) {
		List<ItemSkuDTO> returnList = new ArrayList<ItemSkuDTO>();
		if(databaseList != null) {
			for(ItemSku dbEntity : databaseList) {
				ItemSkuDTO dtoTemp = new ItemSkuDTO();
				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
				returnList.add(dtoTemp);
			}
		}
		return returnList;
	}

	private List<ItemSku> toDB(List<ItemSkuDTO> dtoList) {
		List<ItemSku> returnList = new ArrayList<ItemSku>();
		if(dtoList != null) {
			for(ItemSkuDTO dtoEntity : dtoList) {
				ItemSku dbTemp = new ItemSku();
				BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
				returnList.add(dbTemp);
			}
		}
		return returnList;
	}
}
