package com.pap.item.item.controller;

import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.item.dto.ItemProductDetailDTO;
import com.pap.item.item.entity.ItemProductDetail;
import com.pap.item.item.service.IItemProductDetailService;
import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.page.Page;
import com.pap.obj.vo.response.ResponseVO;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


//@RestController
//@RequestMapping("/itemproductdetail")
//@Api(value = "货品扩展属性信息维护", tags = "货品扩展属性信息维护", description="货品扩展属性信息维护")
@Deprecated
public class ItemProductDetailController {

	private static Logger logger  =  LoggerFactory.getLogger(ItemProductDetailController.class);
	
	@Resource(name = "itemProductDetailService")
	private IItemProductDetailService itemProductDetailService;

	@Resource(name = "idWorker")
	private IdWorker idWorker;

	@ApiOperation("创建")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "itemProductDetailDTO", value = "", required = true, dataType = "ItemProductDetailDTO", paramType="body")
	})
	@PostMapping(value = "")
	public ResponseVO<ItemProductDetailDTO> create(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												   @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												   @RequestBody ItemProductDetailDTO itemProductDetailDTO){
		String idStr = idWorker.nextIdStr();
		itemProductDetailDTO.setItemProductDetailId(idStr);

		ItemProductDetail databaseObj = new ItemProductDetail();
		BeanCopyUtilss.myCopyProperties(itemProductDetailDTO, databaseObj);

		int i = itemProductDetailService.insertSelective(databaseObj);

		return ResponseVO.successdata(itemProductDetailDTO);
	}

	@ApiOperation("更新")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "itemProductDetailDTO", value = "", required = true, dataType = "ItemProductDetailDTO", paramType="body")
	})
	@PutMapping(value = "")
	public ResponseVO<ItemProductDetailDTO> update(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody ItemProductDetailDTO itemProductDetailDTO){

		ItemProductDetail databaseObj = new ItemProductDetail();
		BeanCopyUtilss.myCopyProperties(itemProductDetailDTO, databaseObj);
		int i = itemProductDetailService.updateByPrimaryKeySelective(databaseObj);

		databaseObj = itemProductDetailService.selectByPrimaryKey(databaseObj.getItemProductDetailId());

		return ResponseVO.successdata(databaseObj);
	}


	@ApiOperation("删除")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "itemProductDetailDTO", value = "", required = true, dataType = "ItemProductDetailDTO", paramType="body")
	})
	@DeleteMapping(value = "")
	public ResponseVO<String> delete(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody ItemProductDetailDTO itemProductDetailDTO){

		ItemProductDetail databaseObj = new ItemProductDetail();
		BeanCopyUtilss.myCopyProperties(itemProductDetailDTO, databaseObj);

		int i = itemProductDetailService.deleteByPrimaryKey(databaseObj.getItemProductDetailId());

		return ResponseVO.successdata("操作成功");
	}

	@ApiOperation("详情")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "id", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/{id}")
	public ResponseVO<ItemProductDetailDTO> selectOne(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@PathVariable("id") String id){

		ItemProductDetail databaseObj = itemProductDetailService.selectByPrimaryKey(id);

		return ResponseVO.successdata(databaseObj);
	}

	@ApiOperation("查询")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "itemProductDetailDTO", value = "应用查询参数", required = false, dataType = "ItemProductDetailDTO", paramType="body"),
		@ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
		@ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
	})
	@PostMapping(value = "/query")
	public ResponseVO<ItemProductDetailDTO>query(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												@RequestBody ItemProductDetailDTO itemProductDetailDTO,
												@RequestParam Integer pageNo, @RequestParam Integer pageSize,
												@RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
												@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		Map<Object, Object> map = BeanCopyUtilss.beanToMap(itemProductDetailDTO, loginedUserVO);
		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			map.put("page", newPage);
		}
		List<ItemProductDetail> list = itemProductDetailService.selectListByMap(map);

		//
		List<ItemProductDetailDTO> dtoList = toDTO(list);

		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			int countNum = itemProductDetailService.selectCountByMap(map);
			newPage.setCount(countNum);
			return ResponseVO.successdatas(dtoList, newPage);
		} else {
			return ResponseVO.successdatas(dtoList, null);
		}

	}

	private List<ItemProductDetailDTO> toDTO(List<ItemProductDetail> databaseList) {
		List<ItemProductDetailDTO> returnList = new ArrayList<ItemProductDetailDTO>();
		if(databaseList != null) {
			for(ItemProductDetail dbEntity : databaseList) {
				ItemProductDetailDTO dtoTemp = new ItemProductDetailDTO();
				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
				returnList.add(dtoTemp);
			}
		}
		return returnList;
	}

	private List<ItemProductDetail> toDB(List<ItemProductDetailDTO> dtoList) {
		List<ItemProductDetail> returnList = new ArrayList<ItemProductDetail>();
		if(dtoList != null) {
			for(ItemProductDetailDTO dtoEntity : dtoList) {
				ItemProductDetail dbTemp = new ItemProductDetail();
				BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
				returnList.add(dbTemp);
			}
		}
		return returnList;
	}
}
