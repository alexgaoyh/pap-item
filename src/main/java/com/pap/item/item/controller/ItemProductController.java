package com.pap.item.item.controller;

import com.github.stuxuhai.jpinyin.PinyinFormat;
import com.github.stuxuhai.jpinyin.PinyinHelper;
import com.pap.base.util.bean.BeanCopyUtilss;
import com.pap.base.util.string.StringUtilss;
import com.pap.beans.idworker.IdWorker;
import com.pap.item.dto.ItemProductDTO;
import com.pap.item.dto.ItemProductTreeNodeVO;
import com.pap.item.item.entity.ItemProduct;
import com.pap.item.item.service.IItemProductService;
import com.pap.obj.vo.logineduser.LoginedUser;
import com.pap.obj.vo.logineduser.LoginedUserVO;
import com.pap.obj.vo.page.Page;
import com.pap.obj.vo.response.ResponseVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 货品信息维护为第一步
 * 货品信息维护完毕之后，跳转到货品扩展属性的维护处理逻辑，此处理逻辑按行存储，将此货品下的扩展属性存储，便于后续生成SKU使用
 */
@RestController
@RequestMapping("/itemproduct")
@Api(value = "货品信息维护", tags = "货品信息维护", description="货品信息维护")
public class ItemProductController {

	private static Logger logger  =  LoggerFactory.getLogger(ItemProductController.class);
	
	@Resource(name = "itemProductService")
	private IItemProductService itemProductService;

	@Resource(name = "idWorker")
	private IdWorker idWorker;

	@ApiOperation("创建")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "itemProductDTO", value = "", required = true, dataType = "ItemProductDTO", paramType="body")
	})
	@PostMapping(value = "")
	public ResponseVO<ItemProductDTO> create(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
											 @RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
											 @RequestBody ItemProductDTO itemProductDTO,
											 @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO) throws Exception {
		String idStr = idWorker.nextIdStr();
		itemProductDTO.setItemProductId(idStr);

		if(itemProductDTO != null &&
				StringUtilss.isNotEmpty(itemProductDTO.getItemProductName())) {
			// 如果前端没有传递过来code值，则默认使用拼音代替
			if(StringUtilss.isEmpty(itemProductDTO.getItemProductCode())) {
				itemProductDTO.setItemProductCode(PinyinHelper.convertToPinyinString(itemProductDTO.getItemProductName(), "", PinyinFormat.WITHOUT_TONE));
			}
			// 处理父级数据
			ItemProduct parentitemProduct = itemProductService.selectByPrimaryKey(itemProductDTO.getItemProductParentId());
			if(parentitemProduct != null) {
				// pathId
				if(StringUtilss.isNotEmpty(parentitemProduct.getPathIds())) {
					itemProductDTO.setPathIds(parentitemProduct.getPathIds() + "," + parentitemProduct.getItemProductId());
				} else {
					itemProductDTO.setPathIds(parentitemProduct.getItemProductId());
				}
				// pathCode
				if(StringUtilss.isNotEmpty(parentitemProduct.getPathCodes())) {
					itemProductDTO.setPathCodes(parentitemProduct.getPathCodes() + "," + parentitemProduct.getItemProductCode());
				} else {
					itemProductDTO.setPathCodes(parentitemProduct.getItemProductCode());
				}
				// pathName
				if(StringUtilss.isNotEmpty(parentitemProduct.getPathNames())) {
					itemProductDTO.setPathNames(parentitemProduct.getPathNames() + "," + parentitemProduct.getItemProductName());
				} else {
					itemProductDTO.setPathNames(parentitemProduct.getItemProductName());
				}
			} else {
				itemProductDTO.setItemProductParentId(null);
				itemProductDTO.setPathIds(null);
				itemProductDTO.setPathCodes(null);
				itemProductDTO.setPathNames(null);
			}
			itemProductDTO.setItemProductParentId(StringUtilss.isNotEmpty(itemProductDTO.getItemProductParentId()) == true ? itemProductDTO.getItemProductParentId() : "-1");

			ItemProduct dbitemProduct = new ItemProduct();
			BeanCopyUtilss.myCopyProperties(itemProductDTO, dbitemProduct, loginedUserVO);
			int operationInt = itemProductService.insertSelective(dbitemProduct);

			return ResponseVO.successdata(itemProductDTO);

		} else {
			return ResponseVO.validfail("入参异常，请判断是否含有商品货品名称");
		}

	}

	@ApiOperation("更新")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "itemProductDTO", value = "", required = true, dataType = "ItemProductDTO", paramType="body")
	})
	@PutMapping(value = "")
	public ResponseVO<ItemProductDTO> update(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody ItemProductDTO itemProductDTO,
											 @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){


		ItemProduct databaseObj = new ItemProduct();
		BeanCopyUtilss.myCopyProperties(itemProductDTO, databaseObj, loginedUserVO, false);

		// 重新更新实体对象和扩展字段
		int i = itemProductService.updateByPrimaryKeySelective(databaseObj);

		ItemProduct dbItemProduct = new ItemProduct();
		BeanCopyUtilss.myCopyProperties(itemProductDTO, dbItemProduct, loginedUserVO, false);
		ResponseVO responseVO = itemProductService.updateAndCheckItemProduct(dbItemProduct);

		// 查询最新的数据进行返回
		ItemProduct itemProductReturn = itemProductService.selectByPrimaryKey(itemProductDTO.getItemProductId());

		ItemProduct itemProductDTOReturn = new ItemProduct();
		BeanCopyUtilss.myCopyProperties(itemProductReturn, itemProductDTOReturn);

		return ResponseVO.successdata(itemProductDTOReturn);
	}


	@ApiOperation("删除")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "itemProductDTO", value = "", required = true, dataType = "ItemProductDTO", paramType="body")
	})
	@DeleteMapping(value = "")
	public ResponseVO<String> delete(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@RequestBody ItemProductDTO itemProductDTO,
									 @LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){

		if(itemProductDTO != null && StringUtilss.isNotEmpty(itemProductDTO.getItemProductId())) {
			Map<Object, Object> pathIdsMap = new HashMap<Object, Object>();
			pathIdsMap.put("pathIds", itemProductDTO.getItemProductId());
			List<ItemProduct> ItemProductList = itemProductService.selectListByMap(pathIdsMap);
			if(ItemProductList != null && ItemProductList.size() > 0) {
				return ResponseVO.validfail("请检查入参，此货品被其他货品所依赖!");
			} else {
				int operationInt = itemProductService.deleteByPrimaryKey(itemProductDTO.getItemProductId());
				return ResponseVO.successdata("操作成功");
			}
		} else {
			return ResponseVO.validfail("入参异常，请判断是否含有货品编码");
		}
	}

	@ApiOperation("详情")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "id", value = "编号", required = true, dataType = "String", paramType="path")
	})
	@GetMapping(value = "/{id}")
	public ResponseVO<ItemProductDTO> selectOne(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
													@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
													@PathVariable("id") String id){

		ItemProduct databaseObj = itemProductService.selectByPrimaryKey(id);

		return ResponseVO.successdata(databaseObj);
	}

	@ApiOperation("查询")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String"),
		@ApiImplicitParam(name = "itemProductDTO", value = "应用查询参数", required = false, dataType = "ItemProductDTO", paramType="body"),
		@ApiImplicitParam(name = "pageNo", value = "页码", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "pageSize", value = "每页记录数", required = true, dataType = "int", paramType="query"),
		@ApiImplicitParam(name = "sortBy", value = "按照哪个属性排序", required = false, dataType = "String", paramType="query"),
		@ApiImplicitParam(name = "order", value = "排序顺序", required = false, dataType = "String", paramType="query")
	})
	@PostMapping(value = "/query")
	public ResponseVO<ItemProductDTO>query(@RequestHeader(value = "papToken", defaultValue = "-1") String papToken,
												@RequestHeader(value = "papVersion", defaultValue = "1.0") String papVersion,
												@RequestBody ItemProductDTO itemProductDTO,
												@RequestParam Integer pageNo, @RequestParam Integer pageSize,
												@RequestParam(defaultValue = "create_time") String sortBy, @RequestParam(defaultValue = "desc") String order,
												@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO){
		Map<Object, Object> map = BeanCopyUtilss.beanToMap(itemProductDTO, loginedUserVO);
		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			map.put("page", newPage);
		}
		List<ItemProduct> list = itemProductService.selectListByMap(map);

		//
		List<ItemProductDTO> dtoList = toDTO(list);

		if(pageSize != 0) {
			Page newPage = new Page(pageNo + "", pageSize + "");
			int countNum = itemProductService.selectCountByMap(map);
			newPage.setCount(countNum);
			return ResponseVO.successdatas(dtoList, newPage);
		} else {
			return ResponseVO.successdatas(dtoList, null);
		}

	}

	@ApiOperation("树形结构查询")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "papToken", value = "登录标识", required = false, paramType = "header", dataType = "String"),
			@ApiImplicitParam(name = "papVersion", value = "版本号", required = false, paramType = "header", dataType = "String")
	})
	@RequestMapping(value = "/treejson", method = {RequestMethod.GET})
	public ResponseVO<List<ItemProductTreeNodeVO>> treeJSON(@LoginedUser @ApiIgnore LoginedUserVO loginedUserVO) {
		//  全局默认组织类别的顶层编码为 -1 
		String globalParentId = "-1";
		return ResponseVO.successdatas(itemProductService.itemProductTreeJson(loginedUserVO != null ? loginedUserVO.getClientLicenseId() : "", globalParentId), null);
	}

	private List<ItemProductDTO> toDTO(List<ItemProduct> databaseList) {
		List<ItemProductDTO> returnList = new ArrayList<ItemProductDTO>();
		if(databaseList != null) {
			for(ItemProduct dbEntity : databaseList) {
				ItemProductDTO dtoTemp = new ItemProductDTO();
				BeanCopyUtilss.myCopyProperties(dbEntity, dtoTemp);
				returnList.add(dtoTemp);
			}
		}
		return returnList;
	}

	private List<ItemProduct> toDB(List<ItemProductDTO> dtoList) {
		List<ItemProduct> returnList = new ArrayList<ItemProduct>();
		if(dtoList != null) {
			for(ItemProductDTO dtoEntity : dtoList) {
				ItemProduct dbTemp = new ItemProduct();
				BeanCopyUtilss.myCopyProperties(dtoEntity, dbTemp);
				returnList.add(dbTemp);
			}
		}
		return returnList;
	}
}
