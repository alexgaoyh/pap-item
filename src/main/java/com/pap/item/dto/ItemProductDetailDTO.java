package com.pap.item.dto;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.MyBatisColumnAnnotation;
import com.pap.base.mybatis.plugin.annotation.MyBatisTableAnnotation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value = "ItemProductDetailDTO", description = "商品货品明细信息实体")
public class ItemProductDetailDTO extends PapBaseEntity implements Serializable {
    /**
     *  货品扩展属性编号,所属表字段为t_item_product_detail.ITEM_PRODUCT_DETAIL_ID
     */
    @MyBatisColumnAnnotation(name = "ITEM_PRODUCT_DETAIL_ID", value = "t_item_product_detail_ITEM_PRODUCT_DETAIL_ID", chineseNote = "货品扩展属性编号", tableAlias = "t_item_product_detail")
    @ApiModelProperty(value = "货品扩展属性编号")
    private String itemProductDetailId;

    /**
     *  所属货品,所属表字段为t_item_product_detail.ITEM_PRODUCT_ID
     */
    @MyBatisColumnAnnotation(name = "ITEM_PRODUCT_ID", value = "t_item_product_detail_ITEM_PRODUCT_ID", chineseNote = "所属货品", tableAlias = "t_item_product_detail")
    @ApiModelProperty(value = "所属货品")
    private String itemProductId;

    /**
     *  所属商品类目属性,所属表字段为t_item_product_detail.ITEM_CATEGORY_ATTR_KEY_ID
     */
    @MyBatisColumnAnnotation(name = "ITEM_CATEGORY_ATTR_KEY_ID", value = "t_item_product_detail_ITEM_CATEGORY_ATTR_KEY_ID", chineseNote = "所属商品类目属性", tableAlias = "t_item_product_detail")
    @ApiModelProperty(value = "所属商品类目属性")
    private String itemCategoryAttrKeyId;

    /**
     *  所属商品类目属性值,所属表字段为t_item_product_detail.ITEM_CATEGORY_ATTR_VALUE_ID
     */
    @MyBatisColumnAnnotation(name = "ITEM_CATEGORY_ATTR_VALUE_ID", value = "t_item_product_detail_ITEM_CATEGORY_ATTR_VALUE_ID", chineseNote = "所属商品类目属性值", tableAlias = "t_item_product_detail")
    @ApiModelProperty(value = "所属商品类目属性值")
    private String itemCategoryAttrValueId;

    /**
     *  用户自定义值(允许自定义输入),所属表字段为t_item_product_detail.SELF_DEFINING_VALUE
     */
    @MyBatisColumnAnnotation(name = "SELF_DEFINING_VALUE", value = "t_item_product_detail_SELF_DEFINING_VALUE", chineseNote = "用户自定义值(允许自定义输入)", tableAlias = "t_item_product_detail")
    @ApiModelProperty(value = "用户自定义值(允许自定义输入)")
    private String selfDefiningValue;

    // alexgaoyh

    /**
     * 属性名称
     */
    private String itemCategoryAttrKeyName;

    @Override
    public String getDynamicTableName() {
        return "t_item_product_detail";
    }

    private static final long serialVersionUID = 1L;

    public String getItemProductDetailId() {
        return itemProductDetailId;
    }

    public void setItemProductDetailId(String itemProductDetailId) {
        this.itemProductDetailId = itemProductDetailId;
    }

    public String getItemProductId() {
        return itemProductId;
    }

    public void setItemProductId(String itemProductId) {
        this.itemProductId = itemProductId;
    }

    public String getItemCategoryAttrKeyId() {
        return itemCategoryAttrKeyId;
    }

    public void setItemCategoryAttrKeyId(String itemCategoryAttrKeyId) {
        this.itemCategoryAttrKeyId = itemCategoryAttrKeyId;
    }

    public String getItemCategoryAttrValueId() {
        return itemCategoryAttrValueId;
    }

    public void setItemCategoryAttrValueId(String itemCategoryAttrValueId) {
        this.itemCategoryAttrValueId = itemCategoryAttrValueId;
    }

    public String getSelfDefiningValue() {
        return selfDefiningValue;
    }

    public void setSelfDefiningValue(String selfDefiningValue) {
        this.selfDefiningValue = selfDefiningValue;
    }

    public String getItemCategoryAttrKeyName() {
        return itemCategoryAttrKeyName;
    }

    public void setItemCategoryAttrKeyName(String itemCategoryAttrKeyName) {
        this.itemCategoryAttrKeyName = itemCategoryAttrKeyName;
    }

    @Override
    public String toString() {
        return "ItemProductDetailDTO{" +
                "itemProductDetailId='" + itemProductDetailId + '\'' +
                ", itemProductId='" + itemProductId + '\'' +
                ", itemCategoryAttrKeyId='" + itemCategoryAttrKeyId + '\'' +
                ", itemCategoryAttrValueId='" + itemCategoryAttrValueId + '\'' +
                ", selfDefiningValue='" + selfDefiningValue + '\'' +
                ", itemCategoryAttrKeyName='" + itemCategoryAttrKeyName + '\'' +
                '}';
    }
}