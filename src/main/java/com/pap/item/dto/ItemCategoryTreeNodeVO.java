package com.pap.item.dto;

import io.swagger.annotations.ApiModel;

import java.util.List;

/**
 * @Auther: alexgaoyh
 * @Date: 2019/1/7 16:58
 * @Description:
 */
@ApiModel(value = "ItemCategoryTreeNodeVO", description = "商品类别树形结构")
public class ItemCategoryTreeNodeVO extends ItemCategoryDTO {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private List<ItemCategoryTreeNodeVO> children = null;

    public List<ItemCategoryTreeNodeVO> getChildren() {
        if(children == null || children.size() == 0) {
            return null;
        } else {
            return children;
        }
    }

    public void setChildren(List<ItemCategoryTreeNodeVO> children) {
        this.children = children;
    }
}
