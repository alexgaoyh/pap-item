package com.pap.item.dto;

import java.io.Serializable;

public class SkuPropDetailsDTO implements Serializable {

    private String id;

    private String cname;

    private Boolean isActiveC = false;

    private Boolean notClick = false;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public Boolean getActiveC() {
        return isActiveC;
    }

    public void setActiveC(Boolean activeC) {
        isActiveC = activeC;
    }

    public Boolean getNotClick() {
        return notClick;
    }

    public void setNotClick(Boolean notClick) {
        this.notClick = notClick;
    }
}
