package com.pap.item.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SkuPropsDTO implements Serializable {

    private String name;

    private Boolean isActive = false;

    private List<SkuPropDetailsDTO> value = new ArrayList<SkuPropDetailsDTO>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public List<SkuPropDetailsDTO> getValue() {
        return value;
    }

    public void setValue(List<SkuPropDetailsDTO> value) {
        this.value = value;
    }
}
