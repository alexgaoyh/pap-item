package com.pap.item.dto;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.MyBatisColumnAnnotation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * 处理完商品货品信息之后，单独维护商品货品信息扩展信息，此处的处理逻辑按行存储
 */
@ApiModel(value = "ItemProductWithDetailDTO", description = "商品货品信息(含扩展明细)实体")
public class ItemProductWithDetailDTO extends PapBaseEntity implements Serializable {
    /**
     *  货品编号,所属表字段为t_item_product.ITEM_PRODUCT_ID
     */
    @MyBatisColumnAnnotation(name = "ITEM_PRODUCT_ID", value = "t_item_product_ITEM_PRODUCT_ID", chineseNote = "货品编号", tableAlias = "t_item_product")
    @ApiModelProperty(value = "货品编号")
    private String itemProductId;

    /**
     *  货品编码,所属表字段为t_item_product.ITEM_PRODUCT_CODE
     */
    @MyBatisColumnAnnotation(name = "ITEM_PRODUCT_CODE", value = "t_item_product_ITEM_PRODUCT_CODE", chineseNote = "货品编码", tableAlias = "t_item_product")
    @ApiModelProperty(value = "货品编码")
    private String itemProductCode;

    /**
     *  货品名称,所属表字段为t_item_product.ITEM_PRODUCT_NAME
     */
    @MyBatisColumnAnnotation(name = "ITEM_PRODUCT_NAME", value = "t_item_product_ITEM_PRODUCT_NAME", chineseNote = "货品名称", tableAlias = "t_item_product")
    @ApiModelProperty(value = "货品名称")
    private String itemProductName;

    /**
     *  父级货品编号,所属表字段为t_item_product.ITEM_PRODUCT_PARENT_ID
     */
    @MyBatisColumnAnnotation(name = "ITEM_PRODUCT_PARENT_ID", value = "t_item_product_ITEM_PRODUCT_PARENT_ID", chineseNote = "父级货品编号", tableAlias = "t_item_product")
    @ApiModelProperty(value = "父级货品编号")
    private String itemProductParentId;

    /**
     *  所属商品品牌,所属表字段为t_item_product.ITEM_BRAND_ID
     */
    @MyBatisColumnAnnotation(name = "ITEM_BRAND_ID", value = "t_item_product_ITEM_BRAND_ID", chineseNote = "所属商品品牌", tableAlias = "t_item_product")
    @ApiModelProperty(value = "所属商品品牌")
    private String itemBrandId;

    /**
     *  所属商品类目,所属表字段为t_item_product.ITEM_CATEGORY_ID
     */
    @MyBatisColumnAnnotation(name = "ITEM_CATEGORY_ID", value = "t_item_product_ITEM_CATEGORY_ID", chineseNote = "所属商品类目", tableAlias = "t_item_product")
    @ApiModelProperty(value = "所属商品类目")
    private String itemCategoryId;

    /**
     *  编号路径集合,所属表字段为t_item_product.PATH_IDS
     */
    @MyBatisColumnAnnotation(name = "PATH_IDS", value = "t_item_product_PATH_IDS", chineseNote = "编号路径集合", tableAlias = "t_item_product")
    @ApiModelProperty(value = "编号路径集合")
    private String pathIds;

    /**
     *  编码路径集合,所属表字段为t_item_product.PATH_CODES
     */
    @MyBatisColumnAnnotation(name = "PATH_CODES", value = "t_item_product_PATH_CODES", chineseNote = "编码路径集合", tableAlias = "t_item_product")
    @ApiModelProperty(value = "编码路径集合")
    private String pathCodes;

    /**
     *  名称路径集合,所属表字段为t_item_product.PATH_NAMES
     */
    @MyBatisColumnAnnotation(name = "PATH_NAMES", value = "t_item_product_PATH_NAMES", chineseNote = "名称路径集合", tableAlias = "t_item_product")
    @ApiModelProperty(value = "名称路径集合")
    private String pathNames;

    /**
     *  备注,所属表字段为t_item_product.REMARK
     */
    @MyBatisColumnAnnotation(name = "REMARK", value = "t_item_product_REMARK", chineseNote = "备注", tableAlias = "t_item_product")
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
     *  顺序号,所属表字段为t_item_product.SEQ_NO
     */
    @MyBatisColumnAnnotation(name = "SEQ_NO", value = "t_item_product_SEQ_NO", chineseNote = "顺序号", tableAlias = "t_item_product")
    @ApiModelProperty(value = "顺序号")
    private Integer seqNo;

    //   alexgaoyh

    @ApiModelProperty(value = "货品扩展明细信息")
    private List<ItemProductDetailDTO> details;

    @ApiModelProperty(value = "所属商品类目路径")
    private String itemCategoryIds;

    @Override
    public String getDynamicTableName() {
        return "t_item_product";
    }

    private static final long serialVersionUID = 1L;

    public String getItemProductId() {
        return itemProductId;
    }

    public void setItemProductId(String itemProductId) {
        this.itemProductId = itemProductId;
    }

    public String getItemProductCode() {
        return itemProductCode;
    }

    public void setItemProductCode(String itemProductCode) {
        this.itemProductCode = itemProductCode;
    }

    public String getItemProductName() {
        return itemProductName;
    }

    public void setItemProductName(String itemProductName) {
        this.itemProductName = itemProductName;
    }

    public String getItemProductParentId() {
        return itemProductParentId;
    }

    public void setItemProductParentId(String itemProductParentId) {
        this.itemProductParentId = itemProductParentId;
    }

    public String getItemBrandId() {
        return itemBrandId;
    }

    public void setItemBrandId(String itemBrandId) {
        this.itemBrandId = itemBrandId;
    }

    public String getItemCategoryId() {
        return itemCategoryId;
    }

    public void setItemCategoryId(String itemCategoryId) {
        this.itemCategoryId = itemCategoryId;
    }

    public String getPathIds() {
        return pathIds;
    }

    public void setPathIds(String pathIds) {
        this.pathIds = pathIds;
    }

    public String getPathCodes() {
        return pathCodes;
    }

    public void setPathCodes(String pathCodes) {
        this.pathCodes = pathCodes;
    }

    public String getPathNames() {
        return pathNames;
    }

    public void setPathNames(String pathNames) {
        this.pathNames = pathNames;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(Integer seqNo) {
        this.seqNo = seqNo;
    }

    public List<ItemProductDetailDTO> getDetails() {
        return details;
    }

    public void setDetails(List<ItemProductDetailDTO> details) {
        this.details = details;
    }

    public String getItemCategoryIds() {
        return itemCategoryIds;
    }

    public void setItemCategoryIds(String itemCategoryIds) {
        this.itemCategoryIds = itemCategoryIds;
    }

    @Override
    public String toString() {
        return "ItemProductWithDetailDTO{" +
                "itemProductId='" + itemProductId + '\'' +
                ", itemProductCode='" + itemProductCode + '\'' +
                ", itemProductName='" + itemProductName + '\'' +
                ", itemProductParentId='" + itemProductParentId + '\'' +
                ", itemBrandId='" + itemBrandId + '\'' +
                ", itemCategoryId='" + itemCategoryId + '\'' +
                ", pathIds='" + pathIds + '\'' +
                ", pathCodes='" + pathCodes + '\'' +
                ", pathNames='" + pathNames + '\'' +
                ", remark='" + remark + '\'' +
                ", seqNo=" + seqNo +
                ", details=" + details +
                ", itemCategoryIds='" + itemCategoryIds + '\'' +
                '}';
    }
}