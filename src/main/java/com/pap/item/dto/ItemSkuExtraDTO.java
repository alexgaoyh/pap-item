package com.pap.item.dto;

import com.pap.base.entity.PapBaseEntity;
import com.pap.base.mybatis.plugin.annotation.MyBatisColumnAnnotation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value = "ItemSkuExtraDTO", description = "商品SKU扩展信息实体")
public class ItemSkuExtraDTO extends PapBaseEntity implements Serializable {
    /**
     *  编号,所属表字段为t_item_sku_extra.ITEM_SKU_EXTRA_ID
     */
    @MyBatisColumnAnnotation(name = "ITEM_SKU_EXTRA_ID", value = "t_item_sku_extra_ITEM_SKU_EXTRA_ID", chineseNote = "编号", tableAlias = "t_item_sku_extra")
    @ApiModelProperty(value = "编号")
    private String itemSkuExtraId;

    /**
     *  所属商品SKU,所属表字段为t_item_sku_extra.ITEM_SKU_ID
     */
    @MyBatisColumnAnnotation(name = "ITEM_SKU_ID", value = "t_item_sku_extra_ITEM_SKU_ID", chineseNote = "所属商品SKU", tableAlias = "t_item_sku_extra")
    @ApiModelProperty(value = "所属商品SKU")
    private String itemSkuId;

    /**
     *  数据键,所属表字段为t_item_sku_extra.ATTR_KEY
     */
    @MyBatisColumnAnnotation(name = "ATTR_KEY", value = "t_item_sku_extra_ATTR_KEY", chineseNote = "数据键", tableAlias = "t_item_sku_extra")
    @ApiModelProperty(value = "数据键")
    private String attrKey;

    /**
     *  数据值,所属表字段为t_item_sku_extra.ATTR_VALUE
     */
    @MyBatisColumnAnnotation(name = "ATTR_VALUE", value = "t_item_sku_extra_ATTR_VALUE", chineseNote = "数据值", tableAlias = "t_item_sku_extra")
    @ApiModelProperty(value = "数据值")
    private String attrValue;

    /**
     *  自定义数据值,所属表字段为t_item_sku_extra.ATTR_SELF_DEFINING_VALUE
     */
    @MyBatisColumnAnnotation(name = "ATTR_SELF_DEFINING_VALUE", value = "t_item_sku_extra_ATTR_SELF_DEFINING_VALUE", chineseNote = "自定义数据值", tableAlias = "t_item_sku_extra")
    @ApiModelProperty(value = "自定义数据值")
    private String attrSelfDefiningValue;

    /**
     *  数据备注字段,所属表字段为t_item_sku_extra.ATTR_REMARK
     */
    @MyBatisColumnAnnotation(name = "ATTR_REMARK", value = "t_item_sku_extra_ATTR_REMARK", chineseNote = "数据备注字段", tableAlias = "t_item_sku_extra")
    @ApiModelProperty(value = "数据备注字段")
    private String attrRemark;

    @Override
    public String getDynamicTableName() {
        return "t_item_sku_extra";
    }

    private static final long serialVersionUID = 1L;

    public String getItemSkuExtraId() {
        return itemSkuExtraId;
    }

    public void setItemSkuExtraId(String itemSkuExtraId) {
        this.itemSkuExtraId = itemSkuExtraId;
    }

    public String getItemSkuId() {
        return itemSkuId;
    }

    public void setItemSkuId(String itemSkuId) {
        this.itemSkuId = itemSkuId;
    }

    public String getAttrKey() {
        return attrKey;
    }

    public void setAttrKey(String attrKey) {
        this.attrKey = attrKey;
    }

    public String getAttrValue() {
        return attrValue;
    }

    public void setAttrValue(String attrValue) {
        this.attrValue = attrValue;
    }

    public String getAttrSelfDefiningValue() {
        return attrSelfDefiningValue;
    }

    public void setAttrSelfDefiningValue(String attrSelfDefiningValue) {
        this.attrSelfDefiningValue = attrSelfDefiningValue;
    }

    public String getAttrRemark() {
        return attrRemark;
    }

    public void setAttrRemark(String attrRemark) {
        this.attrRemark = attrRemark;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", itemSkuExtraId=").append(itemSkuExtraId);
        sb.append(", itemSkuId=").append(itemSkuId);
        sb.append(", attrKey=").append(attrKey);
        sb.append(", attrValue=").append(attrValue);
        sb.append(", attrSelfDefiningValue=").append(attrSelfDefiningValue);
        sb.append(", attrRemark=").append(attrRemark);
        sb.append("]");
        return sb.toString();
    }
}