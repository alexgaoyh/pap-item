package com.pap.item.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class VantSkuPropsDTO implements Serializable {

    private String k;

    private String k_id;

    private String k_s;

    private List<VantSkuPropDetailsDTO> v;

    public String getK() {
        return k;
    }

    public void setK(String k) {
        this.k = k;
    }

    public String getK_id() {
        return k_id;
    }

    public void setK_id(String k_id) {
        this.k_id = k_id;
    }

    public String getK_s() {
        return k_s;
    }

    public void setK_s(String k_s) {
        this.k_s = k_s;
    }

    public List<VantSkuPropDetailsDTO> getV() {
        return v;
    }

    public void setV(List<VantSkuPropDetailsDTO> v) {
        this.v = v;
    }
}
