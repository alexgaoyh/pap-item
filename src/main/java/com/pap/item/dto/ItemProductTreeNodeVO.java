package com.pap.item.dto;

import io.swagger.annotations.ApiModel;

import java.util.List;

/**
 * @Auther: alexgaoyh
 * @Date: 2019/1/7 16:58
 * @Description:
 */
@ApiModel(value = "ItemProductTreeNodeVO", description = "商品货品树形结构")
public class ItemProductTreeNodeVO extends ItemProductDTO {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private List<ItemProductTreeNodeVO> children = null;

    public List<ItemProductTreeNodeVO> getChildren() {
        if(children == null || children.size() == 0) {
            return null;
        } else {
            return children;
        }
    }

    public void setChildren(List<ItemProductTreeNodeVO> children) {
        this.children = children;
    }
}
